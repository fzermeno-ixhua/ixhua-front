import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';
import { environment } from '../environments/environment';

import { AppComponent } from './app.component';
import { metaReducers, reducers } from './store/reducers';
import { SharedModule } from './modules/shared/shared.module';
import { APP_ROUTES } from './routes';
import { HttpClientModule } from '@angular/common/http';

import { LOCALE_ID } from '@angular/core';
import { MAT_DATE_LOCALE } from '@angular/material';
import localeEsMx from '@angular/common/locales/es-MX';
import { registerLocaleData } from '@angular/common';
import { GtagModule } from 'angular-gtag';

import { MatCarouselModule } from '@ngmodule/material-carousel';

import {
  FaqsComponent,
  TycComponent,
  ArcoComponent,
  PrivacyComponent,
  BusinessComponent, } from './components/pages';
import { MatPasswordStrengthModule } from '@angular-material-extensions/password-strength';


registerLocaleData(localeEsMx, 'es-MX');

@NgModule({
  declarations: [
  AppComponent, 
  FaqsComponent, 
  TycComponent, 
  ArcoComponent, 
  PrivacyComponent,
  BusinessComponent,
  ],
  imports: [
    //google analytics
    GtagModule.forRoot({ trackingId: 'UA-153490944-1', trackPageviews: true }),
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatPasswordStrengthModule.forRoot(),
    MatCarouselModule.forRoot(),
    RouterModule.forRoot(APP_ROUTES),
    StoreModule.forRoot(reducers, {
      metaReducers,
      runtimeChecks: {
        strictStateImmutability: true,
        strictActionImmutability: true
      }
    }),
    // Debugging config
    StoreDevtoolsModule.instrument({
      maxAge: 25,
      logOnly: environment.production
    }),
    EffectsModule.forRoot([]),
    SharedModule,
    MatPasswordStrengthModule,
  ],
  providers: [{ provide: MAT_DATE_LOCALE, useValue: 'es-MX' },{ provide: LOCALE_ID, useValue: 'es-MX' }],
  bootstrap: [AppComponent]
})
export class AppModule {}
