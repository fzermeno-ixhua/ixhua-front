import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { State } from '../../store/reducers/auth.reducer';
import { Store } from '@ngrx/store';
import { clearState, selectClientToken } from 'src/app/store/reducers';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { WhoAmIResponse } from '../../models/interfaces';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthEndpointsService {
  private clientToken: string = null;
  public static objectToQueryParam(obj: any): string {
    return Object.keys(obj)
      .map(k => `${encodeURIComponent(k)}=${encodeURIComponent(obj[k])}`)
      .join('&');
  }

  constructor(
    public http: HttpClient,
    private store: Store<State>,
    private router: Router
  ) {
    this.store.select<string>(selectClientToken)
      .subscribe(token => {
        return this.clientToken = token;
      });
  }

  private validateToken(): HttpHeaders | null {
    const { clientToken } = this;
    if (!clientToken) {
      this.store.dispatch(clearState());
      this.router.navigateByUrl('/auth/login')
        .then(() => console.error('Trying to access a protected resource without a token.'));
      return null;
    }
    return new HttpHeaders({
      Authorization: `Token ${clientToken}`
    });
  }

  protected post<T, P>(url: string, req: T): Observable<P> {
    const headers = this.validateToken();
    if (!headers) {
      return;
    }
    return this.http.post<P>(url, req, {
      headers
    });
  }

  protected get<P>(url: string, params: { [s: string]: any } = null): Observable<P> {
    const headers = this.validateToken();
    if (!headers) {
      return;
    }
    if (params) {
      url += '?' + AuthEndpointsService.objectToQueryParam(params);
    }
    return this.http.get<P>(url, {
      headers
    });
  }

  protected delete<P>(url: string, params: { [s: string]: any } = null): Observable<P> {
    const headers = this.validateToken();
    if (!headers) {
      return;
    }
    if (params) {
      url += '?' + AuthEndpointsService.objectToQueryParam(params);
    }
    return this.http.delete<P>(url, {
      headers
    });
  }

  public whoAmI(): Observable<{ success: boolean, user: WhoAmIResponse }> {
    return this.get<{ success: boolean, user: WhoAmIResponse }>(`${environment.API_URL}/api/v1/whoami/`);
  }

}
