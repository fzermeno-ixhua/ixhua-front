import { AuthEndpointsService } from "./auth-endpoints/auth-endpoints.service";
import { environment } from "../../environments/environment";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";


@Injectable({
  providedIn: 'root'
})
export class HrService extends AuthEndpointsService {
  
    private API_URL = environment.API_URL;

    //Validates Component Access Permission
    public validateComponent(req: any): Observable<any> {
      return this.http.post(`${this.API_URL}/api/v1/human_resources/validate_component_access/`, req);
    }

    //Obtain Family Team List
    public getTeamList(req: any): Observable<any> {
      return this.http.post(`${this.API_URL}/api/v1/human_resources/team_list/`, req);
    }

    // Obtain Family Messages from Last 72 Hours
    public getMessagesList(req: any): Observable<any> {
      return this.http.post(`${this.API_URL}/api/v1/human_resources/messages_list/`, req);
    }
    
    // Obtain Family Person Data
    public getPersonData(req: any): Observable<any> {
      return this.http.post(`${this.API_URL}/api/v1/human_resources/person_data/`, req);
    }

    //Save Family's Massive Message
    public putMassiveMessage(req: any): Observable<any> {
      return this.http.post(`${this.API_URL}/api/v1/human_resources/massive_messages/`, req);
    }

}
