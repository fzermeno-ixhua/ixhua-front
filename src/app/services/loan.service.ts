import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { AuthEndpointsService } from './auth-endpoints/auth-endpoints.service';
import {
  ClientFormResponse, 
  ClientLoansResponse,
  FamiliesListResponse,
  GetClientLoansResponse,
  LoanCalculatorRequest,
  LoanCalculatorResponse,
  LoanRequest,
  LoanResponse,
  ZipCodeResponse
} from 'src/app/models';

@Injectable({
  providedIn: 'root'
})
export class LoanService extends AuthEndpointsService {
  private API_URL = environment.API_URL;

  public loanInfo(): Observable<GetClientLoansResponse> {
    return this.get(`${this.API_URL}/api/v1/basic_loan_info/`);
  }

  // TODO: Update the interface that represents this response (missing 'extra_data')
  public loanOptions(req: LoanCalculatorRequest): Observable<LoanCalculatorResponse> {
    return this.get(`${this.API_URL}/api/v1/loan_calculator/`, req);
  }

  public clientInfo(): Observable<ClientFormResponse> {
    return this.get(`${this.API_URL}/api/v1/client_info/`);
  }

  public familiesOptions(): Observable<FamiliesListResponse> {
    return this.get<FamiliesListResponse>(`${this.API_URL}/api/v1/families_info/`);
  }

  public clientFiles(req: any): Observable<any> {
    //return this.http.post(`${this.API_URL}/api/v1/client/client_upload_documents/`, req);
    return this.post(`${this.API_URL}/api/v1/client_upload_file/`, req);
  }

  public loanRequest(req: LoanRequest): Observable<LoanResponse> {
    return this.post(`${this.API_URL}/api/v1/loan_request/`, req);
  }

  public zipCode(req: { zip: string }): Observable<ZipCodeResponse> {
    const { zip } = req;
    return this.http.get<ZipCodeResponse>(`https://api-codigos-postales.herokuapp.com/v2/codigo_postal/${zip}`);
  }

  public clientLoans(): Observable<ClientLoansResponse> {
    return this.get<ClientLoansResponse>(`${this.API_URL}/api/v1/my_loans/`);
  }

  // TODO: Type this function response
  public contractData(): Observable<any> {
    return this.get<any>(`${this.API_URL}/api/v1/contract_data/`)
  }

  //Obtener documentos
  public getClientDocuments(req: any): Observable<any> {
    return this.http.post(`${this.API_URL}/api/v1/client/client_document/`,req );
  }

  //Obtain client-loan requirements
  public getClientLoanRequirements(req: any): Observable<any> {
    return this.http.post(`${this.API_URL}/api/v1/client/client_loan_requirements/`,req );
  }

  //Obtain client services
  public getClientServices(req: any): Observable<any> {
    return this.http.post(`${this.API_URL}/api/v1/client/client_services/`,req );
  }

}
