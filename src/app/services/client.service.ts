import { AuthEndpointsService } from "./auth-endpoints/auth-endpoints.service";
import { environment } from "../../environments/environment";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { 
  DeleteAccountResponse,
  RequestLoanForm,
  LoanResponse
} from 'src/app/models';


@Injectable({
  providedIn: 'root'
})
export class ClientService extends AuthEndpointsService {
  
  private API_URL = environment.API_URL;

  public deleteClientAccount(): Observable<DeleteAccountResponse> {
    return this.delete<DeleteAccountResponse>(`${this.API_URL}/api/v1/delete_me/`);
  }

  //MESAJE PERSONALIZADO POR FAMILIA
  public customClientMessage(req: any): Observable<any> {
    return this.http.post(`${this.API_URL}/api/v1/client/show_custom_message/`, req);
  }

  //CANCELAR CUENTA
  public cancelClientAccount(req: any): Observable<any> {
    return this.http.post(`${this.API_URL}/api/v1/client/cancel_account/`, req);
  }

  //SUBIR DOCUMENTOS
  public UploadClientFiles(req: any): Observable<any> {
    return this.http.post(`${this.API_URL}/api/v1/client/client_upload_documents/`, req);
  }
  public DeleteClientFiles(req: any): Observable<any> {
    return this.http.post(`${this.API_URL}/api/v1/client/delete_document/`, req);
  }

  public updateInfo(req: RequestLoanForm): Observable<LoanResponse> {
    return this.post(`${this.API_URL}/api/v1/client/update_info/`, req);
  }



}
