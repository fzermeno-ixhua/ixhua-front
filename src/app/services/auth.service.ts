import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import {
  ConfirmRFCResponse,
  ConfirmRFCRequest,
  ConfirmPINResponse,
  ConfirmPINRequest,
  ChangePasswordRequest,
  ChangePasswordResponse,
  LoginRequest,
  LoginResponse,
  ResetPasswordRequest,
  ResetPasswordEmailRequest,
  ResetPasswordResponse,
  ResetPasswordEmailResponse
} from 'src/app/models';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private API_URL = environment.API_URL;

  constructor(
    private http: HttpClient
  ) {  }

  public confirmRfc(req: ConfirmRFCRequest): Observable<ConfirmRFCResponse> {
    return this.http.post<ConfirmRFCResponse>(`${this.API_URL}/api/v1/client/rfc_validation/`, req);
  }

  public validatePin(req: ConfirmPINRequest): Observable<ConfirmPINResponse> {
    return this.http.post<ConfirmPINResponse>(`${this.API_URL}/api/v1/pin_validation/`, req);
  }

  public changePasswordAfterPin(req: ChangePasswordRequest): Observable<ChangePasswordResponse> {
    return this.http.post<ChangePasswordResponse>(`${this.API_URL}/api/v1/client/create_password/`, req);
  }

  public forgotPassword(req: ResetPasswordEmailRequest): Observable<ResetPasswordEmailResponse> {
    return this.http.post<ResetPasswordEmailResponse>(`${this.API_URL}/api/v1/reset_password_mail/`, req);
  }

  public changePassword(req: ResetPasswordRequest): Observable<ResetPasswordResponse> {
    return this.http.post<ResetPasswordResponse>(`${this.API_URL}/api/v1/reset_password/`, req);
  }

  public clientLogin(req: LoginRequest): Observable<LoginResponse> {
    return this.http.post<LoginResponse>(`${this.API_URL}/api/v1/login/`, req,);
  }

  
  //FUNCION PARA ACTIVAR USUARIO - ACTUALIZAR SU INFORMACION
  public activateAccountSendInfo(req: any): Observable<any> {
    return this.http.post(`${this.API_URL}/api/v1/client/activate_client_account_step2/`, req);
  }

  //FUNCION PARA ACTIVAR USUARIO - SUBIR DOCUMENTOS
  public activateAccountUploadFiles(req: any): Observable<any> {
    return this.http.post(`${this.API_URL}/api/v1/client/activate_client_account_step3/`, req);
  }

  //FUNCION PARA ACTIVAR USUARIO - ENVIAR EL PIN
  public activateAccountSendPin(req: any): Observable<any> {
    return this.http.post(`${this.API_URL}/api/v1/client/activate_client_account_step4/`, req);
  }

  //FUNCION PARA CONTACTO DE EMPRESAS
  public businessContact(req: any): Observable<any> {
    return this.http.post(`${this.API_URL}/api/v1/client/business_message/`, req);
  }

  //FUNCION PARA CODIGO/NOMBRE DE EMPRESA/FAMILIA
  public getFamily(req: any): Observable<any> {
    return this.http.post(`${this.API_URL}/api/v1/client/business_family/`, req);
  }


}
