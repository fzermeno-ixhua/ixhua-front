import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatButtonModule,
  MatListModule,
  MatCardModule,
  MatInputModule,
  MatFormFieldModule,
  MatCheckboxModule,
  MatSidenavModule,
  MatToolbarModule,
  MatIconModule,
  MatRadioModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatSelectModule,
  MatTabsModule,
  MatDialogModule,
  MatSliderModule,
  MatSnackBarModule,
  MatTooltipModule,
  MatExpansionModule,
  MatProgressSpinnerModule

} from '@angular/material';

import { MatPasswordStrengthModule } from '@angular-material-extensions/password-strength';
import { MatCarouselModule } from '@ngmodule/material-carousel';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MatButtonModule,
    MatListModule,
    MatCardModule,
    MatInputModule,
    MatFormFieldModule,
    MatCheckboxModule,
    MatSidenavModule,
    MatToolbarModule,
    MatIconModule,
    MatRadioModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSelectModule,
    MatTabsModule,
    MatDialogModule,
    MatSliderModule,
    MatSnackBarModule,
    MatTooltipModule,
    MatExpansionModule,
    MatPasswordStrengthModule,
    MatProgressSpinnerModule,
    MatCarouselModule,
  ],
  exports: [
    MatButtonModule,
    MatListModule,
    MatCardModule,
    MatInputModule,
    MatFormFieldModule,
    MatCheckboxModule,
    MatSidenavModule,
    MatToolbarModule,
    MatIconModule,
    MatRadioModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSelectModule,
    MatTabsModule,
    MatDialogModule,
    MatSliderModule,
    MatSnackBarModule,
    MatTooltipModule,
    MatExpansionModule,
    MatPasswordStrengthModule,
    MatProgressSpinnerModule,
    MatCarouselModule,
  ]
})
export class MaterialModule {}
