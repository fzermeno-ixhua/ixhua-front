import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material/icon';
import { MaterialModule } from './material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import {
  LoanTotalsComponent,
  HeaderComponent,
  FooterComponent,
  UploadFileComponent,
  DropdownDateComponent,
  ContactComponent,
  QuestionsComponent,
  HowWorksComponent,
  InfoBlueSectionComponent,
  AboutUsComponent,
  LoanFooterComponent,
} from 'src/app/components';
import { FirstLoginDialogComponent } from 'src/app/components/first-login-dialog/first-login-dialog.component';
import { AccountDeletionModalComponent } from 'src/app/components/options/account-deletion-modal.component';
import { AccountDeletionFailedModalComponent } from "src/app/components/options/account-deletion-failed-modal.component";
import { EffectsModule } from "@ngrx/effects";
import { OptionsEffects } from "../../store/effects/options.effects";
import { DEFAULT_TIMEOUT, TimeoutInterceptor } from '../../services/timeoutinterceptor';
import { ClientInfoValidationDialogComponent } from 'src/app/components/client-info-validation-dialog/client-info-validation-dialog.component';
import { CloseSessionDialogComponent } from 'src/app/components/close-session-dialog/close-session-dialog.compoment';
import { OptionsChangePasswordDialogComponent } from 'src/app/components/options/options-change-password/options-change-password-dialog/options-change-password-dialog.component';


@NgModule({
  declarations: [
    LoanTotalsComponent,
    FirstLoginDialogComponent,
    HeaderComponent,
    FooterComponent,
    AccountDeletionModalComponent,
    AccountDeletionFailedModalComponent,
    ClientInfoValidationDialogComponent,
    CloseSessionDialogComponent,
    OptionsChangePasswordDialogComponent,
    UploadFileComponent,
    DropdownDateComponent,
    ContactComponent,
    QuestionsComponent,
    HowWorksComponent,
    InfoBlueSectionComponent,
    AboutUsComponent,
    LoanFooterComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule,
    EffectsModule.forFeature([ OptionsEffects ])
  ],
  exports: [
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    LoanTotalsComponent,
    FirstLoginDialogComponent,
    HeaderComponent,
    FooterComponent,
    AccountDeletionModalComponent,
    AccountDeletionFailedModalComponent,
    ClientInfoValidationDialogComponent,
    CloseSessionDialogComponent,
    OptionsChangePasswordDialogComponent,
    UploadFileComponent,
    DropdownDateComponent,
    ContactComponent,
    QuestionsComponent,
    HowWorksComponent,
    InfoBlueSectionComponent,
    AboutUsComponent,
    LoanFooterComponent
  ],
  entryComponents: [
    FirstLoginDialogComponent,
    AccountDeletionModalComponent,
    AccountDeletionFailedModalComponent,
    ClientInfoValidationDialogComponent,
    CloseSessionDialogComponent,
    OptionsChangePasswordDialogComponent,
    UploadFileComponent,
    QuestionsComponent
  ],
  providers: [
    [{ provide: HTTP_INTERCEPTORS, useClass: TimeoutInterceptor, multi: true }],
    [{ provide: DEFAULT_TIMEOUT, useValue: 60000 }]
  ],
})
export class SharedModule {
  constructor(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer) {
    iconRegistry
      .addSvgIcon(
        'app-menu',
        sanitizer.bypassSecurityTrustResourceUrl('assets/img/icons/menu.svg')
      )
      .addSvgIcon(
        'app-user',
        sanitizer.bypassSecurityTrustResourceUrl('assets/img/icons/menu.svg')
      )
      .addSvgIcon(
        'app-wallet',
        sanitizer.bypassSecurityTrustResourceUrl('assets/img/carterita.svg')
      )   
      .addSvgIcon(
        'app-options',
        sanitizer.bypassSecurityTrustResourceUrl('assets/img/icons/settings.svg')
      )
      .addSvgIcon(
        'app-loans',
        sanitizer.bypassSecurityTrustResourceUrl('assets/img/icons/loans.svg')
      )
      .addSvgIcon(
        'app-questions',
        sanitizer.bypassSecurityTrustResourceUrl('assets/img/icons/information.svg')
      )
      .addSvgIcon(
        'app-user',
        sanitizer.bypassSecurityTrustResourceUrl('assets/img/icons/user.svg')
      )
      .addSvgIcon(
        'app-eye',
        sanitizer.bypassSecurityTrustResourceUrl('assets/img/icons/eye.svg')
      )
      .addSvgIcon(
        'app-replace',
        sanitizer.bypassSecurityTrustResourceUrl('assets/img/icons/arrow.svg')
      )
      .addSvgIcon(
        'app-delete',
        sanitizer.bypassSecurityTrustResourceUrl('assets/img/icons/bin.svg')
      )

      .addSvgIcon(
        'red-circle',
        sanitizer.bypassSecurityTrustResourceUrl('assets/img/circle-red.svg')
      )

      .addSvgIcon(
        'app-seed-gray',
        sanitizer.bypassSecurityTrustResourceUrl(
          'assets/img/icons/seed-gray.svg'
        )
      )
      .addSvgIcon(
        'app-seed-green',
        sanitizer.bypassSecurityTrustResourceUrl(
          'assets/ixhua-colors/icons/seed_green.svg'
        )
      )
      .addSvgIcon(
        'app-seed-yellow',
        sanitizer.bypassSecurityTrustResourceUrl(
          'assets/ixhua-colors/icons/seed_orange.svg'
        )
      )

      .addSvgIcon(
        'app-menu-home',
        sanitizer.bypassSecurityTrustResourceUrl(
          'assets/ixhua-colors/icons/home.svg'
        )
      )
      .addSvgIcon(
        'app-menu-book',
        sanitizer.bypassSecurityTrustResourceUrl(
          'assets/ixhua-colors/icons/book.svg'
        )
      )

      .addSvgIcon(
        'app-menu-archive',
        sanitizer.bypassSecurityTrustResourceUrl(
          'assets/ixhua-colors/icons/archive.svg'
        )
      )

      .addSvgIcon(
        'app-menu-dollar-sign',
        sanitizer.bypassSecurityTrustResourceUrl(
          'assets/ixhua-colors/icons/dollar-sign.svg'
        )
      )

      .addSvgIcon(
        'app-menu-credit-card',
        sanitizer.bypassSecurityTrustResourceUrl(
          'assets/ixhua-colors/icons/credit-card.svg'
        )
      )

      .addSvgIcon(
        'app-menu-clipboard',
        sanitizer.bypassSecurityTrustResourceUrl(
          'assets/ixhua-colors/icons/clipboard.svg'
        )
      )

      .addSvgIcon(
        'app-menu-smile',
        sanitizer.bypassSecurityTrustResourceUrl(
          'assets/ixhua-colors/icons/smile.svg'
        )
      )

      .addSvgIcon(
        'app-menu-myteam',
        sanitizer.bypassSecurityTrustResourceUrl(
          'assets/ixhua-colors/icons/hr-myteam.svg')
      )

      .addSvgIcon(
        'app-menu-communication',
        sanitizer.bypassSecurityTrustResourceUrl(
          'assets/ixhua-colors/icons/hr-communication.svg')
      )


      .addSvgIcon(
        'app-add',
        sanitizer.bypassSecurityTrustResourceUrl('assets/img/icons/add.svg')
      )
      .addSvgIcon(
        'app-equals',
        sanitizer.bypassSecurityTrustResourceUrl('assets/img/icons/equals.svg')
      )
      .addSvgIcon(
        'app-close',
        sanitizer.bypassSecurityTrustResourceUrl('assets/img/icons/close.svg')
      )
      .addSvgIconSetInNamespace(
        'step-set',
        sanitizer.bypassSecurityTrustResourceUrl(
          '../../assets/img/icons/step.svg'
        )
      );


  }
}
