import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from 'src/app/modules/shared/shared.module';
import { LOAN_REQUEST_ROUTES } from 'src/app/routes';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { reducer } from 'src/app/store/reducers/loan-request.reducer';
import { LoanRequestEffects } from '../store/effects/loan-request.effects';
import {
  LoanRequestComponent,
  LoanRequestCalculatorComponent,
  LoanRequestFormComponent,
  //UploadFileComponent,
  DropdownDateComponent,
  LoanRequestContractComponent,
  LoanRequestDocumentsComponent,
} from './../components';
import { LoanRequestSuccessModalComponent } from '../components/loan-request/loan-request-contract/loan-request-success-modal';


@NgModule({
  declarations: [
    LoanRequestComponent,
    //UploadFileComponent,
    //DropdownDateComponent,
    LoanRequestCalculatorComponent,
    LoanRequestFormComponent,
    LoanRequestContractComponent,
    LoanRequestSuccessModalComponent,
    LoanRequestDocumentsComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(LOAN_REQUEST_ROUTES),
    StoreModule.forFeature('loan', reducer),
    EffectsModule.forFeature([LoanRequestEffects]),
    HttpClientModule
  ],
  exports: [
    LoanRequestComponent,
    //UploadFileComponent,
    //DropdownDateComponent,
    LoanRequestCalculatorComponent,
    LoanRequestFormComponent,
    LoanRequestDocumentsComponent,
  ],
  entryComponents: [
    LoanRequestSuccessModalComponent,
  ]
})
export class LoanRequestModule {}
