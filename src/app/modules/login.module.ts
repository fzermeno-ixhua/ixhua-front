import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import {
  AuthComponent,
  AuthFormComponent,
  AuthCreatePasswordComponent,
  AuthRfcConfirmComponent,
  AuthPinComponent,
  AuthForgotPasswordComponent,
  AuthExpiredRecoveryComponent,
  ActivateCountCellComponent,
  ActivateAccountEmailComponent,
  ActivationPinComponent,
  CreatePasswordComponent,
  ClientInfoValidationComponent,
  ClientInfoValidationDocumentsComponent,
  UploadFileComponent,
  LoginHomeComponent,
  CarouselComponent,
  RegisterFormComponent
} from 'src/app/components';
import { AuthForgotPasswordEmailSentComponent } from '../components/login/auth-forgot-password/auth-forgot-password-email-sent.component';
import { SharedModule } from 'src/app/modules/shared/shared.module';
import { LOGIN_ROUTES } from 'src/app/routes';
import { AuthEffects } from 'src/app/store/effects/auth.effects';
import { reducer } from 'src/app/store/reducers/auth.reducer';
import {MatTooltipModule} from '@angular/material/tooltip';


@NgModule({
  declarations: [
    AuthFormComponent,
    AuthComponent,
    AuthCreatePasswordComponent,
    AuthRfcConfirmComponent,
    AuthPinComponent,
    AuthForgotPasswordComponent,
    AuthForgotPasswordEmailSentComponent,
    AuthExpiredRecoveryComponent,
    ActivateCountCellComponent,
    ActivateAccountEmailComponent,
    ActivationPinComponent,
    CreatePasswordComponent,
    ClientInfoValidationComponent,
    ClientInfoValidationDocumentsComponent,
    LoginHomeComponent,
    CarouselComponent,
    RegisterFormComponent,
    //UploadFileComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(LOGIN_ROUTES),
    SharedModule,
    StoreModule.forFeature('auth', reducer),
    EffectsModule.forFeature([ AuthEffects ]),
    MatTooltipModule
  ],
  exports: [
    AuthFormComponent,
    AuthComponent,
    AuthCreatePasswordComponent,
    AuthRfcConfirmComponent,
    AuthForgotPasswordComponent,
    AuthForgotPasswordEmailSentComponent,
    AuthExpiredRecoveryComponent,
    ActivateCountCellComponent,
    ActivateAccountEmailComponent,
    ActivationPinComponent,
    CreatePasswordComponent,
    ClientInfoValidationComponent,
    ClientInfoValidationDocumentsComponent,
    LoginHomeComponent,
    CarouselComponent,
    RegisterFormComponent,
    //UploadFileComponent,
  ],
  entryComponents: [
    AuthForgotPasswordEmailSentComponent,
  ]
})
export class LoginModule { }
