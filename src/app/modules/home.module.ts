import { HOME_ROUTES } from '../routes/home.routes';
import { LoanSeedsComponent,LoanFooterComponent } from '../components/loan-request/loan-request-subcomponents';
import { HomeComponent } from '../components/home/home.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from './shared/shared.module';
import { RouterModule } from '@angular/router';
import { 
  OptionsComponent, 
  MyLoansComponent, 
  OptionsChangePasswordComponent, 
  MyDocumentsComponent,
  DropdownDateComponent,
  MyInfoComponent,
  MyPaymentsComponent,
  MyProfileComponent,
  MyContractComponent,
  MyDashboardComponent,
  HrMyteamComponent,
  HrPersonComponent,
  HrMymessagesComponent,
  MyHelpComponent,
} from '../components';

@NgModule({
  declarations: [
    HomeComponent,
    LoanSeedsComponent,
    MyLoansComponent,
    OptionsComponent,
    OptionsChangePasswordComponent,
    MyDocumentsComponent,
    MyInfoComponent,
    MyPaymentsComponent,
    MyProfileComponent,
    MyContractComponent,
    MyDashboardComponent,
    HrMyteamComponent,
    HrPersonComponent,
    HrMymessagesComponent,
    MyHelpComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(HOME_ROUTES),
  ],
  exports: [
    LoanSeedsComponent,
  ],
})
export class HomeModule {}
