import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'close-session-dialog',
  templateUrl: './close-session-dialog.component.html',
  styleUrls: ['./close-session-dialog.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CloseSessionDialogComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}