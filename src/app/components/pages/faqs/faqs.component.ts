import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Subject } from 'rxjs';
import { Gtag } from 'angular-gtag';

@Component({
  encapsulation: ViewEncapsulation.None,
  selector: 'app-faqs',
  templateUrl: './faqs.component.html'
})
export class FaqsComponent implements OnInit {
  routeValue: string;

  constructor(public gtag : Gtag,) {}


  parentSubject:Subject<any> = new Subject();
  public addOpenedClass: boolean = false;


  gtagEvent(){
    this.gtag.event('click', { 
      method: 'whatsapp',
      event_category: 'telefono',
      event_label: 'whatsapp'
    });
  }


  ngOnInit() {
    this.routeValue = 'faqs';
  }

  _checkClickInBack() {
    this.parentSubject.next(true);
  }

  _statusClassOpened(event : false){
    if(event){
      this.addOpenedClass = true;
    } else {
      this.addOpenedClass = false;
    }
  }
}
