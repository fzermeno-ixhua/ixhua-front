import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Subject } from 'rxjs';

@Component({
  encapsulation: ViewEncapsulation.None,
  selector: 'app-tyc',
  templateUrl: './tyc.component.html'
})
export class TycComponent implements OnInit {
  routeValue: string;
  parentSubject:Subject<any> = new Subject();
  public addOpenedClass: boolean = false;

  ngOnInit() {
    this.routeValue = 'tyc';
  }

  _checkClickInBack() {
    this.parentSubject.next(true);
  }

  _statusClassOpened(event : false){
    if(event){
      this.addOpenedClass = true;
    } else {
      this.addOpenedClass = false;
    }
  }
}
