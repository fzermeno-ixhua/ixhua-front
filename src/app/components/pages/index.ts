export * from './arco/arco.component';
export * from './faqs/faqs.component';
export * from './privacy/privacy.component';
export * from './tyc/tyc.component';
export * from './business/business.component';