import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Subject } from 'rxjs';

@Component({
  encapsulation: ViewEncapsulation.None,
  selector: 'app-arco',
  templateUrl: './arco.component.html'
})
export class ArcoComponent implements OnInit {
  routeValue: string;

  public fileName = 'file_page_arco_format.pdf';
  public path = '/assets/files/' + this.fileName;

  parentSubject:Subject<any> = new Subject();
  public addOpenedClass: boolean = false;

  ngOnInit() {
    this.routeValue = 'arco';
  }

  _checkClickInBack() {
    this.parentSubject.next(true);
  }

  _statusClassOpened(event : false){
    if(event){
      this.addOpenedClass = true;
    } else {
      this.addOpenedClass = false;
    }
  }
}
