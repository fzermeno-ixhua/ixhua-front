import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Subject } from 'rxjs';
import { FormControl, FormGroup,Validators} from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { AuthService } from 'src/app/services/auth.service';
import { Gtag } from 'angular-gtag';

@Component({
  encapsulation: ViewEncapsulation.None,
  selector: 'app-faqs',
  templateUrl: './business.component.html',
  //styleUrls: ['./business.component.scss'],
  //styleUrls: ['../../login/auth-styles/all.scss'],
})
export class BusinessComponent implements OnInit {

  routeValue: string;
  public slideIndex = 1
  public form = new FormGroup({
    name: new FormControl('', [Validators.required]),
    cell: new FormControl('', [Validators.required]),
    company: new FormControl('', [Validators.required]),
    number_employees: new FormControl('', [Validators.required]),
  });



  parentSubject:Subject<any> = new Subject();
  public addOpenedClass: boolean = false;
  private API_URL = environment.API_URL;
  constructor(
      private authService: AuthService,
      private http: HttpClient,
      private _snackBar: MatSnackBar,
      public gtag : Gtag,
    ) {  }


  ngOnInit() {
    this.routeValue = 'Empresas';
    window.scrollTo(0, 0)
    this.showSlides(this.slideIndex);
  }

  _checkClickInBack() {
    this.parentSubject.next(true);
  }

  _statusClassOpened(event : false){
    if(event){
      this.addOpenedClass = true;
    } else {
      this.addOpenedClass = false;
    }
  }


  gtagEvent(){
    this.gtag.event('click', { 
      method: 'whatsapp',
      event_category: 'telefono',
      event_label: 'whatsapp'
    });
  }

  public get nameErrorMessage() {
    const { form } = this;
    if (form.controls.name.hasError('required')) {
      return 'Este campo es requerido'
    }

    if (form.controls.name.errors) {
      return 'Este campo requiere al menos 5 caracteres';
    }

    return '&nbsp;';
  }

  public get cellErrorMessage() {
    const { form } = this;
    if (form.controls.cell.hasError('required')) {
      return 'Este campo es requerido'
    }

    if (form.controls.cell.errors) {
      return 'Este campo requiere 10 digitos, sin espacios, sin guiones y sin paréntesis.';
    }

    return '&nbsp;';
  }

  public activateConfirmButton(): void {
    const { form } = this;

    form.markAllAsTouched();
    if (form.invalid) {
      this.openSnackBar("No se pudo enviar la información.","Cerrar")
      return;
    }

    const body = { 
      name: form.value.name, 
      cell: form.value.cell,
      number_employees: form.value.number_employees,
      company:form.value.company
    }

    this.authService
    .businessContact(body)
    .subscribe({
      next: data => {
        this.openSnackBar("Se ha enviado la solicitud.","Cerrar")
      },
      error: error => {
        console.log("error",error.error.error)
        this.openSnackBar(error.error.error,"Cerrar")
      }
    })

    
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 5000,
    });
  }


  // Next/previous controls
  plusSlides(n) {
    this.showSlides(this.slideIndex += n);
  }

  // Thumbnail image controls
  currentSlide(n) {
    this.showSlides(this.slideIndex = n);
  }

  showSlides(n) {
    var i;
    //var slides = document.getElementsByClassName("mySlides");
    var slides = Array.from(document.getElementsByClassName('mySlides') as HTMLCollectionOf<HTMLElement>)
    if (n > slides.length) {this.slideIndex = 1} 
    if (n < 1) {this.slideIndex = slides.length}
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none"; 
    }
    
    slides[this.slideIndex-1].style.display = "block"; 
  }







}
