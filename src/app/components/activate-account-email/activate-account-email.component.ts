import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
import { Store } from '@ngrx/store';
import { State, selectAuthState } from 'src/app/store/reducers';
import * as fromAuth from 'src/app/store/reducers/auth.reducer';
import { confirmRfc } from 'src/app/store/actions';
import { FormControl, FormGroup,Validators} from '@angular/forms';
import {Subscription} from 'rxjs';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import {Router} from "@angular/router"

@Component({
  encapsulation: ViewEncapsulation.None,
  selector: 'activate-account-email',
  templateUrl: './activate-account-email.component.html',
  styleUrls: ['../login/auth-styles/all.scss']
})
export class ActivateAccountEmailComponent implements OnInit {
  safeSrc: SafeResourceUrl;
  parentSubject:Subject<any> = new Subject();
  addOpenedClass: boolean = false;

  private storeSubscription: Subscription;
  public form = new FormGroup({
    name: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required]),
    salary: new FormControl('', [Validators.required])
  });
  public rfc:any;
  private API_URL = environment.API_URL;
  public general_error:any;

  constructor(
    private sanitizer: DomSanitizer,
    private store: Store<State>,
    private http: HttpClient,
    private router: Router) {}

  ngOnInit(): void {
    window.scrollTo(0, 0) 
    this.storeSubscription = this.store.select<fromAuth.State>(selectAuthState)
      .subscribe(({ authUser,error }) => {
        if (error) {
          this.form.controls.name.setErrors({
            nameConfirm: error.message ? error.message : error.error
          });
        }
        this.rfc = authUser.rfc
      });
  }
 

  public get nameErrorMessage() {
    const { form } = this;
    if (form.controls.name.hasError('required')) {
      return 'Este campo es requerido'
    }

    if (form.controls.name.errors) {
      return 'Este campo requiere al menos 5 caracteres';
    }
    
    return '&nbsp;';
  }


  public get emailErrorMessage() {
    const { form } = this;
    if (form.controls.email.hasError('required')) {
      return 'Este campo es requerido'
    }
    return '&nbsp;';
  }


  public get salaryErrorMessage() {
    const { form } = this;
    if (form.controls.salary.hasError('required')) {
      return 'Este campo es requerido, Ingresa tu sueldo sin comas ni signos'
    }
    return '&nbsp;';
  }



  public activateConfirmButton(): void {
    const { form } = this;
    form.markAllAsTouched();
    if (form.invalid) {
      return;
    }

    const name_var = form.controls.name.value;
    const email_var = form.controls.email.value;
    const salary_var = form.controls.salary.value;
    const rfc = this.rfc;

    
    const body = { rfc:rfc,name:name_var,salary:salary_var,receptor:email_var,receptor_type:0 }
    
    this.http.post(`${this.API_URL}/api/v1/client/activate_account/`, body)
    .subscribe({
      next: data => {
        this.general_error = ""
        sessionStorage.setItem("receptor",email_var);
        this.router.navigate(['/auth/activation-pin'])
      },
      error: error => { 
        console.log("error",error.error.error)
        this.general_error = error.error.error;
      }
    })


  }

  _checkClickInBack() {
    this.parentSubject.next(true);
  }

  _statusClassOpened(event : false){
    if(event){
      this.addOpenedClass = true;
    } else {
      this.addOpenedClass = false;
    }
  }
 
}
