import {Component, OnInit, ViewEncapsulation, EventEmitter, Output, ChangeDetectorRef, ViewChild} from '@angular/core';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
import { Store } from '@ngrx/store';
import { State, selectAuthState } from 'src/app/store/reducers';
import * as fromAuth from 'src/app/store/reducers/auth.reducer';
import { confirmRfc, confirmCode } from 'src/app/store/actions';
import { FormControl, FormGroup, Validators} from '@angular/forms';
import {Subscription} from 'rxjs';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import {Router} from "@angular/router"
import { AuthService } from 'src/app/services/auth.service';
import { ClientInfoValidationDialogComponent } from 'src/app/components/client-info-validation-dialog/client-info-validation-dialog.component';
import { Gtag } from 'angular-gtag';
import { MatDialog } from '@angular/material';
import { ViewportScroller } from '@angular/common';
import * as dropdownsData from './client-info-validation-dropdowns-data';

interface Payment {
  value: string;
  viewValue: string;
}

@Component({
  encapsulation: ViewEncapsulation.None,
  selector: '[client-info-validation],[date-pipe]',
  templateUrl: './client-info-validation.component.html',
  styleUrls: ['../login/auth-styles/all.scss']
})
export class ClientInfoValidationComponent implements OnInit {
  @ViewChild('inputRef',{static: false}) inputRef;
  safeSrc: SafeResourceUrl;
  parentSubject:Subject<any> = new Subject();
  addOpenedClass: boolean = false;
  receptor:any;
  rfc:any;
  code: any;
  familyProduct: number = 0;
  familyID: number = 0;
  generalMessage = "Espera un momento, se esta guardando la información";
  isFormUploading = false;
  formLaboralStatus = dropdownsData.LABORAL_STATUS;
  public in_progress:boolean = false;
  
  public form = new FormGroup({
    name: new FormControl('',),
    firstLastName: new FormControl(''),
    secondLastName: new FormControl(''),
    cell: new FormControl(''),
    email: new FormControl(''),
    company: new FormControl(''),
    laboral_status: new FormControl(''),
    years: new FormControl(0,),
    months: new FormControl(1,),
    //salary: new FormControl('',[Validators.min(2000.00),Validators.max(99999.99)]),
    payment_frequency: new FormControl(''),
    checkbox: new FormControl(false),
    gender: new FormControl('No', [Validators.required]),
    //code: new FormControl(null),
    //rfc: new FormControl(''),
  });

  payments: Payment[] = [
    {value: 'Semanal', viewValue: 'Semanal'},
    {value: 'Catorcenal', viewValue: 'Catorcenal'},
    {value: 'Quincenal', viewValue: 'Quincenal'}
  ];
  

  private API_URL = environment.API_URL;
  public general_error:any;
  public show_general_error:any = false
  public today: number = Date.now();
  public companyData: any;

  @Output() open: EventEmitter<any> = new EventEmitter();
  @Output() close: EventEmitter<any> = new EventEmitter();

  constructor(
    private sanitizer: DomSanitizer,
    private authService: AuthService,
    private store: Store<State>,
    private http: HttpClient,
    private router: Router,
    private dialog: MatDialog,
    public gtag : Gtag,
    private viewportScroller: ViewportScroller,) {}

  ngOnInit(): void {

    window.scrollTo(0, 0)
    let authInfo = JSON.parse(sessionStorage.getItem('auth'))
    if (authInfo.authUser){

      if (authInfo.authUser.rfc != null){
        this.rfc = authInfo.authUser.rfc
      }else{
        this.router.navigate(['/auth'])
      }

      if (authInfo.authUser.code != null){
        this.code = authInfo.authUser.code
      }else{
        this.router.navigate(['/auth'])
      }
    
    }else{
      this.router.navigate(['/auth'])
    }


    let companyInfo = JSON.parse(sessionStorage.getItem('company'))
    //console.log(companyInfo)
    if(companyInfo.name){

        /* Data Setted in register-form */ 
      this.form.controls['company'].setValue(companyInfo.name);
      this.form.controls['company'].disable();

      this.familyID = companyInfo.id
      this.familyProduct = companyInfo.product

    }else{

      /* Data was not found in register-form */ 
      this.authService
      .getFamily({code:this.code})
      .subscribe({
        next: data => {
          this.companyData = data
          if(this.companyData.company_name){
            this.form.controls['company'].setValue(this.companyData.company_name);
            this.form.controls['company'].disable();

            this.familyID = this.companyData.company_id
            this.familyProduct = this.companyData.company_product
          }
        },
        error: error => { 
          console.log(error)    
        }
      })

    }

    // Binding email changeValue detection to apply lowercase
    this.form.get('email')
    .valueChanges
    .subscribe({
      next: val => {
        const elRef = this.inputRef.nativeElement;
        // get stored original unmodified value (not including last change)
        const orVal = elRef.getAttribute('data-model-value') || '';
        // modify new value to be equal to the original input (including last change)
        val = val.replace(orVal.toLowerCase(), orVal);
        // store original unmodified value (including last change)
        elRef.setAttribute('data-model-value', val);
        // set view value using DOM value property
        elRef.value = val.toLowerCase();
        this.form.controls['email'].setValue(val, {
          emitEvent: false,
          emitModelToViewChange: false
        });
      }
    })

  }
  
 

  public get nameErrorMessage() {
    const { form } = this;

    if (form.controls.name.hasError('required')) {
      return 'Este campo es requerido'
    }

    if (form.controls.name.errors) {
      return 'Este campo requiere al menos 3 caracteres';
    }

    return '&nbsp;';
  }

  public get firstLastNameErrorMessage() {
    const { form } = this;

    if (form.controls.firstLastName.hasError('required')) {
      return 'Este campo es requerido'
    }

    if (form.controls.firstLastName.errors) {
      return 'Este campo requiere al menos 3 caracteres';
    }

    return '&nbsp;';
  }

  public get secondLastNameErrorMessage() {
    const { form } = this;

    if (form.controls.secondLastName.hasError('required')) {
      return 'Este campo es requerido'
    }

    if (form.controls.secondLastName.errors) {
      return 'Este campo requiere al menos 3 caracteres';
    }

    return '&nbsp;';
  }

  public get cellErrorMessage() {
    const { form } = this;
    if (form.controls.cell.hasError('required')) {
      return 'Este campo es requerido'
    }

    if (form.controls.cell.errors) {
      return 'Este campo requiere 10 digitos, sin espacios, sin guiones y sin paréntesis';
    }

    return '&nbsp;';
  }

  public get emailErrorMessage() {
    const { form } = this;
    if (form.controls.email.hasError('required')) {
      return 'Este campo es requerido'
    }
    return '&nbsp;';
  }

  public get companyErrorMessage() {
    const { form } = this;
    if (form.controls.company.hasError('required')) {
      return 'Este campo es requerido'
    }
    return '&nbsp;';
  }

  public get yearsErrorMessage() {
    const { form } = this;
    if (form.controls.years.hasError('required')) {
      return 'Este campo es requerido'
    }

    if (form.controls.years.hasError('max')) {
      return 'Solo se permiten valores de 2 dígitos'
    }

    if (form.controls.years.errors) {
      //console.log(form.controls.cell.errors)
      return "Solo se permiten valores enteros"
    }

    return '&nbsp;';
  }

  public get monthsErrorMessage() {
    const { form } = this;
    if (form.controls.months.hasError('required')) {
      return 'Este campo es requerido'
    }

    if (form.controls.months.hasError('max')) {
      return 'El valor máximo es de 11 meses'
    }

    if (form.controls.months.errors) {
      //console.log(form.controls.cell.errors)
      return "Solo se permiten valores enteros"
    }

    //validacion de antiguedad de 6 meses
    //if ((form.controls.years.value == 0) && (form.controls.months.value < 6  ) || (form.controls.years.value == null) && (form.controls.months.value < 6  )) {
    //  return "El mínimo para solicitar es de 6 meses, regresa pronto"
    //}

    return '&nbsp;';
  }


  /*public get salaryErrorMessage() {
    const { form } = this;
    if (form.controls.salary.hasError('required')) {
      return 'Este campo es requerido'
    }

    if (form.controls.salary.hasError('min')) {
      return 'El mínimo para solicitar es de $ 2,000 pesos mensuales libres'
    }

    return '&nbsp;';
  }*/

  public get paymentFrequencyErrorMessage() {
    const { form } = this;
    if (form.controls.payment_frequency.hasError('required')) {
      return 'Este campo es requerido'
    }
    return '&nbsp;';
  }


  public activateConfirmButton(): void {

    const { form } = this;

    this.show_general_error = false

    form.markAllAsTouched();
    if (form.invalid) {
      return;
    }
    const name = form.controls.name.value
    const firstLastName = form.controls.firstLastName.value
    const secondLastName = form.controls.secondLastName.value
    const cell = form.controls.cell.value
    const email = form.controls.email.value
    const company = form.controls.company.value
    const years = form.controls.years.value
    const months = form.controls.months.value
    //const salary = form.controls.salary.value
    const payment_frequency = form.controls.payment_frequency.value    
    const rfc = this.rfc
    const code = this.code
    const laboralStatus = form.controls.laboral_status.value

    //codigo de promocion
    //const code = form.controls.code.value 
    //cuerpo
    const body = { 
      name: name,
      firstLastName:firstLastName,
      secondLastName:secondLastName, 
      cell: cell,
      email:email, 
      company: company, 
      years:years,
      months:months,
      //salary:salary,
      payment_frequency:payment_frequency,
      rfc: rfc,
      code:code,
      familyProduct: this.familyProduct,
      familyID: this.familyID,
      laboral_status: laboralStatus,
    }
    this.in_progress = true
    this.authService
    .activateAccountSendInfo(body)
    .subscribe({
      next: data => {
        this.general_error = ""
        sessionStorage.setItem("receptor",cell);
        sessionStorage.setItem("receptor_aux",email);

        this.gtag.event('click', { 
          method: 'registro',
          event_category: 'formulario',
          event_label: 'registro'
        });

        this.authService
        .activateAccountSendPin(body)
        .subscribe({
          next: data => {
            this.general_error = ""  
            this.router.navigate(['/auth/activation-pin'])   
          },
          error: error => { 
            this.general_error = error.error.error;
            this.in_progress = false
            this.show_general_error = true
          }
        })
        //this.router.navigate(['/auth/documents-validation'])
      },
      error: error => { 
        this.general_error = error.error.error;
        this.in_progress = false 
        this.show_general_error = true

      }
    })
  }

  _checkClickInBack() {
    this.parentSubject.next(true);
  }

  _statusClassOpened(event : false){
    if(event){
      this.addOpenedClass = true;
    } else {
      this.addOpenedClass = false;
    }
  }

  _goToPosition(e: string, fromXS: boolean) {
    this.viewportScroller.scrollToAnchor(e);
    if(fromXS) {
      this.close.emit(false);
    }
  }
 
}
