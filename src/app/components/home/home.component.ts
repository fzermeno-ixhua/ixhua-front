import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, filter, withLatestFrom } from 'rxjs/operators';
import { State } from 'src/app/store/reducers';
import { Store } from '@ngrx/store';
import { Router, NavigationEnd } from '@angular/router';
import { MatSidenav } from '@angular/material';
import {
  clientForm,
  getClientLoansInfo,
  logout
} from 'src/app/store/actions';
import { LoanService } from 'src/app/services/loan.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['../loan-request/loan-request-styles/all.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HomeComponent implements OnInit {

  rfc:any;
  show_documents_notification = false;
  @ViewChild('drawer', { static: true }) drawer: MatSidenav;
  public isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(map(result => result.matches));

  public clientServices: any;
  public showLoanRequest = true;
  public showUtilities = true;
  public showOutsourcing = true;

  /* HR Sections */
  public showHRMyTeam = true;
  public showHRCommunication = true;


  constructor(
    private store: Store<State>,
    private breakpointObserver: BreakpointObserver,
    private router: Router,
    private loanService: LoanService,
  ) {

    router.events.pipe(
      withLatestFrom(this.isHandset$),
      filter(([a, b]) => b && a instanceof NavigationEnd)
    ).subscribe(_ => this.drawer.close());

  }

  ngOnInit(): void {
    this.store.dispatch(clientForm());
    this.store.dispatch(getClientLoansInfo());

    //revisar estatus de documentos para mostrar notificacion
    let info = JSON.parse(sessionStorage.auth)
    if (info.client) {

      if(info.client.rfc){

        this.rfc = info.client.rfc

        this.loanService
        .getClientDocuments({rfc:this.rfc})
        .subscribe({
          next: data => {
            if (data.additional_file_date==false|| data.bank_account_date==false || data.ine_date==false || data.paysheet_one_date==false || data.paysheet_three_date==false || data.paysheet_two_date==false || data.proof_address_date==false || data.selfie_date==false) {
              this.show_documents_notification=true
            }
          },   
          error: error => { 
                console.log(error)    
          }
        })

        /* Get Client Services allowed */
        /*
        this.loanService
        .getClientServices({rfc:this.rfc})
        .subscribe({
          next: data => {

            this.clientServices = data

            if (!this.clientServices.loan) {
              this.showLoanRequest = false;
            }

            if (!this.clientServices.utilities){
              this.showUtilities = false;
            }

            if (!this.clientServices.outsourcing){
              this.showOutsourcing = false;
            }

          },
          error: error => { 
            console.log(error)    
          }
        })
        */
      }

    }

  }

  public onCloseSession(): void {
    this.store.dispatch(logout());
  }
}
