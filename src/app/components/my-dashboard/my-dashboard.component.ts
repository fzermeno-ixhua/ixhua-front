import { Component, OnDestroy, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Store } from '@ngrx/store';
import { selectClientLoans, State } from '../../store/reducers';
import { LoanInfo } from 'src/app/models';
import { Subscription } from 'rxjs';
import { fetchClientLoans } from '../../store/actions';
import { HrService } from '../../services/hr.service';

@Component({
  selector: 'app-my-dashboard',
  templateUrl: './my-dashboard.component.html',
})
export class MyDashboardComponent implements OnInit, OnDestroy {
  
  rfc:any;
  public messageList: any;
  public showMessageList = true;

  slides = [
    {
      'image': '/assets/ixhua-colors/banners/maria1.png',
      'link': '/home/my-info',
    },
    {
      'image': '/assets/ixhua-colors/banners/maria2.png',
      'link': '/home/my-documents',
    },
    {
      'image': '/assets/ixhua-colors/banners/maria3.png',
      'link': '/home',
    }
  ];
 
  constructor(
    private sanitizer: DomSanitizer,
    private store: Store<State>,
    private hrService: HrService,
  ) {  }

  ngOnInit(): void {
  
    let info = JSON.parse(sessionStorage.auth)
    if (info.client) {

      if(info.client.rfc){

        this.rfc = info.client.rfc

        /* Get Family Messages */
        this.hrService
        .getMessagesList({rfc:this.rfc})
        .subscribe({
          next: data => {
            this.messageList = data.messages            
          },
          error: error => { 
            console.log(error)    
          }
        })

      }

    }

  }

  ngOnDestroy(): void {
    
  }

  counter(i: number) {
    return new Array(i);
  }
  


}

