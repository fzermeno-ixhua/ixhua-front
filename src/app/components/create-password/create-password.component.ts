import {Component, OnInit, ViewEncapsulation,EventEmitter,Output} from '@angular/core';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
import { Store } from '@ngrx/store';
import { State, selectAuthState } from 'src/app/store/reducers';
import * as fromAuth from 'src/app/store/reducers/auth.reducer';
import { confirmRfc } from 'src/app/store/actions';
import { FormControl, FormGroup,Validators, ValidatorFn, AbstractControl} from '@angular/forms';
import {Subscription} from 'rxjs';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import {Router} from "@angular/router"
import {changePassword} from 'src/app/store/actions';
import { MatPasswordStrengthModule } from '@angular-material-extensions/password-strength';
import { ViewportScroller } from '@angular/common';

//import { ConsoleReporter } from 'jasmine';
import { Subject } from 'rxjs';

@Component({
  encapsulation: ViewEncapsulation.None,
  selector: 'create-password',
  templateUrl: './create-password.component.html',
  styleUrls: ['../login/auth-styles/all.scss']
})

export class CreatePasswordComponent implements OnInit {
  safeSrc: SafeResourceUrl;
  parentSubject:Subject<any> = new Subject();
  addOpenedClass: boolean = false;
  mismatch: boolean = false;

  public type_error_1 = false;
  public type_error_2 = false;
  public type_error_3 = false;

  
  public form = new FormGroup({
    password: new FormControl('', [
      Validators.required,
      Validators.minLength(8),
      ixhuaPasswordValidator()
    ]),
    passwordConfirm: new FormControl('', [
      Validators.required,
      Validators.minLength(8),
      ixhuaPasswordValidator()
    ]),
  }, passwordMatchValidator);
  

  private API_URL = environment.API_URL;
  public general_error:any;

  @Output() open: EventEmitter<any> = new EventEmitter();
  @Output() close: EventEmitter<any> = new EventEmitter();

  constructor(
    private sanitizer: DomSanitizer,
    private store: Store<State>,
    private http: HttpClient,
    private router: Router,
    private viewportScroller: ViewportScroller) {}

  ngOnInit(): void {
     window.scrollTo(0, 0) 
     if (!sessionStorage.pin) {
       this.router.navigate(['/auth/login'])
     }
  }
 

  public get passwordFormatError() {
    const { form } = this;

    if (form.controls.password.hasError('passwordValidation')) {
      return form.controls.password.errors.passwordValidation.value;
    }

    if (form.controls.password.hasError('minlength')) {
      return 'La contraseña debe tener por lo menos 8 caracteres.';
    }

    return '&nbsp;';
  }

  public get passwordConfirmFormatError() {
    const { form } = this;

    if (form.controls.passwordConfirm.hasError('passwordValidation')) {
      return form.controls.passwordConfirm.errors.passwordValidation.value;
    }

    if (form.controls.passwordConfirm .hasError('minlength')) {
      return 'La contraseña debe tener por lo menos 8 caracteres.';
    }

    return '&nbsp;';
  }


  public activateConfirmButton(): void {
    const { form } = this;
    form.markAllAsTouched();
    if (form.invalid) {
      return;
    }
    const pin = sessionStorage.pin
    const password =form.controls.password.value;
    const passwordConfirm = form.controls.passwordConfirm.value;
    
    /*this.http.post(`${this.API_URL}/api/v1/client/create_password/`, body)
    .subscribe({
      next: data => {
        this.general_error = ""
        this.router.navigate(['/auth/login'])
      },
      error: error => { 
        this.general_error = error.error.error;
      }
    })*/

    this.store.dispatch(changePassword({ pin, password,passwordConfirm }));


  }

  _checkClickInBack() {
    this.parentSubject.next(true);
  }

  _statusClassOpened(event : false){
    if(event){
      this.addOpenedClass = true;
    } else {
      this.addOpenedClass = false;
    }
  }

  _goToPosition(e: string, fromXS: boolean) {
    this.viewportScroller.scrollToAnchor(e);
    if(fromXS) {
      this.close.emit(false);
    }
  }


  validatePassword() {
    let control = this.form.controls.password.value
    const upperCaseRegex = /[A-Z]/;
    const symbolRegex = /[\$\%\&\/\(\)\[\]\¡\!\¿\?\=\#\@\*\^\|\-\.\,\:\;\\]/;

    if (!upperCaseRegex.test(control)) {
      this.type_error_1=false;
    }
    else{
      this.type_error_1=true;
    }


    if (control.length < 8) {
      this.type_error_2=false;
    }
    else{
      this.type_error_2=true;
    }

    if (!symbolRegex.test(control)) {
      this.type_error_3=false;
    }
    else{
      this.type_error_3=true;
    }

  }
 
}


function ixhuaPasswordValidator(): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} | null => {
    const { value } = control;
    const upperCaseRegex = /[A-Z]/;
    const symbolRegex = /[\$\%\&\/\(\)\[\]\¡\!\¿\?\=\#\@\*\^\|\-\.\,\:\;\\]/;

    if (!upperCaseRegex.test(value)) {
      return { passwordValidation: { value: 'La contraseña debe incluir una mayúscula.' } };
    }

    if (value.length < 8) {
      return { passwordValidation: { value: 'Al menos 8 caracteres' } };
    }

    if (!symbolRegex.test(value)) {
      return { passwordValidation: { value: 'La contraseña debe incluir por lo menos un símbolo.'} };
    }
    return null;
  };
}

function  passwordMatchValidator(g: FormGroup) {
  
  return g.get('password').value === g.get('passwordConfirm').value
      ? null : {'mismatch': true};
}

