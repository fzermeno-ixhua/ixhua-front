import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
import { Store } from '@ngrx/store';
import { State, selectAuthState } from 'src/app/store/reducers';
import * as fromAuth from 'src/app/store/reducers/auth.reducer';
import { confirmRfc } from 'src/app/store/actions';
import { FormControl, FormGroup,Validators} from '@angular/forms';
import {Subscription} from 'rxjs';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import {Router} from "@angular/router"
import { AuthService } from 'src/app/services/auth.service';
import { ClientInfoValidationDialogComponent } from 'src/app/components/client-info-validation-dialog/client-info-validation-dialog.component';

import { MatDialog } from '@angular/material';

interface Payment {
  value: string;
  viewValue: string;
}

@Component({
  encapsulation: ViewEncapsulation.None,
  selector: 'client-info-validation-documents',
  templateUrl: './client-info-validation-documents.component.html',
  styleUrls: ['../login/auth-styles/all.scss']
})
export class ClientInfoValidationDocumentsComponent implements OnInit {
  safeSrc: SafeResourceUrl;
  parentSubject:Subject<any> = new Subject();
  addOpenedClass: boolean = false;
  rfc:any;

  public static ALLOWED_FILE_TYPES = [
    'image/png',
    'image/jpeg',
    'image/jpg',
    'application/pdf'
  ];
  filesForm: FormGroup;
  isFormUploadingFiles = false;
  uploadFilesError = false;
  invalidFileMessage = 'El archivo que intentaste cargar excede el tamaño permitido o no es válido. Por favor verifícalo.';
  generalMessage = "Espera un momento, los archivos se están subiendo..."
  public form = new FormGroup({
    name: new FormControl('',),   
  });

  private API_URL = environment.API_URL;
  public general_error:any;
  public in_progress:boolean = false;

 
  constructor(
    private sanitizer: DomSanitizer,
    private authService: AuthService,
    private store: Store<State>,
    private http: HttpClient,
    private router: Router,
    private dialog: MatDialog) {}

  static toFormData<T>(formValue: T) {
    const formData = new FormData();
    for (const key of Object.keys(formValue)) {
      formData.append(key, formValue[key]);
    }
    return formData;
  }

  ngOnInit(): void { 
    window.scrollTo(0, 0) 
    this.initForm();  
    let authInfo = JSON.parse(sessionStorage.getItem('auth'))
    
    if (authInfo.authUser){
      if (authInfo.authUser.rfc != null){
        this.rfc = authInfo.authUser.rfc
      }
      else{
        this.router.navigate(['/auth'])
      }
    }
    else{
      this.router.navigate(['/auth'])
    }

  }

  initForm() {
    this.filesForm = new FormGroup({
      paysheet_one: new FormControl(null),
      paysheet_two: new FormControl(null),
      paysheet_three: new FormControl(null),
      selfie: new FormControl(null),
      rfc: new FormControl(null),
    });
  }
  

  public onFileChange(file, name: string): void {
    const { type, size } = file;
    const sizeInMb = size / 1024 / 1024;
    this.filesForm.get(name).markAsTouched();
    if (ClientInfoValidationDocumentsComponent.ALLOWED_FILE_TYPES.indexOf(type) === -1) {
      this.filesForm.get(name).setErrors({ invalidFileType: true });
      return;
    }
    if (sizeInMb > 20) {
      this.filesForm.get(name).setErrors({ invalidFileType: true });
      return;
    }
    this.filesForm.get(name).setValue(file);
    this.filesForm.get(name).setErrors(null);
  }

 
  public activateConfirmButton(): void {
    const { form, filesForm } = this;
    filesForm.markAllAsTouched();

    if (filesForm.invalid) {
      this.isFormUploadingFiles = false;
      return;
    }
    this.uploadFilesError = false;
    this.isFormUploadingFiles = true;   
    this.filesForm.patchValue({rfc: this.rfc});
    let body = (ClientInfoValidationDocumentsComponent.toFormData(filesForm.value))
    this.in_progress = true 
    //SUBIR LOS DOCUMENTOS
    this.authService
    .activateAccountUploadFiles(body)
    .subscribe({
      next: data => {
        this.general_error = ""  
        //ENVIAR EL PIN Y CAMBIAR A ESTATUS 4
        this.authService
        .activateAccountSendPin(body)
        .subscribe({
          next: data => {
            this.general_error = ""  
            this.router.navigate(['/auth/activation-pin'])   
          },
          error: error => { 
            this.general_error = error.error.error;
            this.in_progress = false 
          }
        })       
      },
      
      error: error => { 
        this.general_error = error.error.error;
        this.in_progress = false 

      }
    
    })
  }

  _checkClickInBack() {
    this.parentSubject.next(true);
  }

  _statusClassOpened(event : false){
    if(event){
      this.addOpenedClass = true;
    } else {
      this.addOpenedClass = false;
    }
  }
 
}
