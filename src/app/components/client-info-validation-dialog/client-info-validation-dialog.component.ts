import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'client-info-validation-dialog',
  templateUrl: './client-info-validation-dialog.component.html',
  styleUrls: ['./client-info-validation-dialog.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ClientInfoValidationDialogComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
