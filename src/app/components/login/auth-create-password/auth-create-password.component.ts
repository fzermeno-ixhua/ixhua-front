import {Component, OnDestroy, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, FormControl, ValidatorFn, Validators} from '@angular/forms';
import {Store} from '@ngrx/store';
import { State } from 'src/app/store/reducers/auth.reducer';
import { Subscription } from 'rxjs';
import {changePassword, resetPassword} from 'src/app/store/actions';
import { selectAuthState } from 'src/app/store/reducers';
import {ActivatedRoute, Router} from '@angular/router';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-auth-create-password',
  templateUrl: './auth-create-password.component.html',
  //styleUrls: ['./auth-create-password.component.scss']
  styleUrls: ['../auth-styles/all.scss']
})
export class AuthCreatePasswordComponent implements OnInit, OnDestroy {
  parentSubject:Subject<any> = new Subject();
  addOpenedClass: boolean = false;

  // First password fields
  userRfc: string = null;
  userPin: string = null;
  // Fields used to reset password
  token: string = null;
  private storeSubscription: Subscription;
  public form = this.fb.group({
    password: new FormControl('', [
      Validators.required,
      Validators.minLength(8),
      ixhuaPasswordValidator()
    ]),
    confirmPassword: new FormControl('', [
      Validators.required
    ])
  });

  constructor(
    private fb: FormBuilder,
    private store: Store<State>,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    // Validations to know if the componet is used to create the first user password
    // or to reset the password via email url
    const { token, timestamp } = this.route.snapshot.params;
    if (!!token && !!timestamp) {
      this.initResetPassword(token, timestamp);
    } else {
      this.initCreatePassword();
    }
  }

  ngOnDestroy(): void {
    if (this.storeSubscription) {
      this.storeSubscription.unsubscribe();
    }
  }

  /**
   * @description This function is called when
   *  the component is used to reset the password.
   * @param token The token to change the password
   * @param timestamp Timestamp in base64 only used to validate the token expiration date
   */
  private initResetPassword(token: string, timestamp: string): void {
    this.token = token;
    const expirationDate = Number(atob(timestamp)) * 1000;
    if (Date.now() > expirationDate) {
      this.router.navigateByUrl('/auth/invalid-recovery');
      return;
    }

    this.storeSubscription =
      this.store.select<State>(selectAuthState)
        .subscribe(({ error }) => {
          if (!!error) {
            this.form.get('confirmPassword').setErrors({ usedToken: error.error });
          }
        });
  }

  /**
   * @description This function is called when
   * the component is used to create the client
   * password for the first time.
   */
  private initCreatePassword(): void {
    this.storeSubscription = this.store.select<State>(selectAuthState)
      .subscribe(({ authUser, error }) => {
        this.userPin = authUser.pin;
        this.userRfc = authUser.rfc;
      });
  }

  public get passwordFormatError() {
    const { form } = this;

    if (form.controls.password.hasError('passwordValidation')) {
      return form.controls.password.errors.passwordValidation.value;
    }

    if (form.controls.password.hasError('minlength')) {
      return 'La contraseña debe tener por lo menos 8 caracteres.';
    }

    return '&nbsp;';
  }

  public get passwordConfirmError() {
    const formValue = this.form.value;

    if (this.form.controls.confirmPassword.hasError('usedToken')) {
      return this.form.controls.confirmPassword.getError('usedToken');
    }

    if (formValue.password !== formValue.confirmPassword) {
      return 'Las contraseñas deben coincidir';
    }
    return '&nbsp;';
  }

  public onSubmit() {
    const { form } = this;
    form.markAllAsTouched();
    if (!form.valid) {
      return;
    }
    const { password } = form.value;
    // Determine the type of request (first password creation or password reset)
    if (!!this.token) {
      const { token } = this;
      this.store.dispatch(resetPassword({ token, password }));
    }
    /*else {
      const { userPin: pin } = this;
      this.store.dispatch(changePassword({ pin, password }));
    }*/
  }

   _checkClickInBack() {
    this.parentSubject.next(true);
  }

  _statusClassOpened(event : false){
    if(event){
      this.addOpenedClass = true;
    } else {
      this.addOpenedClass = false;
    }
  }
  
}

/**
 * @description Custom validator function
 * to match Ixhua password requirements
 */
function ixhuaPasswordValidator(): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} | null => {
    const { value } = control;
    const upperCaseRegex = /[A-Z]/;
    const symbolRegex = /[\$\%\&\/\(\)\[\]\¡\!\¿\?\=\#\@\*\^\|\-\.\,\:\;\\]/;

    if (!upperCaseRegex.test(value)) {
      return { passwordValidation: { value: 'La contraseña debe incluir una mayúscula.' } };
    }

    if (!symbolRegex.test(value)) {
      return { passwordValidation: { value: 'La contraseña debe incluir por lo menos un símbolo.' } };
    }
    return null;
  };
}
