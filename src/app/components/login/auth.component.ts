import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
import { Gtag } from 'angular-gtag';
import { AuthService } from 'src/app/services/auth.service';
import {Router, ActivatedRoute} from "@angular/router"
//import { ConsoleReporter } from 'jasmine';
import { Subject } from 'rxjs';

@Component({
  encapsulation: ViewEncapsulation.None,
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth-styles/all.scss']
})
export class AuthComponent implements OnInit {
  safeSrc: SafeResourceUrl;
  parentSubject:Subject<any> = new Subject();
  addOpenedClass: boolean = false;
  private sub: any;
  public urlCode: string = '00000';
  public companyData: any;
  public companyName: string = 'Ixhua';
  public companyImages: string = 'ixhua';

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authService: AuthService,
    private sanitizer: DomSanitizer, public gtag : Gtag,) {
    this.safeSrc =  this.sanitizer.bypassSecurityTrustResourceUrl("https://www.youtube.com/embed/Fr72UT5cUCg?rel=0");
  }

  ngOnInit(){ 

    /* Comes from path: register/:code - User has a family personalized registration link */ 
    this.sub = this.route.params.subscribe(params => {

      this.urlCode = params['code'];
      if(this.urlCode ){

        this.authService
        .getFamily({code:this.urlCode})
        .subscribe({
          next: data => {
            this.companyData = data

            delete sessionStorage.company
            sessionStorage.setItem("company", JSON.stringify({ 
              id: this.companyData.company_id,
              name: this.companyData.company_name, 
              code: this.urlCode,
              images: this.companyData.company_images,
              product: this.companyData.company_product, 
            }) );

            this.companyName = this.companyData.company_name
            this.companyImages = this.companyData.company_images

          },
          error: error => {
            delete sessionStorage.company
            sessionStorage.setItem("company", JSON.stringify({ 
              id: 0,
              name: this.companyName, 
              code: this.urlCode,
              images: this.companyImages,
              product: 0, 
            }) );    
          }
        })
      }else{
        console.log('no venia parametro de codigo')
        delete sessionStorage.company
        sessionStorage.setItem("company", JSON.stringify({ 
          id: 0,
          name: '', 
          code: this.urlCode,
          images: this.companyImages,
          product: 0, 
        }) ); 
      }
    });
    
  }

  gtagEvent(){
    this.gtag.event('click', { 
      method: 'whatsapp',
      event_category: 'telefono',
      event_label: 'whatsapp'
    });
  }

  _checkClickInBack() {
    this.parentSubject.next(true);
  }

  _statusClassOpened(event : false){
    if(event){
      this.addOpenedClass = true;
    } else {
      this.addOpenedClass = false;
    }
  }
}
