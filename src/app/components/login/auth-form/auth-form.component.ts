import {Component, OnDestroy, OnInit} from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { State } from 'src/app/store/reducers/auth.reducer';
import { selectAuthState } from 'src/app/store/reducers';
import { login } from 'src/app/store/actions';

@Component({
  selector: 'app-auth-form',
  templateUrl: './auth-form.component.html',
  styleUrls: ['./auth-form.component.scss']
})
export class AuthFormComponent implements OnInit, OnDestroy {
  form = this.fb.group({
    rfc: new FormControl(null, Validators.required),
    password: new FormControl(null, Validators.required),
    checkbox: new FormControl(false, Validators.required)
  });
  private storeSubscription: Subscription;
  constructor(
    private fb: FormBuilder,
    private store: Store<State>
  ) { }

  ngOnInit(): void {
    this.storeSubscription = this.store.select<State>(selectAuthState)
      .subscribe(({ error }) => {
        if (error) {
          this.form.setErrors({
            invalidCredentials: error.message ? error.message : 'Ocurrió un error inesperado'
          });
        }
      });
  }

  ngOnDestroy(): void {
    this.storeSubscription.unsubscribe();
  }


  public get rfcErrorMessage() {
    const { form } = this;

    if (!form.controls.rfc.errors) {
      return '&nbsp;'; // No error message
    }

    if (form.controls.rfc.errors.required) {
      return 'Este campo es requerido';
    }
    if (form.controls.rfc.errors.pattern) {
      return 'Tu RFC está incorrecto, tienen que ser de 12-13 caracteres (revisa que contenga la homoclave).';
    }

    // No error message
    return '&nbsp;';
  }



  public onSubmit(): void {
    const { form } = this;
    form.markAllAsTouched();
    if (!form.valid) {
      return;
    }
    const { rfc, password } = form.value;
    this.store.dispatch(login({ rfc: rfc.toUpperCase(), password }));
  }

}
