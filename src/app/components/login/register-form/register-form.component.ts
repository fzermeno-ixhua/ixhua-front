import {Component, OnDestroy, OnInit, Input} from '@angular/core';
import { Store } from '@ngrx/store';
import { State, selectAuthState } from 'src/app/store/reducers';
import * as fromAuth from 'src/app/store/reducers/auth.reducer';
import { confirmRfc, confirmCode } from 'src/app/store/actions';
import { FormControl, FormGroup } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import {Subscription} from 'rxjs';
import {Router, ActivatedRoute} from "@angular/router"
import { Gtag } from 'angular-gtag';

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['../../login/auth-styles/all.scss']
})
export class RegisterFormComponent implements OnInit, OnDestroy {
  @Input() companyCode: string;
  private storeSubscription: Subscription;
  public form = new FormGroup({
    rfc: new FormControl(''),
    code: new FormControl(''),
    checkbox: new FormControl(false),
    //checkboxTwo: new FormControl(false)
  });
  public urlCode: string;
  public companyID: number = 0;
  public show_code_error: boolean = false;


  constructor(
    private store: Store<State>,
    private router: Router,
    private route: ActivatedRoute,
    private authService: AuthService,
    public gtag : Gtag,
  ) {  }

  ngOnInit() {

    if(this.companyCode){
      this.urlCode = this.companyCode;
      this.form.controls['code'].setValue(this.urlCode);
      this.form.controls['code'].disable();
    }
    
    //window.sessionStorage.clear()
    this.storeSubscription = this.store.select<fromAuth.State>(selectAuthState)
      .subscribe(({ error }) => {
        if (error) {
          this.form.controls.rfc.setErrors({
            rfcConfirm: error.message ? error.message : error.error
          });
        }
      });

  }

  ngOnDestroy(): void {
    this.storeSubscription.unsubscribe();
  }

  public get rfcErrorMessage() {
    const { form } = this;
    
    if (!form.controls.rfc.errors) {
      return '&nbsp;'; // No error message
    }

    if (form.controls.rfc.errors.rfcConfirm) {
      return form.controls.rfc.errors.rfcConfirm;
    }

    if (form.controls.rfc.errors.required) {
      return 'Este campo es requerido';
    }

    if (form.controls.rfc.errors.pattern) {
      return 'Tu RFC está incorrecto, tienen que ser 12-13 caracteres (revisa que contenga la homoclave).';
    }

    // No error message
    return '&nbsp;';
  }

  public get codeErrorMessage() {
    const { form } = this;

    if (!form.controls.code.errors) {
      return '&nbsp;'; // No error message
    }
    
    if (form.controls.code.hasError('required')) {
      return 'Este campo es requerido'
    }

    if (form.controls.code.errors) {
      return 'El valor debe ser de 5 a 10 caracteres'
    }

    return '&nbsp;';
  }


  gtagEventWhatsApp(){
    this.gtag.event('click', { 
      method: 'whatsapp',
      event_category: 'telefono',
      event_label: 'whatsapp'
    });
  }

  public onRfcConfirmButton(): void {
    const { form } = this;
    form.markAllAsTouched();
    if (form.invalid) {
      return;
    }
    const rfc = `${form.controls.rfc.value}`.toUpperCase();
    const code =  `${form.controls.code.value}`;
    this.store.dispatch(confirmRfc({ rfc }));
    this.store.dispatch(confirmCode({ code }));
  
  }

}
