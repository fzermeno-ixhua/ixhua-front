import { Component } from '@angular/core';
import { Subject } from 'rxjs';
@Component({
  selector: 'app-auth-expired-recovery',
  templateUrl: './auth-expired-recovery.component.html',
  styleUrls: ['../auth-styles/all.scss']
})
export class AuthExpiredRecoveryComponent {  
parentSubject:Subject<any> = new Subject();
addOpenedClass: boolean = false;


 _checkClickInBack() {
    this.parentSubject.next(true);
  }

  _statusClassOpened(event : false){
    if(event){
      this.addOpenedClass = true;
    } else {
      this.addOpenedClass = false;
    }
  }

}
