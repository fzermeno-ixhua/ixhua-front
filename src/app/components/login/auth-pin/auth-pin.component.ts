import {Component, OnDestroy, OnInit} from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import {confirmRfc, validatePin} from 'src/app/store/actions';
import { selectAuthState, State } from 'src/app/store/reducers';
import * as fromAuth from 'src/app/store/reducers/auth.reducer';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-auth-pin',
  templateUrl: './auth-pin.component.html',
  styleUrls: ['./auth-pin.component.scss']
})
export class AuthPinComponent implements OnInit, OnDestroy {

  mutedEmail = 'i****a@gmail.com';
  userRfc: string = null;
  isUserRfcConfirmed = false;
  // pinValidationError: string = null;
  private storeSubscription: Subscription;
  public form = new FormGroup({
    pin: new FormControl('')
  });

  constructor(
    private router: Router,
    private store: Store<State>
  ) { }

  ngOnInit() {
    this.storeSubscription = this.store.select<fromAuth.State>(selectAuthState)
      .subscribe(({ authUser: user, isRfcConfirmed, error}) => {

        if (error) {
          this.form.controls.pin.setErrors({
            pinValidation: error.message ? error.message : 'Ocurrió un error inesperado.'
          });
        }

        this.isUserRfcConfirmed = isRfcConfirmed;
        this.mutedEmail = user.mutedEmail;
        this.userRfc = user.rfc;
      });
  }

  ngOnDestroy(): void {
    this.storeSubscription.unsubscribe();
  }

  public get pinErrorMessage() {
    const { form } = this;

    if (!form.controls.pin.errors) {
      return '&nbsp;'; // No error message
    }

    if (form.controls.pin.errors.pinValidation) {
      return form.controls.pin.errors.pinValidation;
    }

    if (form.controls.pin.errors.required) {
      return 'Este campo es requerido.';
    }

    if (form.controls.pin.errors.pattern) {
      return 'El formato del PIN es incorrecto.';
    }

    return '&nbsp;';
  }

  public onResendPin(): void {
    const { userRfc: rfc } = this;
    if (!rfc) {
      this.router.navigateByUrl('/auth');
    }
    this.store.dispatch(confirmRfc({ rfc }));
  }

  public onPinSubmit(): void {
    const { form } = this;
    form.markAllAsTouched();
    if (!form.valid) {
      return;
    }
    this.store.dispatch(validatePin({ pin: form.value.pin }));
  }

}
