import { Component, ViewEncapsulation } from '@angular/core';
import {MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-auth-forgot-password',
  template: `
    <div class="text-center">
      <h2 class="my-1">Éxito</h2>
      <hr>
      <h3 class="mb-3">Te hemos enviado instrucciones a tu correo electrónico. {{email}}</h3>
      <button class="fs-24px mat-rounded-button mt-1 py-1"
              mat-flat-button
              color="primary"
              (click)="onClose()">
        Entendido
      </button>
    </div>
  `,
  encapsulation: ViewEncapsulation.None
})
export class AuthForgotPasswordEmailSentComponent {

   email = ""

  constructor(
    private dialogRef: MatDialogRef<AuthForgotPasswordEmailSentComponent>
  ) {  }

  onClose() {
    this.dialogRef.close();
  }
}
