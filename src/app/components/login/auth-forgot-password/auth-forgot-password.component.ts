import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { selectAuthState } from 'src/app/store/reducers';
import * as fromAuth from 'src/app/store/reducers/auth.reducer';
import { forgotPasswordEmail, forgotPasswordEmailReset } from 'src/app/store/actions';
import { FormControl, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { State } from 'src/app/store/reducers/auth.reducer';
import { MatDialog } from '@angular/material';
import { AuthForgotPasswordEmailSentComponent } from './auth-forgot-password-email-sent.component';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-auth-forgot-password',
  templateUrl: './auth-forgot-password.component.html',
  styleUrls: ['../auth-styles/all.scss']
})
export class AuthForgotPasswordComponent implements OnInit, OnDestroy {
  
  parentSubject:Subject<any> = new Subject();
  addOpenedClass: boolean = false;

  private storeSubscription: Subscription;
  public form = new FormGroup({
    rfc: new FormControl('')
  });

  constructor(
    private store: Store<State>,
    private dialog: MatDialog,
    private router: Router
  ) {  }

  ngOnInit() {
    this.storeSubscription = this.store.select<fromAuth.State>(selectAuthState)
      .subscribe(({ error, forgotEmailSent,forgotEmailNotification }) => {
        if (error) {
          this.form.controls.rfc.setErrors({
            rfcConfirm: !!error.message ? error.message : error
          });
        }

        if (forgotEmailSent) {
          const dialogRef = this.dialog.open(AuthForgotPasswordEmailSentComponent, {
            width: '500px',
          });
          dialogRef.componentInstance.email = forgotEmailNotification;

          dialogRef.afterClosed()
            .subscribe(() => {
              this.router.navigateByUrl('/auth/login')
                .then(() => this.store.dispatch(forgotPasswordEmailReset()));
            });
        }
      });
  }

  ngOnDestroy(): void {
    this.storeSubscription.unsubscribe();
  }

  public get rfcErrorMessage() {
    const { form } = this;

    if (!form.controls.rfc.errors) {
      return '&nbsp;'; // No error message
    }

    if (form.controls.rfc.errors.required) {
      return 'Este campo es requerido';
    }

    if (form.controls.rfc.errors.pattern) {
      return 'RFC incorrecto, favor de verificar.';
    }

    if (form.controls.rfc.errors.rfcConfirm) {
      return form.get('rfc').getError('rfcConfirm');
    }

    // No error message
    return '&nbsp;';
  }

  public onRfcConfirmButton(): void {
    const { form } = this;
    form.markAllAsTouched();
    if (form.invalid) {
      return;
    }
    const { rfc } = form.value;
    this.store.dispatch(forgotPasswordEmail({ rfc: `${rfc}`.toUpperCase() }));
  }

   _checkClickInBack() {
    this.parentSubject.next(true);
  }

  _statusClassOpened(event : false){
    if(event){
      this.addOpenedClass = true;
    } else {
      this.addOpenedClass = false;
    }
  }

}
