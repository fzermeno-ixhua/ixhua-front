import {Component, OnInit, ViewEncapsulation,EventEmitter,Output} from '@angular/core';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
import { Store } from '@ngrx/store';
import { State, selectAuthState } from 'src/app/store/reducers';
import * as fromAuth from 'src/app/store/reducers/auth.reducer';
import { confirmRfc } from 'src/app/store/actions';
import { FormControl, FormGroup,Validators} from '@angular/forms';
import {Subscription} from 'rxjs';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import {Router} from "@angular/router"
import { Gtag } from 'angular-gtag';
import { ViewportScroller } from '@angular/common';


@Component({
  encapsulation: ViewEncapsulation.None,
  selector: 'activation-pin',
  templateUrl: './activation-pin.component.html',
  styleUrls: ['../login/auth-styles/all.scss']
})
export class ActivationPinComponent implements OnInit {
  
  private storeSubscription: Subscription;
  safeSrc: SafeResourceUrl;
  parentSubject:Subject<any> = new Subject();
  addOpenedClass: boolean = false;
  receptor:any;
  receptor_aux:any;
  rfc:any;
  
  public form = new FormGroup({
    pin: new FormControl('')
  });
  

  private API_URL = environment.API_URL;
  public general_error:any;
  public resend_message:any;

  @Output() open: EventEmitter<any> = new EventEmitter();
  @Output() close: EventEmitter<any> = new EventEmitter();

  constructor(
    private sanitizer: DomSanitizer,
    private store: Store<State>,
    private http: HttpClient,
    private router: Router,
    public gtag : Gtag,
    private viewportScroller: ViewportScroller,) {}

  ngOnInit(): void {
    window.scrollTo(0, 0) 
    if(sessionStorage.receptor_aux){
      this.receptor_aux= sessionStorage.receptor_aux
    }
    this.storeSubscription = this.store.select<fromAuth.State>(selectAuthState)
      .subscribe(({ authUser,error }) => {
         if (authUser.rfc) {
           this.rfc = authUser.rfc
           this.receptor =  sessionStorage.receptor;
         }
      });
  }
 

  public get pinErrorMessage() {
    const { form } = this;

    if (!form.controls.pin.errors) {
      return '&nbsp;'; // No error message
    }

    if (form.controls.pin.errors.pinValidation) {
      return form.controls.pin.errors.pinValidation;
    }

    if (form.controls.pin.errors.required) {
      return 'Este campo es requerido.';
    }

    if (form.controls.pin.errors.pattern) {
      return 'El formato del PIN es incorrecto.';
    }

    return '&nbsp;';
  }


  public activateConfirmButton(): void {
    const { form } = this;
    form.markAllAsTouched();
    if (form.invalid) {
      return;
    }

    const pin = `${form.controls.pin.value}`.toUpperCase();
    const body = { pin: pin}

    this.http.post(`${this.API_URL}/api/v1/client/pin_validation/`, body)
    .subscribe({
      next: data => {
        this.general_error = ""
        this.resend_message = ""

        this.gtag.event('click', { 
          method: 'pin',
          event_category: 'formulario',
          event_label: 'pin'
        });

        sessionStorage.setItem("pin",pin);
        this.router.navigate(['/auth/activate-password'])
      },
      error: error => { 
        this.resend_message = ""
        this.general_error = error.error.error;
      }
    })
  }



  public resendPin(): void {
    const body = { rfc: this.rfc,receptor:this.receptor}

    this.http.post(`${this.API_URL}/api/v1/client/resend_pin/`, body)
    .subscribe({
      next: data => {
        this.resend_message = "Te enviamos un nuevo mensaje con tu número de PIN"
      },
      error: error => {
        this.resend_message = ""
        this.general_error = error.error.error;
      }
    })
  }

  _checkClickInBack() {
    this.parentSubject.next(true);
  }

  _statusClassOpened(event : false){
    if(event){
      this.addOpenedClass = true;
    } else {
      this.addOpenedClass = false;
    }
  }

  _goToPosition(e: string, fromXS: boolean) {
    this.viewportScroller.scrollToAnchor(e);
    if(fromXS) {
      this.close.emit(false);
    }
  }
 
}
