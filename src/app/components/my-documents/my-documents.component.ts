import { Component, OnDestroy, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Store } from '@ngrx/store';
import { selectClientLoans, State,selectClient } from '../../store/reducers';
import { LoanInfo } from 'src/app/models';
import { Subscription } from 'rxjs';
import { fetchClientLoans } from '../../store/actions';
import { environment } from '../../../environments/environment';
import { FormControl, FormGroup,Validators} from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import {Router} from "@angular/router"
import { MatDialog } from '@angular/material';
import { ClientService } from 'src/app/services/client.service';
import { LoanService } from 'src/app/services/loan.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatTooltipModule} from '@angular/material/tooltip';
@Component({
  selector: 'app-my-documents',
  templateUrl: './my-documents.component.html',
  styleUrls: ['../loan-request/loan-request-styles/all.scss']
})
export class MyDocumentsComponent implements OnInit {

	rfc:any;
	public static ALLOWED_FILE_TYPES = [
    	'image/png',
    	'image/jpeg',
    	'image/jpg',
    	'application/pdf'
  	];
  	filesForm: FormGroup;
  	isFormUploadingFiles = false;
  	uploadFilesError = false;
  	invalidFileMessage = 'El archivo que intentaste cargar excede el tamaño permitido o no es válido. Por favor verifícalo.';
  	generalMessage = "Espera un momento, los cambios se estan guardado"
  	public form = new FormGroup({
    	name: new FormControl('',),   
  	});

  private API_URL = environment.API_URL;
  public general_error:any;
  public in_progress:boolean = false;

  public clientDocuments: any;
  //ine
  public showIne = false;
  public fileIneAux : any = null;
  public fileIneDate : any = true;
  //additional_file
  public showAdditionalFile = false;
  public fileAdditionalFileAux : any = null;
  public fileAdditionalDate : any = true;

  //bank account
  public showBankAccount = false;
  public fileBankAccountAux : any = null;
  public fileBankAccountDate : any = true;
  // proof address
  public showProofAddress = false;
  public fileProofAddressAux : any = null;
  public fileProofAddressDate : any = true;
  //paysheet one
  public showPaysheetOne= false;
  public filePaysheetOneAux : any = null;
  public filePaysheetOneDate: any = true;

  public showPaysheetTwo= false;
  public filePaysheetTwoAux : any = null;
  public filePaysheetTwoDate : any = true;

  public showPaysheetThree= false;
  public filePaysheetThreeAux : any = null;
  public filePaysheetThreeDate : any = true;

  public showSelfie= false;
  public fileSelfieAux : any = null;
  public fileSelfieDate : any = true;


  public showClientType = false



  	constructor(
  		private sanitizer: DomSanitizer,
	    private store: Store<State>,
	    private http: HttpClient,
	    private router: Router,
	    private dialog: MatDialog,
	    private ClientService: ClientService,
      private loanService: LoanService,
      private _snackBar: MatSnackBar
  	) {  }

  	static toFormData<T>(formValue: T) {
    	const formData = new FormData();
    	for (const key of Object.keys(formValue)) {
      	formData.append(key, formValue[key]);
    	}
    	return formData;
  	}


	ngOnInit(): void {
	  this.initForm();

    let info = JSON.parse(sessionStorage.auth)
    if (info.client) {
      if(info.client.rfc){
        this.rfc = info.client.rfc
        
        this.loanService
        .getClientDocuments({rfc:this.rfc})
        .subscribe({
          next: data => {
              this.clientDocuments = data

              if(this.clientDocuments.client_type){
                this.showClientType = this.clientDocuments.client_type
              }

              if (this.clientDocuments.ine) {
                this.showIne = true;
                this.fileIneAux = this.clientDocuments.ine; 
                this.fileIneDate = this.clientDocuments.ine_date    
              }

              if (this.clientDocuments.additional_file) {
                this.showAdditionalFile = true;
                this.fileAdditionalFileAux = this.clientDocuments.additional_file; 
                this.fileAdditionalDate = this.clientDocuments.additional_file_date
              }

              if (this.clientDocuments.bank_account) {
                this.showBankAccount = true;
                this.fileBankAccountAux = this.clientDocuments.bank_account;
                this.fileBankAccountDate = this.clientDocuments.bank_account_date;      
              }

              if (this.clientDocuments.proof_address) {
                this.showProofAddress = true;
                this.fileProofAddressAux = this.clientDocuments.proof_address;  
                this.fileProofAddressDate = this.clientDocuments.proof_address_date;
              }

              if (this.clientDocuments.paysheet_one) {
                this.showPaysheetOne = true;
                this.filePaysheetOneAux = this.clientDocuments.paysheet_one;
                this.filePaysheetOneDate = this.clientDocuments.paysheet_one_date;      
              }

              if (this.clientDocuments.paysheet_two) {
                this.showPaysheetTwo = true;
                this.filePaysheetTwoAux = this.clientDocuments.paysheet_two;   
                this.filePaysheetTwoDate = this.clientDocuments.paysheet_two_date;     
              }

              if (this.clientDocuments.paysheet_three) {
                this.showPaysheetThree = true;
                this.filePaysheetThreeAux = this.clientDocuments.paysheet_three;
                this.filePaysheetThreeDate = this.clientDocuments.paysheet_three_date;        
              }

              if (this.clientDocuments.selfie) {
                this.showSelfie = true;
                this.fileSelfieAux= this.clientDocuments.selfie; 
                this.fileSelfieDate = this.clientDocuments.selfie_date;       
              }
            },   
          error: error => { 
                console.log(error)    
            }
                
        })

      }
    }
	}

	initForm() {
    this.filesForm = new FormGroup({
      
      paysheet_one: new FormControl(null),   
     	paysheet_two: new FormControl(null),
      paysheet_three: new FormControl(null),
      selfie: new FormControl(null),
      ine: new FormControl(null),
      bank_account: new FormControl(null), 
      proof_address: new FormControl(null),
      additional_file: new FormControl(null),
      rfc: new FormControl(null), 
    });
  }

  public onFileChange(file, name: string): void {
    const { type, size } = file;
    const sizeInMb = size / 1024 / 1024;
    this.filesForm.get(name).markAsTouched();
    if (MyDocumentsComponent.ALLOWED_FILE_TYPES.indexOf(type) === -1) {
      this.filesForm.get(name).setErrors({ invalidFileType: true });
      return;
    }
    if (sizeInMb > 20) {
      this.filesForm.get(name).setErrors({ invalidFileType: true });
      return;
    }
    this.filesForm.get(name).setValue(file);
    this.filesForm.get(name).setErrors(null);
  }


  public changeFile(name,status){

    if (name == 'ine'){
      this.showIne = status;
      this.fileIneDate = true;
    }

    if (name == 'additional_file'){
      this.showAdditionalFile = status;
      this.fileAdditionalDate = true;
    }

    if (name == 'bank_account'){
      this.showBankAccount = status;
      this.fileBankAccountDate = true;
    }

    if (name == 'proof_address'){
      this.showProofAddress = status;
      this.fileProofAddressDate = true;
    }

    if (name == 'paysheet_one'){
      this.showPaysheetOne = status;
      this.filePaysheetOneDate = true;
    }

    if (name == 'paysheet_two'){
      this.showPaysheetTwo = status;
      this.filePaysheetTwoDate = true;
    }

    if (name == 'paysheet_three'){
      this.showPaysheetThree = status;
      this.filePaysheetThreeDate = true;
    }

    if (name == 'selfie'){
      this.showSelfie = status;
      this.fileSelfieDate = true;
    }

  }

  public deleteFile(name,status){

    let body = {rfc: this.rfc,document_type:name}
    this.in_progress = true
    this.ClientService
    .DeleteClientFiles(body)
    .subscribe({
      next: data => {
        this.general_error = "" 
        this.in_progress = false
        this.changeFile(name,status)
        this.openSnackBar("El archivo fue eliminado","Cerrar")
      
      },
      
      error: error => { 
        this.general_error = error.error.error;
        this.in_progress = false 
        //this.openSnackBar("Error,El archivo no pudo ser eliminado","Cerrar")
      }
    
    })

  }

  public activateConfirmButton(): void {
    const { form, filesForm } = this;
    filesForm.markAllAsTouched();

    if (filesForm.invalid) {
      this.isFormUploadingFiles = false;
      return;
    }

    if (this.fileIneDate == false || this.fileAdditionalDate == false || this.fileBankAccountDate == false || this.fileProofAddressDate == false || this.filePaysheetOneDate == false || this.filePaysheetTwoDate == false || this.filePaysheetThreeDate == false || this.fileSelfieDate == false  ) {
      this.isFormUploadingFiles = false;
      this.general_error = 'Uno o más documentos están vencidos, actualizalos para guardar'
      return;
    }

    this.uploadFilesError = false;
    this.isFormUploadingFiles = true;   
    this.filesForm.patchValue({rfc: this.rfc});

    let body = (MyDocumentsComponent.toFormData(filesForm.value))
    this.in_progress = true 
    //SUBIR LOS DOCUMENTOS
    this.ClientService
    .UploadClientFiles(body)
    .subscribe({
      next: data => {
        this.general_error = "" 
        this.in_progress = false
        this.openSnackBar("Los archvios se han guardado","Cerrar")
        this.router.navigate(['/home'])   
      },  
      error: error => { 
        this.general_error = error.error.error;
        this.in_progress = false 

      }
    
    })
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 5000,
    });
  }



  
}
