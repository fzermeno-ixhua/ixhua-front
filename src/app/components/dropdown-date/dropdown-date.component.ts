import {
  Component,
  OnInit,
  ViewEncapsulation,
  Output,
  EventEmitter,
  OnDestroy, Input
} from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { SubSink } from 'subsink';

@Component({
  encapsulation: ViewEncapsulation.None,
  selector: 'app-dropdown-date',
  templateUrl: './dropdown-date.component.html',
  styleUrls: ['./dropdown-date.component.scss']
})
export class DropdownDateComponent implements OnInit, OnDestroy {
  sub = new SubSink();
  today = new Date();
  form = new FormGroup({
    day: new FormControl('01'),
    month: new FormControl('01'),
    year: new FormControl(this.today.getFullYear())
  });

  days = [];
  months = [
    '01',
    '02',
    '03',
    '04',
    '05',
    '06',
    '07',
    '08',
    '09',
    '10',
    '11',
    '12'
  ];
  years = [];

  @Output() changesDate = new EventEmitter<string>();
  @Input() set isFirstLoan(isFirstLoan: boolean) {
    if (!!isFirstLoan) {
      this.form.enable();
    } else {
      this.form.disable();
    }
  }
  @Input() set date(date: string) {
    const dateArray = date.split('-');
    if (dateArray.length !== 3) { return; }
    this.form.get('year').setValue(+dateArray[0]);
    this.form.get('month').setValue(dateArray[1]);
    this.daysInMonth();
    this.form.get('day').setValue(dateArray[2]);
  }

  ngOnInit() {
    this.createYear();
    this.daysInMonth();
    this.sub.sink = this.form.valueChanges.subscribe(() => this.emitDate());
    this.emitDate();
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  createYear() {
    const arrY = [];
    const start = this.today.getFullYear() - 100;
    let end = this.today.getFullYear();

    while (start < end) {
      arrY.push(end);
      end = end - 1;
    }
    this.years = arrY;
  }

  daysInMonth() {
    const { month, year } = this.form.value;
    const n = new Date(
      +month,
      +year,
      0
    ).getDate();

    const days = [];
    for (let i = 1; i <= n; i++) {
      days.push((i < 10 ? '0' : '') + i);
    }
    this.days = days;
  }

  emitDate() {
    const date = this.form.getRawValue();
    // this.changesDate.emit(`${date['day']}/${date['month']}/${date['year']}`);
    this.changesDate.emit(`${date.year}-${date.month}-${date.day}`);
  }

  trackByFn(index) {
    return index;
  }
}
