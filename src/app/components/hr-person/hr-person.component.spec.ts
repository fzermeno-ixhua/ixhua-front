import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HrPersonComponent } from './hr-person.component';

describe('HrPersonComponent', () => {
  let component: HrPersonComponent;
  let fixture: ComponentFixture<HrPersonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HrPersonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HrPersonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
