import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { HrService } from 'src/app/services/hr.service'
import { LoanService } from 'src/app/services/loan.service'

@Component({
  selector: 'app-hr-person',
  templateUrl: './hr-person.component.html',
  styleUrls: ['./hr-person.component.scss']
})
export class HrPersonComponent implements OnInit {

  rfc:any;
  idCollaborator: string;
  private sub: any
  public userData: any;

  public clientDocuments: any;
  //ine
  public showIne = false;
  public fileIneAux : any = null;
  //additional_file
  public showAdditionalFile = false;
  public fileAdditionalFileAux : any = null;
  //bank account
  public showBankAccount = false;
  public fileBankAccountAux : any = null;
  // proof address
  public showProofAddress = false;
  public fileProofAddressAux : any = null;
  //paysheet one
  public showPaysheetOne= false;
  public filePaysheetOneAux : any = null;
  //paysheet two
  public showPaysheetTwo= false;
  public filePaysheetTwoAux : any = null;
  //paysheet three
  public showPaysheetThree= false;
  public filePaysheetThreeAux : any = null;
  //selfie
  public showSelfie= false;
  public fileSelfieAux : any = null;

  public userSelfie: string = '/assets/ixhua-colors/people/avatar.png'

  constructor(
    private route: ActivatedRoute,
    private hrService: HrService,
    private http: HttpClient,
  ) {  }

  ngOnInit() {

    let info = JSON.parse(sessionStorage.auth)
    if (info.client) {
      this.rfc = info.client.rfc
    }

    this.sub = this.route.params.subscribe(params => {
      this.idCollaborator = params['id'];
    });

    if(this.idCollaborator && this.rfc){

      this.hrService
      .getPersonData({rfc:this.rfc, id_member:this.idCollaborator})
        .subscribe({
          next: data => {

            this.userData = data

            console.log(this.userData)
            if (this.userData.documents.ine) {              
              this.showIne = true;
            }

            if (this.userData.documents.additional_file) {              
              this.showAdditionalFile = true;
            }

            if (this.userData.documents.bank_account) {              
              this.showBankAccount = true;
            }

            if (this.userData.documents.proof_address) {              
              this.showProofAddress = true;
            }

            if (this.userData.documents.selfie) {              
              this.userSelfie = this.userData.documents.selfie.url;
            }

          },
          error: error => { 
            console.log(error)    
          }
        })

    }
    
  }

}
