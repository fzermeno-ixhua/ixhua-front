import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HrMymessagesComponent } from './hr-mymessages.component';

describe('HrMymessagesComponent', () => {
  let component: HrMymessagesComponent;
  let fixture: ComponentFixture<HrMymessagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HrMymessagesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HrMymessagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
