import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatTooltipModule} from '@angular/material/tooltip';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { environment } from '../../../environments/environment';
import { HrService } from 'src/app/services/hr.service';


@Component({
  selector: 'app-hr-mymessages',
  templateUrl: './hr-mymessages.component.html',
  styleUrls: ['./hr-mymessages.component.scss']
})
export class HrMymessagesComponent implements OnInit {

  rfc:any;
  component : string = 'communication';
  form: FormGroup;
  requiredMessage = 'Este campo es requerido'

  public showCommunication : boolean = false;
  public userComponent: any;
  public formSubmitAttempt: boolean;
  private API_URL = environment.API_URL;

  constructor(
    private hrService: HrService,
    private http: HttpClient,
    private router: Router,
    private _snackBar: MatSnackBar
  ) { }
  
   /**
   * @description Converts a angular reactive form value to FormData
   *  in order to be sent over http request.
   * @param formValue The reactive form value
   */
  static toFormData<T>(formValue: T) {
    const formData = new FormData();
    for (const key of Object.keys(formValue)) {
      formData.append(key, formValue[key]);
    }
    return formData;
  }



  ngOnInit() {

    let info = JSON.parse(sessionStorage.auth)
    if (info.client) {
      this.rfc = info.client.rfc
    }

    this.hrService
    .validateComponent({rfc:this.rfc, component:this.component})
        .subscribe({
          next: data => {

            this.userComponent = data

            if (this.userComponent.granted == true) {
              this.showCommunication = true;
            }

            if (!this.showCommunication){

              this.router.navigateByUrl('/home/my-dashboard');
              window.scroll({
                left: 0,
                behavior: 'smooth'
              });

            }    

          },
          error: error => { 
            console.log(error)    
          }
        })

    this.initForm();

  }

 
  initForm() {
    /* Form Elements */
    this.form = new FormGroup({
      massive_message: new FormControl('',[Validators.required,]),
      rfc: new FormControl(this.rfc)
    });
  }


  hasError(controlName: string, typeError: string): boolean {
    /* Error Detection */
    return this.form.get(controlName).hasError(typeError);
  }


  public onFormSubmit(e): void {
    e.preventDefault();

    const { form } = this;
    form.markAllAsTouched();
    this.formSubmitAttempt = true;

    if (form.invalid) {
      return;
    }

    let formData = form.getRawValue();

    //this.store.dispatch(saveFormData({ form: formData }));
    this.hrService
    .putMassiveMessage(formData)
    .subscribe(res => {
          
      this.openSnackBarSuccess(res.message,"Cerrar")
      this.ngOnInit();
      window.scroll({
        top: 0,
        left: 0,
        behavior: 'smooth'
      });
    },
    error => {
      this.openSnackBarError(error.error.error,"Cerrar")
      return
    })

    this.formSubmitAttempt = false;

  }

  openSnackBarSuccess(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 5000,
      horizontalPosition: "end",
      verticalPosition: "top",
      panelClass: ['success-snackbar'],
    });
  }

  openSnackBarError(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 5000,
      horizontalPosition: "end",
      verticalPosition: "top",
      panelClass: ['error-snackbar'],
    });
  }


}
