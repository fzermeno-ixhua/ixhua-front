import { Component, OnInit,ViewChild ,EventEmitter,Output} from '@angular/core';
import { Gtag } from 'angular-gtag';
import { ViewportScroller } from '@angular/common';
import { MatSidenav } from '@angular/material/sidenav';
@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['../../login/auth-styles/all.scss']
})
export class AboutUsComponent implements OnInit {
  public currentYear = new Date().getFullYear();
  @ViewChild('sidenav', { static: false} ) sidenav: MatSidenav;
  @Output() open: EventEmitter<any> = new EventEmitter();
  @Output() close: EventEmitter<any> = new EventEmitter();
  panelOpenState = false;
  constructor(
    public gtag : Gtag,
    private viewportScroller: ViewportScroller
  ) { }

  ngOnInit() {
  }

  _goToPosition(e: string, fromXS: boolean) {
    this.viewportScroller.scrollToAnchor(e);
    if(fromXS) {
      this.close.emit(false);
    }
  }



}