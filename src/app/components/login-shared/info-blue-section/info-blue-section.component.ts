import { Component, OnInit,ViewChild ,EventEmitter,Output} from '@angular/core';
import { Gtag } from 'angular-gtag';
import { ViewportScroller } from '@angular/common';
import { MatSidenav } from '@angular/material/sidenav';
@Component({
  selector: 'app-info-blue-section',
  templateUrl: './info-blue-section.component.html',
  styleUrls: ['../../login/auth-styles/all.scss']
})
export class InfoBlueSectionComponent implements OnInit {
  public currentYear = new Date().getFullYear();
  @ViewChild('sidenav', { static: false} ) sidenav: MatSidenav;
  @Output() open: EventEmitter<any> = new EventEmitter();
  @Output() close: EventEmitter<any> = new EventEmitter();
  panelOpenState = false;

  constructor(
    public gtag : Gtag,
    private viewportScroller: ViewportScroller
  ) { }

  ngOnInit() {
  }

}
