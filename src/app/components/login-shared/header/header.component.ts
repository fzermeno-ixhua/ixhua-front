import {Component, OnInit, Input, OnDestroy, ViewChild, EventEmitter, Output } from '@angular/core';
import { Router, Scroll } from '@angular/router';
import { ViewportScroller } from '@angular/common';
import { filter } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { MatSidenav } from '@angular/material/sidenav';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent implements OnInit, OnDestroy {

  links = ['Inicio','¿Cómo Funciona?','Empresas','Ayuda'];
  activeLink = this.links[0];


  scroll: any;
  @Input() routeValue: string;
  @Input() parentSubject:Subject<any>;
  @ViewChild('sidenav', { static: false} ) sidenav: MatSidenav;
  @Output() open: EventEmitter<any> = new EventEmitter();
  @Output() close: EventEmitter<any> = new EventEmitter();

  pathRoute:string = '';
  constructor(
    private router: Router,
    private viewportScroller: ViewportScroller
  ) { }

  ngOnInit() {
    this.checkRoute();
    this.scroll = this.router.events.pipe(filter(e => e instanceof Scroll));
    this.scroll.subscribe((e: any) => {
      if(e.anchor != null) {
        this.viewportScroller.scrollToAnchor(e.anchor);
      }
    });
    this._reciveFromParent();
   }

  ngOnDestroy(): void {
    this.scroll.unsubscribe();
    this.parentSubject.unsubscribe();
  }   

  checkRoute(){
    if(this.routeValue){
      console.log("You are in "+ this.routeValue + " route");
    }else {
      console.log("You are in auth route");
    }
  }

  _reciveFromParent() {
    this.parentSubject.subscribe(event => {
      if(event){
        this.close.emit(false);
        this.sidenav.close();        
      }
    });
  }

  _goToPosition(e: string, fromXS: boolean) {
    this.viewportScroller.scrollToAnchor(e);
    if(fromXS) {
      this.close.emit(false);
    }
  }

  sidenavOnClick() {
    this.sidenav.opened ? this.open.emit(true) : this.open.emit(false);
  }

  closeBack() {
    if(this.sidenav.opened) {
      this.open.emit(false);
      this.sidenav.close(); 
    }
  }

  scrollTo(x: number, y: number) {
    window.scroll({
      top: y,
      left: x,
      behavior: 'smooth'
    });
  }

  tabClick(tab: string, state: boolean) {
    if (tab == "Inicio") {
      this.viewportScroller.scrollToAnchor("start");
      if(state==true) {
        this.close.emit(false);
      }
    }

    if (tab == "¿Cómo Funciona?") {
      this.viewportScroller.scrollToAnchor("howworks");
      if(state==true) {
        this.close.emit(false);
      }
    }

    if (tab == "Empresas") {
      
      window.open('/empresas'); 
    }

    if (tab == "Ayuda") {
      this.router.navigateByUrl('/faqs');
    }
  }

}
