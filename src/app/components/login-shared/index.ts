export * from './header/header.component';
export * from './footer/footer.component';
export * from './contact/contact.component';
export * from './questions/questions.component';
export * from './how-works/how-works.component';
export * from './info-blue-section/info-blue-section.component';
export * from './about-us/about-us.component';
export * from './carousel/carousel.component';