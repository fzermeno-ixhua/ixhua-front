import { Component, OnInit,ViewChild ,EventEmitter,Output} from '@angular/core';
import { Gtag } from 'angular-gtag';
import { ViewportScroller } from '@angular/common';
import { MatSidenav } from '@angular/material/sidenav';
@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['../../login/auth-styles/all.scss']
})
export class FooterComponent implements OnInit {
  public currentYear = new Date().getFullYear();
  @ViewChild('sidenav', { static: false} ) sidenav: MatSidenav;
  @Output() open: EventEmitter<any> = new EventEmitter();
  @Output() close: EventEmitter<any> = new EventEmitter();

  constructor(
    public gtag : Gtag,
    private viewportScroller: ViewportScroller
  ) { }

  ngOnInit() {
  }

  gtagEventWhatsapp(){
    this.gtag.event('click', { 
      method: 'whatsapp',
      event_category: 'telefono',
      event_label: 'whatsapp'
    });
  }

  gtagEventMail(){
    this.gtag.event('click', { 
      method: 'email',
      event_category: 'correo',
      event_label: 'email'
    });
  }

  gtagEventCall(){
    this.gtag.event('click', { 
      method: 'llamada',
      event_category: 'telefono',
      event_label: 'llamada'
    });
  }

  _goToPosition(e: string, fromXS: boolean) {
    this.viewportScroller.scrollToAnchor(e);
    if(fromXS) {
      this.close.emit(false);
    }
  }

  closeBack() {
    if(this.sidenav.opened) {
      this.open.emit(false);
      this.sidenav.close(); 
    }
  }

  scrollTo(x: number, y: number) {
    window.scroll({
      top: y,
      left: x,
      behavior: 'smooth'
    });
  }

}
