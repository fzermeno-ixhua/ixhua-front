import { Component, OnInit,ViewChild ,EventEmitter,Output} from '@angular/core';
import { Gtag } from 'angular-gtag';
import { ViewportScroller } from '@angular/common';
import { MatSidenav } from '@angular/material/sidenav';
@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['../../login/auth-styles/all.scss']
})
export class CarouselComponent implements OnInit {
  public currentYear = new Date().getFullYear();
  @ViewChild('sidenav', { static: false} ) sidenav: MatSidenav;
  @Output() open: EventEmitter<any> = new EventEmitter();
  @Output() close: EventEmitter<any> = new EventEmitter();
  panelOpenState = false;
  public slideIndex = 1
  constructor(
    public gtag : Gtag,
    private viewportScroller: ViewportScroller
  ) { }

  ngOnInit() {
    this.showSlides(this.slideIndex);
  }

  _goToPosition(e: string, fromXS: boolean) {
    this.viewportScroller.scrollToAnchor(e);
    if(fromXS) {
      this.close.emit(false);
    }
  }


  // Next/previous controls
plusSlides(n) {
  this.showSlides(this.slideIndex += n);
}

// Thumbnail image controls
currentSlide(n) {
  this.showSlides(this.slideIndex = n);
}

showSlides(n) {
  var i;
  //var slides = document.getElementsByClassName("mySlides");
  var slides = Array.from(document.getElementsByClassName('mySlides') as HTMLCollectionOf<HTMLElement>)
  if (n > slides.length) {this.slideIndex = 1} 
  if (n < 1) {this.slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none"; 
  }
  
  slides[this.slideIndex-1].style.display = "block"; 
}

}