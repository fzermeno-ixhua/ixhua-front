import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatTooltipModule} from '@angular/material/tooltip';

import { environment } from '../../../environments/environment';
import { HrService } from 'src/app/services/hr.service';




@Component({
  selector: 'app-hr-myteam',
  templateUrl: './hr-myteam.component.html',
  styleUrls: ['./hr-myteam.component.scss']
})
export class HrMyteamComponent implements OnInit {

  rfc:any;
  component : string = 'collaborators';
  public showTeamList : boolean = false;
  public userComponent: any;
  public teamList: any;

  constructor(
    private hrService: HrService,
    private http: HttpClient,
    private router: Router
  ) {}


  ngOnInit() {

    let info = JSON.parse(sessionStorage.auth)
    if (info.client) {

      if(info.client.rfc){

        this.rfc = info.client.rfc

        /* Validate Service Allowed*/
        this.hrService
        .validateComponent({rfc:this.rfc, component:this.component})
            .subscribe({
              next: data => {

                this.userComponent = data

                if (this.userComponent.granted == true) {
                  // Service access granted
                  this.showTeamList = true;
                }

                if (!this.showTeamList){
                  // Service access forbiden
                  this.router.navigateByUrl('/home/my-dashboard');
                  window.scroll({
                    left: 0,
                    behavior: 'smooth'
                  });

                }
                
                /* Get Client Services allowed */
                this.hrService
                .getTeamList({rfc:this.rfc})
                .subscribe({
                  next: data => {

                    this.teamList = data.collaborators
                    
                  },
                  error: error => { 
                    console.log(error)    
                  }
                })

              },
              error: error => { 
                console.log(error)    
              }
            })

      }

    }

    
  }

}
