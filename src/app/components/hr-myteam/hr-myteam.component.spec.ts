import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HrMyteamComponent } from './hr-myteam.component';

describe('HrMyteamComponent', () => {
  let component: HrMyteamComponent;
  let fixture: ComponentFixture<HrMyteamComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HrMyteamComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HrMyteamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
