import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-first-login-dialog',
  templateUrl: './first-login-dialog.component.html',
  styleUrls: ['./first-login-dialog.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FirstLoginDialogComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
