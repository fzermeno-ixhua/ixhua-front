import { Component, OnInit } from '@angular/core';
import { MatDialog } from "@angular/material/dialog";
import { AccountDeletionModalComponent } from './account-deletion-modal.component';
import { Store } from "@ngrx/store";
import { State, deleteAccount } from "../../store/reducers";
import {Router} from "@angular/router"
import { ClientService } from 'src/app/services/client.service';
import { AccountDeletionFailedModalComponent } from "./account-deletion-failed-modal.component";
import { logout } from 'src/app/store/actions';

export interface DialogData {
  error: string;
}

@Component({
  selector: 'app-options',
  templateUrl: './options.component.html',
  //styleUrls: ['./options.component.scss']
})

export class OptionsComponent implements OnInit {
  constructor(
    private dialog: MatDialog,
    private store: Store<State>,
    private router: Router,
    private clientService: ClientService,
  ) {  }

  ngOnInit() {  }

  public onDeleteAccount(): void {
    const dialogReference = this.dialog.open(AccountDeletionModalComponent, {
      width: '870px',
      disableClose: true
    });

    dialogReference
      .afterClosed()
      .subscribe(({ userConfirmedDeletion }) => {
        if (!userConfirmedDeletion) {
          console.log('The user is not sure about deleting their account...');
          return;
        }

        let info = JSON.parse(sessionStorage.auth)
        if (info.client) {
          if(info.client.rfc){
            let body =  {rfc:info.client.rfc}

            this.clientService
            .cancelClientAccount(body)
            .subscribe({
              next: data => {
                console.log(data)
                this.store.dispatch(logout());
              },   
              error: error => { 
                this.dialog.open(AccountDeletionFailedModalComponent, {
                  width: '870px',
                  disableClose: true,
                  data: {error: error.error.error},
                });
              }
            
            })
          }
        }
      });
  }

  public onChangePassword(): void {
    this.router.navigate(['/home/change'])
  }

}
