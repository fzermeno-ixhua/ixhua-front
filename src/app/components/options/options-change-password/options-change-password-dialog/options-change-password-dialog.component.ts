import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'options-change-password-diaglog',
  templateUrl: './options-change-password-dialog.component.html',
  styleUrls: ['./options-change-password-dialog.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class OptionsChangePasswordDialogComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
