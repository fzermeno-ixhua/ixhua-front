import { Component, OnInit } from '@angular/core';
import { MatDialog } from "@angular/material/dialog";
import { Store } from "@ngrx/store";
import { State, deleteAccount } from "./../../../store/reducers";
import {Router} from "@angular/router"
import { FormControl, FormGroup,Validators,ValidatorFn,AbstractControl} from '@angular/forms';
import { environment } from '../../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { OptionsChangePasswordDialogComponent } from 'src/app/components/options/options-change-password/options-change-password-dialog/options-change-password-dialog.component';
@Component({
  selector: 'app-options',
  templateUrl: './options-change-password.component.html',
  styleUrls: ['./../options.component.scss']
})
export class OptionsChangePasswordComponent implements OnInit {
   
  public form = new FormGroup({
    oldPassword: new FormControl('', [
      Validators.required,
      Validators.minLength(8),
    ]),
    newPassword: new FormControl('', [
      Validators.required,
      Validators.minLength(8),
      ixhuaPasswordValidator()
    ]),
    confirmPassword: new FormControl('', [
      Validators.required,
      Validators.minLength(8),
    ]),
  });

  private API_URL = environment.API_URL;
  public general_error:any;

  constructor(
    private dialog: MatDialog,
    private store: Store<State>,
    private router: Router,
    private http: HttpClient,
  ) {  }

  ngOnInit() {  }

  public get oldPasswordFormatError() {
    const { form } = this;

    if (form.controls.oldPassword.hasError('passwordValidation')) {
      return form.controls.oldPassword.errors.passwordValidation.value;
    }

    if (form.controls.oldPassword.hasError('minlength')) {
      return 'La contraseña debe tener por lo menos 8 caracteres.';
    }

    if (form.controls.oldPassword.hasError('required')) {
      return 'Este campo es requerido';
    }

    return '&nbsp;';
  }

  public get newPasswordFormatError() {
    const { form } = this;

    if (form.controls.newPassword.hasError('passwordValidation')) {
      return form.controls.newPassword.errors.passwordValidation.value;
    }

    if (form.controls.newPassword.hasError('minlength')) {
      return 'La contraseña debe tener por lo menos 8 caracteres.';
    }

    if (form.controls.newPassword.hasError('required')) {
      return 'Este campo es requerido';
    }

    return '&nbsp;';
  }

  public get confirmPasswordFormatError() {
    const { form } = this;

    if (form.controls.confirmPassword.hasError('passwordValidation')) {
      return form.controls.oldPassword.errors.passwordValidation.value;
    }

    if (form.controls.confirmPassword.hasError('minlength')) {
      return 'La contraseña debe tener por lo menos 8 caracteres.';
    }

    if (form.controls.confirmPassword.hasError('required')) {
      return 'Este campo es requerido';
    }

    return '&nbsp;';
  }

  onChangePassword(){
    const { form } = this;
    form.markAllAsTouched();
    if (!form.valid) {
      return;
    }
    let info = JSON.parse(sessionStorage.auth)
    let body = {rfc:info.client.rfc, oldPassword:form.value.oldPassword,newPassword:form.value.newPassword,confirmPassword:form.value.confirmPassword}

    this.http.post(`${this.API_URL}/api/v1/client/change_password/`, body)
    .subscribe({
      next: data => {
        this.general_error=""
        const dialogRef = this.dialog.open(OptionsChangePasswordDialogComponent, {
        width: '600px',
        panelClass: 'option-change-password-dialog',
        disableClose: true
        });
        this.router.navigate(['/home/'])
        setTimeout(() => dialogRef.close(), 5 * 1000);
      },
      error: error => {
        console.log(error.error.error)
        this.general_error = error.error.error;
      }
    })

  }

}


/**
 * @description Custom validator function
 * to match Ixhua password requirements
 */
function ixhuaPasswordValidator(): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} | null => {
    const { value } = control;
    const upperCaseRegex = /[A-Z]/;
    const symbolRegex = /[\$\%\&\/\(\)\[\]\¡\!\¿\?\=\#\@\*\^\|\-\.\,\:\;\\]/;

    if (!upperCaseRegex.test(value)) {
      return { passwordValidation: { value: 'La contraseña debe incluir una mayúscula.' } };
    }

    if (!symbolRegex.test(value)) {
      return { passwordValidation: { value: 'La contraseña debe incluir por lo menos un símbolo.' } };
    }
    return null;
  };
}
