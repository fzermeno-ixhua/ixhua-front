import { Component, ViewEncapsulation } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-auth-forgot-password',
  template: `
    <div class="mat-dialog-delete text-center">
      <h2 class="my-1">Confirmación</h2>
      <hr>
      <div class="mb-30px mt-30px">
          <span class="fs-24px fw-300">¿Está seguro de querer cancelar su cuenta?</span>
      </div>
      <div class="mat-dialog-delete__buttons flex__completely-centered"> 
        <button class="fs-24px mat-rounded-button"
                mat-flat-button
                (click)="onAction()">
            Aceptar
        </button>
        <button class="fs-24px mat-rounded-button"
                mat-flat-button
                color="primary"
                (click)="onClose()">
          Cancelar
        </button>      
      </div>
    </div>
  `,
  encapsulation: ViewEncapsulation.None
})
export class AccountDeletionModalComponent {
  constructor(
    private dialogRef: MatDialogRef<AccountDeletionModalComponent>
  ) {  }

  onClose() {
    this.dialogRef.close({ userConfirmedDeletion: false });
  }

  onAction() {
    this.dialogRef.close({ userConfirmedDeletion: true });
  }
}
