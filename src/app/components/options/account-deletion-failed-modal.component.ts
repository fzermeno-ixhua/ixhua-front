import { Component, ViewEncapsulation,Inject} from '@angular/core';
import { MatDialogRef,MAT_DIALOG_DATA } from '@angular/material';
import { DialogData } from "./options.component";

@Component({
  selector: 'app-auth-forgot-password',
  template: `
    <div class="mat-dialog-delete text-center">
      <h2 class="my-1">La cuenta no puede ser cancelada</h2>
      <hr>
      <div class="mb-30px mt-30px">
          <span class="fs-24px fw-300">{{data.error}}</span>
          <p class="fs-24px fw-300">Ponte en contacto con nosotros. contacto@ixhua.mx</p>

      </div>
      <div class="mat-dialog-delete__buttons flex__completely-centered"> 
        <button class="fs-24px mat-rounded-button"
                mat-flat-button
                (click)="onClose()">
            Aceptar
        </button>
      </div>
    </div>
  `,
  encapsulation: ViewEncapsulation.None
})
export class AccountDeletionFailedModalComponent {
  constructor(
    private dialogRef: MatDialogRef<AccountDeletionFailedModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
  ) {  }

  onClose(): void {
    this.dialogRef.close();
  }
}
