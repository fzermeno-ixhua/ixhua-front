import { Component, OnDestroy, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Store } from '@ngrx/store';
import { selectClientLoans, State } from '../../store/reducers';
import { LoanInfo } from 'src/app/models';
import { Subscription } from 'rxjs';
import { fetchClientLoans } from '../../store/actions';

@Component({
  selector: 'app-my-loans',
  templateUrl: './my-loans.component.html',
  styleUrls: ['./my-loans.component.scss']
})
export class MyLoansComponent implements OnInit, OnDestroy {
  private static STATUSES = {
    Vigente: {
      label: 'vigente',
      description:
        'Recuerda pagar de manera puntual para ganar semillas y mejorar tus condiciones en los próximos créditos.',
      colorClass: 'fc-blue'
    },
    'Pago puntual': {
      label: 'pago puntual',
      description: '¡FELICIDADES! Ganaste una semilla verde / dorada.',
      colorClass: 'fc-success'
    },
    'Pago atrasado': {
      label: 'pago atrasado',
      description: 'Perdiste una semilla debido a que recibimos tu pago @days@ días después de tu fecha de pago. '
        + 'Recuerda pagar de manera puntual para ganar semillas y mejorar tus condiciones en los próximos créditos.',
      colorClass: 'fc-turbo'
    },
    Moroso: {
      label: 'moroso',
      description:
        'Te pedimos nos contactes para ayudarte a agendar tu pago de manera inmediata.',
      colorClass: 'fc-warn'
    },
    'En tramite': {
      label: 'en trámite',
      description: 'Tu solicitud está siendo revisada.',
      colorClass: 'fc-blue'
    },
    Rechazado: {
      label: 'rechazado',
      description:
        'La información que proporcionaste en tu solicitud, no coincide con nuestra base de datos.',
      colorClass: 'fc-tomato'
    }
  };
  private storeSubscription: Subscription;
  public clientLoans: LoanInfo[] = [];

  constructor(
    private sanitizer: DomSanitizer,
    private store: Store<State>
  ) {  }

  ngOnInit(): void {
    this.storeSubscription =
      this.store.select<LoanInfo[]>(selectClientLoans)
        .subscribe(loans => this.clientLoans = loans);

    this.store.dispatch(fetchClientLoans());
  }

  ngOnDestroy(): void {
    this.storeSubscription.unsubscribe();
  }

  getStatus(status: string, loanObject: LoanInfo) {

    // TODO: Refactor this function to only receive one parameter
    const { STATUSES: statusesList } = MyLoansComponent;
    let statusLabel = statusesList[status].description;

    // Morosos
    if (status === 'Pago atrasado') {
      const { delayed_days } = loanObject;
      statusLabel = statusLabel.replace('@days@', delayed_days.toString());
    }

    //si el estatus es rechazado tiene que mostrar el mensaje ingresado en el admin
    if (status === 'Rechazado') {
      if (loanObject.reason_for_rejection) {
        statusLabel = loanObject.reason_for_rejection
      }
    }

    const statusHtml = `
      <span class="${statusesList[status].colorClass} fw-normal fs-12px">
        ${statusesList[status].label.toUpperCase()}:
      </span> ${statusLabel}`;
    return this.sanitizer.bypassSecurityTrustHtml(statusHtml);
  }
}
