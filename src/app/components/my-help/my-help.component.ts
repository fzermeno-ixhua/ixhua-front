import { Component, OnDestroy, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Store } from '@ngrx/store';
import { selectClientLoans, State } from '../../store/reducers';
import { LoanInfo } from 'src/app/models';
import { Subscription } from 'rxjs';
import { fetchClientLoans } from '../../store/actions';

@Component({
  selector: 'app-my-help',
  templateUrl: './my-help.component.html',
})
export class MyHelpComponent implements OnInit, OnDestroy {
  
  

  constructor(
    private sanitizer: DomSanitizer,
    private store: Store<State>
  ) {  }

  ngOnInit(): void {
    
  }

  ngOnDestroy(): void {
    
  }

 
}