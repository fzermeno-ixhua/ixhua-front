import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { SubSink } from 'subsink';
import { Store } from '@ngrx/store';
import { selectLoanState, State } from 'src/app/store/reducers';
import {
  selectClientForm,
  selectFamiliesList,
  selectZipData,
  State as LoanState,
  selectLoanRequest,
  selectContractData,
} from 'src/app/store/reducers/loan-request.reducer';
import { map } from 'rxjs/operators';

import { LoanService } from 'src/app/services/loan.service';
import { ClientService } from 'src/app/services/client.service';
import { Router } from '@angular/router';
import { environment } from '../../../../environments/environment';
import { HttpClient } from '@angular/common/http';

import { ContractData, LoanRequest } from 'src/app/models/interfaces';
import { loanRequest } from 'src/app/store/actions';
import { Subscription } from 'rxjs';
import {MatSnackBar} from '@angular/material/snack-bar';


@Component({
  selector: 'app-loan-request-documents',
  templateUrl: './loan-request-documents.component.html'
})


export class LoanRequestDocumentsComponent implements OnInit, OnDestroy {
  
  rfc:any;
  public static ALLOWED_FILE_TYPES = [
    'image/png',
    'image/jpeg',
    'image/jpg',
    'application/pdf'
  ];
  form: FormGroup;
  filesForm: FormGroup;
  requiredMessage = 'Este campo es requerido';
  invalidEmailMessage = 'El formato es invalido';
  invalidFileMessage = 'El archivo que intentaste cargar excede el tamaño permitido o no es válido. Por favor verifícalo';
  
  formFamiliesList = [];
  
  isFormUploadingFiles = false;
  uploadFilesError = false;

  public isFirstLoan = true;
  private subs = new SubSink();
  private API_URL = environment.API_URL;
  public general_error:any;
  public clientDocuments: any;
  public clientLoanRequirements: any;


  //ine
  public showIneReq = true
  public showIne = false;
  public fileIneAux : any = null;
  public fileIneDate : any = true;
  //additional_file
  public showAdditionalFileReq = true;
  public showAdditionalFile = false;
  public fileAdditionalFileAux : any = null;
  public fileAdditionalDate : any = true;
  //bank account
  public showBankAccountReq = true;
  public showBankAccount = false;
  public fileBankAccountAux : any = null;
  public fileBankAccountDate : any = true;
  // proof address
  public showProofAddressReq = true;
  public showProofAddress = false;
  public fileProofAddressAux : any = null;
  public fileProofAddressDate : any = true;
  // ine-bankaccount-proofaddress
  public showThirdRowReq = true;
  //all paysheets section
  public showPaysheetsReq = true;
  //paysheet one
  public showPaysheetOneReq= true;
  public showPaysheetOne= false;
  public filePaysheetOneAux : any = null;
  public filePaysheetOneDate: any = true;
  //paysheet two
  public showPaysheetTwoReq= true;
  public showPaysheetTwo= false;
  public filePaysheetTwoAux : any = null;
  public filePaysheetTwoDate : any = true;
  //paysheet three
  public showPaysheetThreeReq= true;
  public showPaysheetThree= false;
  public filePaysheetThreeAux : any = null;
  public filePaysheetThreeDate : any = true;
  //selfie
  public showSelfieReq= true;
  public showSelfie= false;
  public fileSelfieAux : any = null;
  public fileSelfieDate : any = true;
  //clientType
  public showClientType = false
  


  private loanRequest: LoanRequest;
  private storeSubscription: Subscription;
  public errorMessage:any;


  constructor(
    private store: Store<State>,
    private loanService: LoanService,
    private clientService: ClientService,
    private router: Router,
    private http: HttpClient,
    private _snackBar: MatSnackBar,
    private ClientService: ClientService,
  ) {}

  /**
   * @description Converts a angular reactive form value to FormData
   *  in order to be sent over http request.
   * @param formValue The reactive form value
   */
  static toFormData<T>(formValue: T) {
    const formData = new FormData();
    for (const key of Object.keys(formValue)) {
      formData.append(key, formValue[key]);
    }
    return formData;
  }

  ngOnInit() {

    this.initForm();

    this.storeSubscription =
      this.store.select<LoanState>(selectLoanState)
        .pipe(
          map(state => ({   
            loan: selectLoanRequest(state),
          }))
        ).subscribe(({loan}) => {
          //this.disableRequests = true;
          // @ts-ignore
          this.generatedContractData = {
            ...loan.client,
          };
          this.loanRequest = loan;
        });

 

    this.loanService
      .getClientDocuments({rfc:this.loanRequest.client.rfc})
      .subscribe({
        next: data => {
          this.clientDocuments = data
          this.showClientType=this.clientDocuments.client_type

          if (this.clientDocuments.ine) {
            this.showIne = true;
            this.fileIneAux = this.clientDocuments.ine;
            this.fileIneDate = this.clientDocuments.ine_date
            this.filesForm.get('ine').clearValidators();
            this.filesForm.get('ine').updateValueAndValidity();         
          }

          if (this.clientDocuments.additional_file) {
            this.showAdditionalFile = true;
            this.fileAdditionalDate = this.clientDocuments.additional_file_date
            this.fileAdditionalFileAux = this.clientDocuments.additional_file; 
          }

          if (this.clientDocuments.bank_account) {
            this.showBankAccount = true;
            this.fileBankAccountAux = this.clientDocuments.bank_account;
            this.fileBankAccountDate = this.clientDocuments.bank_account_date;
            this.filesForm.get('bank_account').clearValidators();
            this.filesForm.get('bank_account').updateValueAndValidity();         
          }

          if (this.clientDocuments.proof_address) {
            this.showProofAddress = true;
            this.fileProofAddressAux = this.clientDocuments.proof_address;
            this.fileProofAddressDate = this.clientDocuments.proof_address_date;
            this.filesForm.get('proof_address').clearValidators();
            this.filesForm.get('proof_address').updateValueAndValidity();         
          }

          if ((this.clientDocuments.paysheet_one) || (this.clientDocuments.client_type == false)) {
            this.showPaysheetOne = true;
            this.filePaysheetOneAux = this.clientDocuments.paysheet_one;
            this.filePaysheetOneDate = this.clientDocuments.paysheet_one_date;
            this.filesForm.get('paysheet_one').clearValidators();
            this.filesForm.get('paysheet_one').updateValueAndValidity();         
          }

          if ((this.clientDocuments.paysheet_two) || (this.clientDocuments.client_type == false)) {
            this.showPaysheetTwo = true;
            this.filePaysheetTwoAux = this.clientDocuments.paysheet_two;
            this.filePaysheetTwoDate = this.clientDocuments.paysheet_two_date;
            this.filesForm.get('paysheet_two').clearValidators();
            this.filesForm.get('paysheet_two').updateValueAndValidity();         
          }

          if ((this.clientDocuments.paysheet_three) || (this.clientDocuments.client_type == false)) {
            this.showPaysheetThree = true;
            this.filePaysheetThreeAux = this.clientDocuments.paysheet_three;
            this.filePaysheetThreeDate = this.clientDocuments.paysheet_three_date;
            this.filesForm.get('paysheet_three').clearValidators();
            this.filesForm.get('paysheet_three').updateValueAndValidity();         
          }

          if ((this.clientDocuments.selfie) || (this.clientDocuments.client_type == false)) {
            this.showSelfie = true;
            this.fileSelfieAux = this.clientDocuments.selfie;
            this.fileSelfieDate = this.clientDocuments.selfie_date;
            this.filesForm.get('selfie').clearValidators();
            this.filesForm.get('selfie').updateValueAndValidity();         
          }

        },   
        error: error => { 
          console.log(error)    
        }                 
      })


    this.loanService
      .getClientLoanRequirements({rfc:this.loanRequest.client.rfc})
      .subscribe({
        next: data => {
          this.clientLoanRequirements = data

          if (!this.clientLoanRequirements.ine && !this.clientLoanRequirements.bank_account && !this.clientLoanRequirements.proof_address){
            this.showThirdRowReq = false;
          }

          if (!this.clientLoanRequirements.ine) {
            this.showIneReq = false;
            this.filesForm.controls.ine.disable();
          }

          if (!this.clientLoanRequirements.bank_account){
            this.showBankAccountReq = false;
            this.filesForm.controls.bank_account.disable();
          }

          if (!this.clientLoanRequirements.proof_address){
            this.showProofAddressReq = false;
            this.filesForm.controls.proof_address.disable();   
          }

          if (!this.clientLoanRequirements.paysheet_one && !this.clientLoanRequirements.paysheet_two && !this.clientLoanRequirements.paysheet_three){
            this.showPaysheetsReq = false;
          }

          if (!this.clientLoanRequirements.paysheet_one){
            this.showPaysheetOneReq = false;
            this.filesForm.controls.paysheet_one.disable();
          }

          if (!this.clientLoanRequirements.paysheet_two){
            this.showPaysheetTwoReq = false;
            this.filesForm.controls.paysheet_two.disable();
          }

          if (!this.clientLoanRequirements.paysheet_three){
            this.showPaysheetThreeReq = false;
            this.filesForm.controls.paysheet_three.disable();
          }

          if (!this.clientLoanRequirements.selfie){
            this.showSelfieReq = false;
            this.filesForm.controls.selfie.disable();
          }

          if (!this.clientLoanRequirements.additional_file || !this.clientLoanRequirements.ine){
            this.showAdditionalFileReq = false;
            this.filesForm.controls.additional_file.disable();
          }

        },
        error: error => { 
          console.log(error)    
        }
      })
   

  }


  ngOnDestroy() {
    this.subs.unsubscribe();
  }



  initForm() {
    this.filesForm = new FormGroup({
      ine: new FormControl(null,[Validators.required]),
      bank_account: new FormControl(null,[Validators.required]),
      proof_address: new FormControl(null,[Validators.required]),
      additional_file: new FormControl(null),
      paysheet_one: new FormControl(null,[Validators.required]),   
      paysheet_two: new FormControl(null,[Validators.required]),
      paysheet_three: new FormControl(null,[Validators.required]),
      selfie: new FormControl(null,[Validators.required]),
      rfc: new FormControl(null),
      notes: new FormControl(null),
    });
  }



  public onFileChange(file, name: string): void {
    const { type, size } = file;
    const sizeInMb = size / 1024 / 1024;
    this.filesForm.get(name).markAsTouched();
    if (LoanRequestDocumentsComponent.ALLOWED_FILE_TYPES.indexOf(type) === -1) {
      this.filesForm.get(name).setErrors({ invalidFileType: true });
      return;
    }
    if (sizeInMb > 20) {
      this.filesForm.get(name).setErrors({ invalidFileType: true });
      return;
    }
    this.filesForm.get(name).setValue(file);
    this.filesForm.get(name).setErrors(null);
  }
  

  public changeFile(name,status){
    if (name == 'ine'){
      this.showIne = status;
      this.fileIneDate = true;
      this.filesForm.get('ine').setValidators([Validators.required]);
      this.filesForm.get('ine').updateValueAndValidity();
    }

    if (name == 'additional_file'){
      this.showAdditionalFile = status;
      this.fileAdditionalDate = true;
    }

    if (name == 'bank_account'){
      this.showBankAccount = status;
      this.fileBankAccountDate = true;
      this.filesForm.get('bank_account').setValidators([Validators.required]);
      this.filesForm.get('bank_account').updateValueAndValidity();
    }

    if (name == 'proof_address'){
      this.showProofAddress = status;
      this.fileProofAddressDate = true;
      this.filesForm.get('proof_address').setValidators([Validators.required]);
      this.filesForm.get('proof_address').updateValueAndValidity();
    }

    if (name == 'paysheet_one'){
      this.showPaysheetOne = status;
      this.filePaysheetOneDate = true;
      this.filesForm.get('paysheet_one').setValidators([Validators.required]);
      this.filesForm.get('paysheet_one').updateValueAndValidity();
    }

    if (name == 'paysheet_two'){
      this.showPaysheetTwo = status;
      this.filePaysheetTwoDate = true;
      this.filesForm.get('paysheet_two').setValidators([Validators.required]);
      this.filesForm.get('paysheet_two').updateValueAndValidity();
    }

    if (name == 'paysheet_three'){
      this.showPaysheetThree = status;
      this.filePaysheetThreeDate = true;
      this.filesForm.get('paysheet_three').setValidators([Validators.required]);
      this.filesForm.get('paysheet_three').updateValueAndValidity();
    }

    if (name == 'selfie'){
      this.showSelfie = status;
      this.fileSelfieDate = true;
      this.filesForm.get('selfie').setValidators([Validators.required]);
      this.filesForm.get('selfie').updateValueAndValidity();
    }

  }
 


  public onFormSubmit(): void {
    
    const { form, filesForm } = this;
    filesForm.markAllAsTouched();

    if (filesForm.invalid) {
      this.isFormUploadingFiles = false;
      return;
    }

    if (this.fileIneDate == false || this.fileAdditionalDate == false || this.fileBankAccountDate == false || this.fileProofAddressDate == false || this.filePaysheetOneDate == false || this.filePaysheetTwoDate == false || this.filePaysheetThreeDate == false || this.fileSelfieDate == false  ) {
      this.isFormUploadingFiles = false;
      this.general_error = 'Uno o más documentos están vencidos, actualizalos para guardar'
      return;
    }

    this.uploadFilesError = false;
    this.isFormUploadingFiles = true;   
    this.filesForm.patchValue({rfc: this.loanRequest.client.rfc});
    this.clientService
      .UploadClientFiles(LoanRequestDocumentsComponent.toFormData(filesForm.value))
          .subscribe(res => {
            // As original unmatched data consistency, the array of
            // credit lines must be joined into a single CSV string in order to be sent
            this.isFormUploadingFiles = false;
            this.uploadFilesError = false;
            this.router.navigateByUrl('/home/contract');
            window.scroll({
              top: 0,
              left: 0,
              behavior: 'smooth'
            });
            
            if (!res.success) {
              this.uploadFilesError = true;
              this.isFormUploadingFiles = false;
            }
          },
          error => {
            this.uploadFilesError = true;
            this.isFormUploadingFiles = false;
            console.error('File upload request failed ', error)
          });    
  }


  public deleteFile(name,status){

    let body = {rfc:this.loanRequest.client.rfc,document_type:name}
    this.isFormUploadingFiles = true
    this.ClientService
    .DeleteClientFiles(body)
    .subscribe({
      next: data => {
        this.general_error = "" 
        this.isFormUploadingFiles = false
        this.changeFile(name,status)
        this.openSnackBar("El archivo fue eliminado","Cerrar")
      },
      
      error: error => { 
        this.general_error = error.error.error;
        this.isFormUploadingFiles = false 
        this.openSnackBar("Error,El archivo no pudo ser eliminado","Cerrar")
      }
    
    })

  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 5000,
    });
  }

}
