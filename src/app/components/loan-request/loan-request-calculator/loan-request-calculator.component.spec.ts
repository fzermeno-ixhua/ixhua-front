import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoanRequestCalculatorComponent } from './loan-request-calculator.component';

describe('LoanRequestCalculatorComponent', () => {
  let component: LoanRequestCalculatorComponent;
  let fixture: ComponentFixture<LoanRequestCalculatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoanRequestCalculatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoanRequestCalculatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
