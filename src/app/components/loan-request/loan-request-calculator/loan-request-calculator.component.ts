import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import {FormControl, Validators,FormGroup} from '@angular/forms';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Subject, Subscription } from 'rxjs';
import { debounceTime, map, filter, tap, distinctUntilChanged } from 'rxjs/operators';
import { State, selectLoanState, selectClient } from 'src/app/store/reducers';
import {
  State as LoanState, selectLoanOptions,
  selectMinLoan, selectMaxLoan,
  selectDaysWorked, selectDaysToLoan,
} from 'src/app/store/reducers/loan-request.reducer';
import { loanOptions, saveLoanOptions } from 'src/app/store/actions';
import { Gtag } from 'angular-gtag';

@Component({
  encapsulation: ViewEncapsulation.None,
  selector: 'app-loan-request-calculator',
  styleUrls: ['./loan-request-calculator.component.scss'],
  templateUrl: './loan-request-calculator.component.html'
})
export class LoanRequestCalculatorComponent implements OnInit, OnDestroy {
  public selectedOption = 1;
  public loanOptions: {
    option?: string;
    aperture?: number;
    interest?: number;
    total?: number;
    cat?: number;
    tfa?: number;
  }[] = [];
  public daysWorked = NaN;
  public daysToLoan = 0;
  public dateToLoan: Date = null;
  public minLoan = NaN;
  public maxLoan = NaN;
  public sharedValue = NaN;
  public sharedValueFormControl = new FormControl(this.sharedValue, [
    Validators.min(this.minLoan),
    Validators.max(this.maxLoan)
  ]);
  public limitReached: boolean = false 

  
  /*public form_code = new FormGroup({
    
    have_code: new FormControl('No', [Validators.required]),
    code: new FormControl(null),
 
  });*/

  public changeSubject = new Subject<number>();
  public disableNextButton = false;
  private loanSubscription: Subscription;
  private clientSubscription: Subscription;
  public clientName = '';

  public loanOptionsHide: any = true;

  constructor(
    private store: Store<State>,
    private router: Router,
    public gtag : Gtag,
  ) {  }

  public static formatNumberToCurrency = (value: string) => `$ ${value.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}.00`;

  ngOnInit(): void {
    // Subscription for the subject when the value changes
    // either on the input or the slider

    this.changeSubject
      .asObservable()
      .pipe(
        filter((value) => !Number.isNaN(value)),
        distinctUntilChanged(),
      )
      .subscribe(value => {
        this.store.dispatch(loanOptions({ amount: value }));
        this.sharedValue = value;
      });

    this.clientSubscription = this.store.select(selectClient)
      .pipe(map(client => client.first_name))
      .subscribe(name => this.clientName = name);

    // Subscription to the store to get the current max loan
    // for the client & its loan options
    this.loanSubscription = this.store
      .select<LoanState>(selectLoanState)
      .pipe(
        map<LoanState, {
          maxLoan: number, minLoan: number, daysWorked: number, daysToLoan: number, loanOptions: any[], disableCalculatorNextButton: boolean
        }>(state => ({
          minLoan: selectMinLoan(state),
          maxLoan: selectMaxLoan(state),
          daysWorked: selectDaysWorked(state),
          daysToLoan: selectDaysToLoan(state),
          loanOptions: selectLoanOptions(state),
          disableCalculatorNextButton: state.disableCalculatorNextButton
        })),
        debounceTime(850)
      )
      .subscribe(({
        maxLoan: max, minLoan: min, daysWorked, daysToLoan, loanOptions: options, disableCalculatorNextButton: disableNextButton
      }) => {
        this.sharedValueFormControl.setValidators([
          Validators.min(this.minLoan),
          Validators.max(this.maxLoan),
        ]);
        this.daysWorked = daysWorked;
        this.daysToLoan = daysToLoan;
        this.dateToLoan = new Date();
        this.dateToLoan.setDate(this.dateToLoan.getDate() + this.daysToLoan);
        this.minLoan = Math.ceil(min);
        this.maxLoan = Math.floor(max);

        if( this.minLoan > this.maxLoan ){
          this.limitReached = true
        }

        if (!this.sharedValue) {
          if (this.maxLoan > this.minLoan) {
            this.sharedValue = Math.max(this.minLoan, Math.floor(this.maxLoan / 4 * 3));  
            this.sharedValue=Math.ceil(this.sharedValue/50)*50;       
          }
          else{
            this.sharedValue = this.minLoan;
          }
        }



        this.loanOptions = options;
        this.selectedOption = this.loanOptions.length  
        if (this.loanOptions.length == 1) {
          this.loanOptionsHide = true
        }
        else{
          this.loanOptionsHide = false
        }

        this.disableNextButton = disableNextButton;
        this.changeSubject.next(this.sharedValue);
      });
    this.changeSubject.next(this.sharedValue);

    document.documentElement.scrollTop = 0;

  }

  ngOnDestroy(): void {
    this.changeSubject.complete();
    this.loanSubscription.unsubscribe();
  }

  public trackOptions(index: number, option: any): string | undefined {
    if (!option) {
      return undefined;
    }
    return option.date;
  }

  public formatLabel(value: number | null) {
    if (!value) { return '$ 0.00'; }
    return `$ ${value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')}.00`;
  }

  public onSliderValueChanges(value) {
    this.changeSubject.next(value);
    this.sharedValueFormControl.updateValueAndValidity({
      onlySelf: true
    });
    this.sharedValueFormControl.setValue(value);
  }

  public onInputValueChanges(target: EventTarget) {
    // @ts-ignore
    let { value } = target;
    const { maxLoan, minLoan } = this;
    value = value
      .replace('$', '')
      .replace(/,/g, '');

    value=Math.ceil(Number(value)/1)*1;

    const inputNumber = Number(value);
    this.sharedValueFormControl.setValue(inputNumber);
    if (inputNumber > maxLoan) {
      this.sharedValueFormControl.setErrors({ max: true });
      // @ts-ignore
      target.value = LoanRequestCalculatorComponent.formatNumberToCurrency(`${maxLoan}`);
      this.changeSubject.next(maxLoan);
      return;
    } else if (inputNumber < minLoan) {
      this.sharedValueFormControl.setErrors({ min: true });
      // @ts-ignore
      target.value = LoanRequestCalculatorComponent.formatNumberToCurrency(`${minLoan}`);
      this.changeSubject.next(minLoan);
      return;
    }
    this.changeSubject.next(inputNumber);
  }

  public onSubmit(): void {
    const { selectedOption: option, sharedValue: amount } = this;
    if (amount <= 0 || !option) {
      return;
    }
    this.gtag.event('click', { 
      method: 'simulador',
      event_category: 'boton',
      event_label: 'simulador'
    });
    this.store.dispatch(saveLoanOptions({ option, amount }));
    this.router.navigateByUrl('/home/form');
  }
}
