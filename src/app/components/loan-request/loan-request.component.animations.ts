import {
    transition,
    trigger,
    query,
    style,
    animate,
    group,
    animateChild
} from '@angular/animations';


export const slideInAnimation =
    trigger('routeAnimations', [
        transition('Contract => *', [
            query(':enter, :leave', style({ position: 'absolute', width: '100%', height: '100%' }), { optional: true }),
            group([
                query(':enter', [
                    style({ transform: 'translateY(-100%)', opacity: 0}),
                    animate('0.5s ease-in-out', style({ transform: 'translateY(0%)' }))
                ], { optional: true }),
                query(':leave', [
                    style({ transform: 'translateY(0%)' , opacity: 0}),
                    animate('0.5s ease-in-out', style({ transform: 'translateY(100%)' }))
                ], { optional: true }),
            ])
        ]),
        transition('Calculator => *', [
            query(':enter, :leave', style({ position: 'absolute', width: '100%', height: '100%' }), { optional: true }),
            group([
                query(':enter', [
                    style({ transform: 'translateY(100%)', opacity: 0 }),
                    animate('0.5s ease-in-out', style({ transform: 'translateY(0%)' }))
                ], { optional: true }),
                query(':leave', [
                    style({ transform: 'translateY(0%)', opacity: 0 }),
                    animate('0.5s ease-in-out', style({ transform: 'translateY(-100%)' }))
                ], { optional: true }),
            ])
        ]),
        transition('Documents => *', [
            query(':enter, :leave', style({ position: 'absolute', width: '100%', height: '100%' }), { optional: true }),
            group([
                query(':enter', [
                    style({ transform: 'translateY(100%)', opacity: 0 }),
                    animate('0.5s ease-in-out', style({ transform: 'translateY(0%)' }))
                ], { optional: true }),
                query(':leave', [
                    style({ transform: 'translateY(0%)', opacity: 0 }),
                    animate('0.5s ease-in-out', style({ transform: 'translateY(-100%)' }))
                ], { optional: true }),
            ])
        ]),
        transition('Form => Contract', [
            query(':enter, :leave', style({ position: 'absolute', width: '100%', height: '100%' }), { optional: true }),
            group([
                query(':enter', [
                    style({ transform: 'translateY(100%)', opacity: 0 }),
                    animate('0.5s ease-in-out', style({ transform: 'translateY(0%)' }))
                ], { optional: true }),
                query(':leave', [
                    style({ transform: 'translateY(0%)', opacity: 0 }),
                    animate('0.5s ease-in-out', style({ transform: 'translateY(-100%)' }))
                ], { optional: true }),
            ])
        ]),
        transition('Form => Calculator', [
            query(':enter, :leave', style({ position: 'absolute', width: '100%', height: '100%'}), { optional: true }),
            group([
                query(':enter', [
                    style({ transform: 'translateY(-100%)', opacity: 0 }),
                    animate('0.5s ease-in-out', style({ transform: 'translateY(0%)' }))
                ], { optional: true }),
                query(':leave', [
                    style({ transform: 'translateY(0%)', opacity: 0 }),
                    animate('0.5s ease-in-out', style({ transform: 'translateY(100%)' }))
                ], { optional: true }),
            ])
        ]),
        transition('Form => Documents', [
            query(':enter, :leave', style({ position: 'absolute', width: '100%', height: '100%'}), { optional: true }),
            group([
                query(':enter', [
                    style({ transform: 'translateY(-100%)', opacity: 0 }),
                    animate('0.5s ease-in-out', style({ transform: 'translateY(0%)' }))
                ], { optional: true }),
                query(':leave', [
                    style({ transform: 'translateY(0%)', opacity: 0 }),
                    animate('0.5s ease-in-out', style({ transform: 'translateY(100%)' }))
                ], { optional: true }),
            ])
        ]),
    ]);
