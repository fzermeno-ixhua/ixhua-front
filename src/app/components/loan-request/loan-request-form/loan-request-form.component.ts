import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit, OnDestroy, ChangeDetectorRef, ViewChild } from '@angular/core';
import { SubSink } from 'subsink';
import * as dropdownsData from './form-dropdowns-data';
import { Store } from '@ngrx/store';
import { selectLoanState, State } from 'src/app/store/reducers';
import {
  selectClientForm,
  selectFamiliesList,
  selectZipData,
  State as LoanState
} from 'src/app/store/reducers/loan-request.reducer';
import { fetchZipCodeData, saveFormData } from 'src/app/store/actions';
import { debounceTime, filter, map } from 'rxjs/operators';
import { LoanService } from 'src/app/services/loan.service';
import { ClientService } from 'src/app/services/client.service';
import { Router } from '@angular/router';
import { environment } from '../../../../environments/environment';
import { HttpClient } from '@angular/common/http';

import { ContractData, LoanRequest } from 'src/app/models/interfaces';
import { loanRequest } from 'src/app/store/actions';

@Component({
  selector: 'app-loan-request-form',
  templateUrl: './loan-request-form.component.html'
})
export class LoanRequestFormComponent implements OnInit, OnDestroy {
  @ViewChild('inputRef',{static: false}) inputRef;
  public static ALLOWED_FILE_TYPES = [
    'image/png',
    'image/jpeg',
    'image/jpg',
    'application/pdf'
  ];
  form: FormGroup;
  filesForm: FormGroup;
  inputNumberInvalidChars = ['-', '+', 'e', '*'];
  requiredMessage = 'Este campo es requerido';
  invalidEmailMessage = 'El formato es invalido';
  invalidFileMessage = 'El archivo que intentaste cargar excede el tamaño permitido o no es válido. Por favor verifícalo';
  invalidBirthDateMessage = 'Error en la fecha de naicmiento, debe tener mas de 18 años de edad';
  formStates = dropdownsData.STATES;
  formMaritalStatus = dropdownsData.MARITAL_STATUS;
  formStudiesDegree = dropdownsData.STUDIES_DEGREE;
  formBanks = dropdownsData.BANKS;
  formCreditBureau = dropdownsData.CREDIT_BUREAU;
  formCreditLine = dropdownsData.CREDIT_LINE;
  formHouseType = dropdownsData.HOUSING_TYPE;
  formNationaloty = dropdownsData.NATIONALITY;
  formCityStates = dropdownsData.STATES;
  formTellUs = dropdownsData.TELL_US;
  formActualPosition = dropdownsData.ACTUAL_POSITION;
  formFamiliesList = [];
  formBirthDate = '2000-01-01';
  formZipData: {
    municipality: string;
    state: string;
    neighborhoods: string[];
  };
  isFormUploadingFiles = false;
  isInvalidBirthDate = false;
  uploadFilesError = false;
  validateEmail = false;

  auxNationality = null
  auxCityState = null
  efektivaBankName = 'Santander Serfin';
  efektivaClabe = '014320655078240476';

  public isZipCodesApiDown = false;
  private isNoneCreditLineAlreadySelected = false;
  public show_extra_file = false;

  public isFirstLoan = true;
  private subs = new SubSink();
  private API_URL = environment.API_URL;
  
  public clientDocuments: any;
  //ine
  public showIne = false;
  public fileIneAux : any = null;
  //additional_file
  public showAdditionalFile = false;
  public fileAdditionalFileAux : any = null;
  //bank account
  public showBankAccount = false;
  public fileBankAccountAux : any = null;
  // proof address
  public showProofAddress = false;
  public fileProofAddressAux : any = null;

  private loanRequest: LoanRequest;
  public errorMessage:any;

  public show_tell_us_note:any = false
  public show_tell_us_invoices:any = false
  public show_second_bank:any = false
  

  constructor(
    private store: Store<State>,
    private loanService: LoanService,
    private clientService: ClientService,
    private router: Router,
    private http: HttpClient,
  ) {}

  /**
   * @description Converts a angular reactive form value to FormData
   *  in order to be sent over http request.
   * @param formValue The reactive form value
   */
  static toFormData<T>(formValue: T) {
    const formData = new FormData();
    for (const key of Object.keys(formValue)) {
      formData.append(key, formValue[key]);
    }
    return formData;
  }

  ngOnInit() {
    this.initForm();
    this.subs.sink = this.store
      .select<LoanState>(selectLoanState)
      .pipe(
        map(state => ({
          zipData: selectZipData(state),
          form: selectClientForm(state),
          isFirstLoan: state.isFirstLoan,
          familiesList: selectFamiliesList(state)
        }))
      )
      .subscribe(({ form, familiesList, isFirstLoan, zipData }) => {
        this.formFamiliesList = familiesList;
        this.isFirstLoan = isFirstLoan;
        this.formZipData = {
          municipality: zipData.municipio,
          state: zipData.estado,
          neighborhoods: zipData.colonias
        };
        let formValue = {
          ...form
        };

        if (formValue.nationality) {
          var keepGoingNationality = true;
          this.formNationaloty.forEach( (myObject, index) => {
            if(keepGoingNationality) {
              if (formValue.nationality == myObject){
                this.auxNationality = formValue.nationality
                keepGoingNationality = false
              }
              else{
                this.auxNationality = null
              }
            }
          });
          formValue.nationality = this.auxNationality
        }

        if (formValue.state) {       
          var keepGoingState = true;
          this.formCityStates.forEach((myObject, index) => {
            if(keepGoingState) {
              if (formValue.state == myObject){
                this.auxCityState = formValue.state
                keepGoingState = false
              }
              else{
                this.auxCityState = null
              }
            }
          });
          formValue.state = this.auxCityState
        }

        // Validate the ZIP Codes API response is valid
        if (!zipData.municipio || zipData.colonias.length === 0) {
          this.setAddressFieldsAvailability(true);
        } else {
          this.setAddressFieldsAvailability(false);
          // Only change the city and state fields if they are available from the API
          formValue['city'] = zipData.municipio;
          formValue['state'] = zipData.estado;
        }
      
        this.toggleDisabledFields(isFirstLoan);
        if (!!formValue) {
          // Due to original data being a single string,
          // the patched data into the form object must be
          // first splitted into an array.
         
          /*if (isFirstLoan == true) {
            formValue.email = "";
            formValue.labor_old=0;
            formValue.labor_old_month=0;
          }*/
          
          const { credit_lines } = formValue;
          if (!!credit_lines) {
            // @ts-ignore
            formValue.credit_lines = credit_lines.split(',');
          }
          this.form.patchValue(formValue, { emitEvent: false });
          if (formValue.birthdate) {
            this.formBirthDate = formValue.birthdate;
          }
        }
      });
    this.subs.add(this.form.get('postal_code')
      .valueChanges.pipe(
        debounceTime(500),
        map<number, string>(val => !!val ? String(val) : ''),
        filter<string>(val => val.length >= 5),
      ).subscribe(zip => {
        let formData = this.form.getRawValue();
        formData = {
          ...formData,
          credit_lines: !!formData.credit_lines ? formData.credit_lines.join(',') : ''
        };
        this.store.dispatch(saveFormData({ form: formData }));
        this.store.dispatch(fetchZipCodeData({ zip }));
      }));

    
    // Binding email changeValue detection to apply lowercase
    this.form.get('email')
    .valueChanges
    .subscribe({
      next: val => {
        const elRef = this.inputRef.nativeElement;
        // get stored original unmodified value (not including last change)
        const orVal = elRef.getAttribute('data-model-value') || '';
        // modify new value to be equal to the original input (including last change)
        val = val.replace(orVal.toLowerCase(), orVal);
        // store original unmodified value (including last change)
        elRef.setAttribute('data-model-value', val);
        // set view value using DOM value property
        elRef.value = val.toLowerCase();
        this.form.controls['email'].setValue(val, {
          emitEvent: false,
          emitModelToViewChange: false
        });
      }
    })


    document.documentElement.scrollTop = 0;
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  
  hasError(controlName: string, typeError: string): boolean {
    return this.form.get(controlName).hasError(typeError);
  }

  controlTouched(controlName: string): boolean {
    return this.form.get(controlName).touched;
  }

  getLabelControlError(controlName: string, typeError: string): boolean {
    return (
      this.form.get(controlName).hasError(typeError) &&
      this.controlTouched(controlName)
    );
  }

  filterNumberInputs(e) {
    if (this.inputNumberInvalidChars.indexOf(e.key) !== -1) {
      e.preventDefault();
    }
  }

  initForm() {
   
    this.form = new FormGroup({
      names: new FormControl('', [
        Validators.required,
        Validators.maxLength(200)
      ]),
      father_last_name: new FormControl('', [
        Validators.required,
        Validators.maxLength(200)
      ]),
      mother_last_name: new FormControl('', [
        Validators.required,
        Validators.maxLength(200)
      ]),
      gender: new FormControl('Masculino', [Validators.required]),
      birthdate: new FormControl('', [Validators.required]),
      nationality: new FormControl('', [
        Validators.required,
        Validators.maxLength(20)
      ]),
      civil_status: new FormControl('', [Validators.required]),
      address: new FormControl('', [
        Validators.required,
        Validators.maxLength(50)
      ]),
      outdoor_no: new FormControl('', [
        Validators.required,
        Validators.maxLength(50)
      ]),
      interior_no: new FormControl('', [Validators.maxLength(50)]),
      postal_code: new FormControl('',
        [
          Validators.required,
          //Validators.max(99999)
          Validators.minLength(5),
          Validators.maxLength(5),
          Validators.pattern('^[0-9]{5}$')
        ]),
      suburb: new FormControl('', [Validators.required]),
      city: new FormControl(
        { value: '', disabled: true },
        [Validators.required]),
      state: new FormControl(
        { value: '', disabled: true },
        [Validators.required]),
      country: new FormControl(
        { value: 'México'},
        [Validators.required]),
      house_type: new FormControl('', [Validators.required]),
      years_residence: new FormControl('', [
        Validators.required,
        Validators.max(99)
      ]),
      phone: new FormControl('', [
        Validators.required,
        //Validators.max(9999999999)
        Validators.minLength(10),
        Validators.maxLength(10),
        Validators.pattern('^[0-9]{10}$')
      ]),
      cellphone: new FormControl('', [
        Validators.required,
        //Validators.max(9999999999)
        Validators.minLength(10),
        Validators.maxLength(10),
        Validators.pattern('^[0-9]{10}$')
      ]),
      email: new FormControl('', [
        Validators.required,
        Validators.email
      ]),
      birth_place: new FormControl('', [Validators.required]),
      curp: new FormControl('', [
        Validators.required,
        Validators.pattern('^[a-zA-Z]{4}\\d{6}[a-zA-Z]{6}[0-9A-Za-z]{2}$')
      ]),
      rfc: new FormControl({ value: '', disabled: true }, [
        Validators.required,
        Validators.pattern(
          '^[A-Za-z]{4}[ |\\\\-]{0,1}[0-9]{6}[ |\\\\-]{0,1}[0-9A-Za-z]{3}$'
        )
      ]),
      study_level: new FormControl('', [Validators.required]),
      family_str: new FormControl('', [
        Validators.required,
        Validators.maxLength(50)
      ]),
      actual_position: new FormControl(null, [
        Validators.required,
        Validators.maxLength(50)
      ]),
      labor_old: new FormControl('', [Validators.required,Validators.max(99)]),
      labor_old_month: new FormControl('', [Validators.required,Validators.max(11)]),
      salary: new FormControl('', [
        Validators.required,
        Validators.min(0),
        Validators.max(999999.99),
      ]),
      family_income: new FormControl('', [
        Validators.min(0),
        Validators.max(999999.99)
      ]),
      economic_dependent: new FormControl('', [Validators.required,Validators.max(15)]),
      bank_name: new FormControl('', [Validators.required]),
      bank_clabe: new FormControl('', [
        Validators.required,
        Validators.pattern('^[0-9]{16}([0-9]{2})?$')
      ]),
      bank_withdraw_deposit: new FormControl('',),
      bank_name_withdraw: new FormControl('', ),
      bank_clabe_withdraw: new FormControl('',),
      credit_bureau: new FormControl('', [Validators.required]),
      credit_lines: new FormControl([], [Validators.required]),
      tell_us: new FormControl('Alimentos', [Validators.required]),
      tell_us_note: new FormControl(null),
      tell_us_invoices: new FormControl(null),
      family_reference: new FormControl('', [
        Validators.required,
        Validators.maxLength(50)
      ]),
      family_reference_phone: new FormControl('', [
        Validators.required,
        Validators.minLength(10),
        Validators.maxLength(10),
        Validators.pattern('^[0-9]{10}$')
      ]),
      reference: new FormControl('', [
        Validators.required,
        Validators.maxLength(50)
      ]),
      reference_phone: new FormControl('', [
        Validators.required,
        Validators.minLength(10),
        Validators.maxLength(10),
        Validators.pattern('^[0-9]{10}$')
      ]),
      notes: new FormControl(null),
      // meta field
      terms_and_conditions: new FormControl(false, Validators.requiredTrue)
    });

    this.form.get('credit_lines')
      .valueChanges
      .subscribe(e => this.onCreditLinesChange(e));

    this.form.get('economic_dependent')
      .valueChanges
      .subscribe(e => this.onEconomicDependentChange(e));
  }

  private toggleDisabledFields(isFirstLoan: boolean = true): void {
    const disabledFieldsNames = [
      'names',
      'father_last_name',
      'mother_last_name',
      'gender',
      'birthdate',
      //'nationality',
      'birth_place',
      'curp',
      'email',
      'country'
    ];
    if (!isFirstLoan) {
      for (const fieldName of disabledFieldsNames) {
        this.form.get(fieldName).disable();
      }
      return;
    }
    
    // If this is not the first user loan request, we must enable all the fields
    for (const fieldName of disabledFieldsNames) {
      this.form.get(fieldName).enable();
    }
  }

  private setAddressFieldsAvailability(areEnabled = true) {
    const { form } = this;
    this.isZipCodesApiDown = areEnabled;
    ['city', 'state'].forEach(field => {
      if (areEnabled) {
        form.get(field).enable();
      } else {
        form.get(field).disable();
      }
    });
  }

  getDate(date: string) {
    let date1:any = new Date();
    let date2:any = new Date(date);

    let diff = Math.floor(date1.getTime() - date2.getTime());
    let day = 1000 * 60 * 60 * 24;

    let days = Math.floor(diff/day);
    let months = Math.floor(days/31);
    let years = Math.floor(months/12);

/*
    if (years+1>= 18) {
      this.isInvalidBirthDate = false
    }else{
      this.isInvalidBirthDate= true
    }
*/    
    this.form.get('birthdate').setValue(date);
  }

  public onEconomicDependentChange(value): void {
    const roundedValue = Math.floor(Math.abs(+value));
    this.form.get('economic_dependent').setValue(roundedValue, { emitEvent: false });
  }


  public SecondBank(event){
    /*
      Function to show second bank div when checkbox checked
    */
    if(event.checked){
      this.show_second_bank = true;
    }else{
      this.show_second_bank = false;
    }

  }


  public onCreditLinesChange(value): void {
    let { form } = this;

    // If the user already marked 'Ninguno' and wanted to select other thing
    if (this.isNoneCreditLineAlreadySelected && value.length > 1) {
      let newValues = [ ...value ];
      newValues.pop();
      form.get('credit_lines').setValue(newValues, { emitEvent: false });
      this.isNoneCreditLineAlreadySelected = false;
      return;
    }

    // Check for the disable all option
    if (value.indexOf('Ninguno') === -1) {
      // If there is not 'Ninguno' option, do nothing
      this.isNoneCreditLineAlreadySelected = false;
      return;
    }

    // When none of the above validations are met, all the others values are removed
    form.get('credit_lines').setValue(['Ninguno'], { emitEvent: false });
    this.isNoneCreditLineAlreadySelected = true;
  }


  public changeTellUs() {

    if (this.form.controls.tell_us.value == 'Otros (explique)'){

      this.show_tell_us_note = true;
      this.form.get('tell_us_note').setValidators([Validators.required]);

    }else if(this.form.controls.tell_us.value == 'Pago de facturas'){

      this.show_tell_us_invoices = true;
      this.form.get('tell_us_invoices').setValidators([Validators.required]);

    }else{

      this.show_tell_us_note = false;
      this.form.get('tell_us_note').clearValidators();
      this.form.controls.tell_us_note.setValue(null)

      this.show_tell_us_invoices = false;
      this.form.get('tell_us_invoices').clearValidators();
      this.form.controls.tell_us_invoices.setValue(null)      

    }

    this.form.get('tell_us_note').updateValueAndValidity();
    this.form.get('tell_us_invoices').updateValueAndValidity();

  }


  public onFormSubmit(e): void {
    e.preventDefault();

    if (this.validateEmail) {
      return;
    }

    const { form } = this;
    form.markAllAsTouched();
    
    if (form.invalid) {
      this.validateEmail = false
      return;
    }

    //validacion de la antiguedad laboral minima de 6 meses
    //if((form.get('labor_old').value) == 0 && (form.get('labor_old_month').value < 6) || (form.get('labor_old').value) == null && (form.get('labor_old_month').value < 6) ){
    //  this.errorMessage = "La antigüedad debe tener un valor mínimo de 6 meses"
    //  return;
    //}


    this.validateEmail = false
    this.errorMessage=""
    
    //if(form.controls.bank_clabe_withdraw.value === null || this.form.controls.bank_clabe_withdraw.value == '' || this.form.controls.bank_clabe_withdraw.value == '000000000000000000'){
      /* If second bank is not set, gets first bank values*/
      this.form.controls.bank_name_withdraw.setValue(this.form.controls.bank_name.value);
      this.form.controls.bank_clabe_withdraw .setValue(this.form.controls.bank_clabe.value);
    //}

    if(form.controls.tell_us_invoices.value !== null && this.form.controls.tell_us_invoices.value != '' ){
      /* Deliverisi Temp bank name and clabe hard coded data - they must be changed to dinamically */
      this.form.patchValue({bank_name: this.efektivaBankName, bank_clabe: this.efektivaClabe});
    }

    //validate email in first loan
    const body = { rfc:form.get('rfc').value, email:form.get('email').value }
    
    this.http.post(`${this.API_URL}/api/v1/client/validate_email_onchange/`, body)
    .subscribe({
      next: data => {       
        // Send the files to the API  
        let formData = form.getRawValue();
        formData = {
          ...formData,
          terms_and_conditions: true,
          credit_lines: formData.credit_lines.join(',')
        };
            
        this.store.dispatch(saveFormData({ form: formData }));
        this.clientService
          .updateInfo(formData)
          .subscribe(res => {
            this.router.navigateByUrl('/home/documents');
            window.scroll({
              
              left: 0,
              behavior: 'smooth'
            });
          },
          error => {
            this.errorMessage = error.error.error
            return
          })
      },
      error: error => { 
        this.validateEmail = true
        return;
      }
    })
    
  }


}
