import { Component, OnDestroy, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Store } from '@ngrx/store';
import { LoanInfo } from 'src/app/models';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-loan-footer',
  templateUrl: './loan-footer.component.html',
})
export class LoanFooterComponent implements OnInit, OnDestroy {
  
  

  constructor(
    private sanitizer: DomSanitizer,
  ) {  }

  ngOnInit(): void {
    
  }

  ngOnDestroy(): void {
    
  }

 
}