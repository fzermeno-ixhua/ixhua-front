import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import { GetClientLoansResponse } from '../../../../models/interfaces';
import {State as LoanState} from '../../../../store/reducers/loan-request.reducer';
import {Store} from '@ngrx/store';
import { State, selectLoanState } from 'src/app/store/reducers';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-loan-totals',
  templateUrl: './loan-totals.component.html',
  styleUrls: ['./loan-totals.component.scss']
})
export class LoanTotalsComponent implements OnInit, OnDestroy {
  private storeSubscription: Subscription;
  public totals: GetClientLoansResponse = {
    active_loan: 1000,
    available_loan: 1000,
    total_loan: 2000,
    min_loan: 1000,
    loan_unit: '$',
    days_to_loan: 0,
    days_worked: 0,
  };
  constructor(
    private store: Store<State>
  ) { }

  ngOnInit() {
    this.storeSubscription = this.store
      .select<LoanState>(selectLoanState)
      .subscribe(({ loanInfo }) => {
        this.totals = loanInfo;
      });
  }

  ngOnDestroy(): void {
    this.storeSubscription.unsubscribe();
  }

}
