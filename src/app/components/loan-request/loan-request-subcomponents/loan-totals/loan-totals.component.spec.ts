import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoanTotalsComponent } from './loan-totals.component';

describe('LoanTotalsComponent', () => {
  let component: LoanTotalsComponent;
  let fixture: ComponentFixture<LoanTotalsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoanTotalsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoanTotalsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
