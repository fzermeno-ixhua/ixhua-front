import {Component, OnDestroy, OnInit} from '@angular/core';
import {selectClient, selectClientSeeds, State} from '../../../../store/reducers';
import {Store} from '@ngrx/store';
import {Subscription} from 'rxjs';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-loan-seeds',
  templateUrl: './loan-seeds.component.html'
})
export class LoanSeedsComponent implements OnInit, OnDestroy {
  private storeSubscription: Subscription;
  private clientStoreSubscription: Subscription;
  private clientStoreSubscriptionPhone: Subscription;
  public clientName = '';
  public clientPhone = '';
  private seeds: {
    green: number;
    gray: number;
    yellow: number;
  } = null;
  constructor(
    private store: Store<State>
  ) { }

  ngOnInit() {
    this.storeSubscription =
      this.store.select<{
        green: number;
        gray: number;
        yellow: number;
      }>(selectClientSeeds)
        .subscribe(s => this.seeds = s);

    this.clientStoreSubscription =
      this.store.select(selectClient)
        .pipe(map(client => client.display_name))
        .subscribe(name => this.clientName = name);

    this.clientStoreSubscriptionPhone =
      this.store.select(selectClient)
        .pipe(map(client => client.phone))
        .subscribe(phone => this.clientPhone = phone);


  }

  ngOnDestroy() {
    this.clientStoreSubscription.unsubscribe();
    this.storeSubscription.unsubscribe();
  }

  public getSeedsArray(seedColor: 'gray' | 'green' | 'yellow') {
    const { seeds } = this;
    return new Array(seeds[seedColor]);
  }

}
