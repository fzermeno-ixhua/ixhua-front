import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoanSeedsComponent } from './loan-seeds.component';

describe('LoanSeedsComponent', () => {
  let component: LoanSeedsComponent;
  let fixture: ComponentFixture<LoanSeedsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoanSeedsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoanSeedsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
