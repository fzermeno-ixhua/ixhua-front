import { Component, ViewEncapsulation } from '@angular/core';
import {MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-loan-request-success-modal',
  template: `
    <div class="modal-body">
      <div class="header text-left">
        <h3 class="fs-24px my-0">Estamos revisando tu solicitud.</h3>
      </div>
      <hr>
      <div class="body">
        <p class="mb-0">
          Estamos revisando tu solicitud Si todos los datos de tu
          solicitud son correctos, te enviaremos un correo para
          notificarte que el dinero esta disponible en tu cuenta.
        </p>
        <p class="mb-0">
          Si tenemos algún dato que no cuadre, te avisaremos para
          que nos ayudes corregirlo. Cualquier duda o comentario, 
          contáctanos vía email a <a class="url" href="mailto:contacto@ixhua.mx">contacto@ixhua.mx</a> 
          o comunícate a nuestro whatsapp <a href="https://wa.me/523334065657"
                                             target="_blank"
                                             style="text-decoration: none">333-072-2026</a>.
        </p>
        <p class="mb-0">
          Consulta nuestro aviso de privacidad aquí <a [routerLink]="['/privacy']" target="_blank">www.ixhua.mx/privacy</a>.
        </p>
        <div class="button-holder text-center pt-3">
          <button
            class="fs-24px mat-rounded-button mt-1 py-1"
            mat-flat-button
            color="primary"
            (click)="onClose()"
          >
            Ir a mis prestamos.
          </button>
        </div>
      </div>
    </div>
  `,
  encapsulation: ViewEncapsulation.None
})
export class LoanRequestSuccessModalComponent {
  constructor(
    private dialogRef: MatDialogRef<LoanRequestSuccessModalComponent>
  ) {  }

  onClose() {
    this.dialogRef.close();
  }
}
