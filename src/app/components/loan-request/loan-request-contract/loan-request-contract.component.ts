import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import { Store } from '@ngrx/store';
import { HttpClient } from '@angular/common/http';
import {
  State as LoanState,
  selectLoanRequest,
  selectContractData,
} from 'src/app/store/reducers/loan-request.reducer';
import { Subscription } from 'rxjs';
import { ContractData, LoanRequest } from 'src/app/models/interfaces';
import { loanRequest } from 'src/app/store/actions';
import { selectLoanState, State } from 'src/app/store/reducers';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-loan-request-contract',
  templateUrl: './loan-request-contract.component.html',
  styleUrls: ['./loan-request-contract.component.scss']
})
export class LoanRequestContractComponent implements OnInit {

  public ipAddress = '';
  public flagToShowError = false;
  public generatedContractData: ClientContractData = null;
  public disableRequests = false;
  public hardcodedData = {
    product: 'CRÉDITO IXHUA',
    creditType: 'CRÉDITO SIMPLE',
    anualMoratory: '540%',
    ixhua: {
      companyName: 'IXHUA',
      website: 'www.ixhua.mx',
      socialReason: 'IXHUA TECH, S.A.P.I. DE C.V.',
      contact: 'contacto@ixhua.mx',
      address: 'Av. Real de  Acueducto  240  int.  8,  Colonia Puerta  de  Hierro en Zapopan,  Jalisco. C.P. 45116',
      phone: '33 19136707',
      rfc: 'MPR1608268Z5',
      nationality: 'MEXICANA',
      legalRepresentative: 'JORGE ALBERTO DORANTES PADILLA',
      bank: 'BBVA',
      bankSocialReason: 'BBVA BANCOMER S.A.',
      bankAccount: '0113675203',
      clabe: '012320001136752038',
      accountHolder: 'IXHUA TECH, S.A.P.I. DE C.V.',
      clientProvider: 'DELIVERISI LOGISTICS SAPI DE CV' /*ALERT: This value has to be a variable at some point*/
    },
    profeco: {
      companyName: 'PROFECO',
      address: 'Av. José Vasconcelos 208,  Col. Condesa, Del. Cuauhtémoc, Ciudad de México. C.P. 06140',
      phone: '(55) 5625 6700',
      phoneCitizensAttention: '800 468 8722',
      website: 'https://www.gob.mx/profeco'
    }
  };
  contractInfo: ContractData = {
    websiteurl: 'http://www.ixhua.com.mx',
    profecourl: 'https://www.gob.mx/profeco',

    company: 'Company',
    president: 'President',
    website: 'Website',
    folio: 1010101010,
    rfc: 'CCCCCCCCCCCCCC',
    clientName: 'clientName',
    curp: 'BADD110313HCMLNS09',
    birthddate: '12/12/2012',
    place_of_birth: 'Guadalajara',
    nacionality: 'MEXICANA',
    gender: 'M',
    email: 'email@email.com',
    address: 'Calle 24 col. Test',
    phone: 12345678890,
    cellphone: 12345678890,
    company_where_you_work: 'Amplemind',
    payment_day: 'día 15 de cada mes',
    privacy_policy_url: 'www.urlprivacy_policy_url.com',
    creditAmount: 30000,
    loan_interest: 20,
    clabe: 123123123123,
    bank: 'Bancomer',
    contract_period_of_time: 30,

    company_account_bank: 'Bancomer',
    company_account: 100000,
    company_clabe: 100000,
    account_holder: 'Name Lastname',
    account_reference: 'Name Lastname',
    moratorium_interest: 20,

    profeco_number: 123123123,
    profeco_number_inside_the_republic: 123123123,
    profeco_email: 'mail@mail.com',
    contract_date: { day: '01', month: '01', year: '2019' }
  };
  public checkboxesForm = new FormGroup({
    cover: new FormControl(false, [Validators.requiredTrue]),
    contract: new FormControl(false, [Validators.requiredTrue]),
    directDebit: new FormControl(false, [Validators.requiredTrue]),
  });
  // public acceptedCheckbox = new FormControl(false, [Validators.requiredTrue]);
  private loanRequest: LoanRequest;
  private storeSubscription: Subscription;
  public show_tell_us_note:any = true;
  public show_tell_us_invoices:any = false;
  public isNotSameClabe: boolean = false;

  constructor(
    private store: Store<State>,
    private http:HttpClient
  ) {  }


  ngOnInit() {
    //this.disableRequests = false;

    this.getIPAddress(); //Obtaining user's IP Address

    this.storeSubscription =
      this.store.select<LoanState>(selectLoanState)
        .pipe(
          map(state => ({
            contract: selectContractData(state),
            loan: selectLoanRequest(state),
          }))
        ).subscribe(({ loan, contract }) => {
          //this.disableRequests = true;
          // @ts-ignore
          this.generatedContractData = {
            ...contract,
            ...loan.client,
            name: `${loan.client.names} ${loan.client.father_last_name} ${loan.client.mother_last_name}`
          };
          this.loanRequest = loan;
   
          if(loan.client.tell_us_invoices !== null && loan.client.tell_us_invoices !== ''){
            /* Show Regular or Invoices Info in HTML */
            this.generatedContractData.clientInvoices = loan.client.tell_us_invoices;
            this.show_tell_us_note = false;
            this.show_tell_us_invoices = true;
          }

          console.log(contract)
          if(contract.clabe !==  contract.clabeWithdraw){
            /* Bank CLABEs not equals - means client wants us tro substract from different account */ 
            this.isNotSameClabe = true
          }

      });

    document.documentElement.scrollTop = 0;
  }

  public onContractSign(): void {

    this.flagToShowError = true;
    const { checkboxesForm } = this;
    checkboxesForm.markAllAsTouched();
    checkboxesForm.markAsTouched();
    if (checkboxesForm.invalid) {
      return;
    }
    this.generatedContractData.ip_client = this.ipAddress; //Adding Client IP Address to request
    this.disableRequests = true;
    this.store.dispatch(loanRequest({ ...this.loanRequest, extra_data: this.contract }));
  }

  public get contract(): ClientContractData {
    const { generatedContractData, hardcodedData } = this;
    const upperCaseData = { ...generatedContractData };
    Object.keys(upperCaseData)
      .forEach(key => {
        upperCaseData[key] = String(upperCaseData[key]).toLocaleUpperCase();
      });
    // @ts-ignore
    return {
      ...upperCaseData,
      ...hardcodedData
    };
  }

  getIPAddress(){
    this.http.get("http://api.ipify.org/?format=json").subscribe((res:any)=>{
      this.ipAddress = res.ip;
    });
  }

}


export interface IxhuaInfo {
  companyName: string;
  website: string;
  socialReason: string;
  contact: string;
  address: string;
  phone: string;
  rfc: string;
  nationality: string;
  legalRepresentative: string;
  bank: string;
  bankSocialReason: string;
  bankAccount: string;
  clabe: string;
  accountHolder: string;
  clientProvider: string;
}
export interface ProfecoInfo {
  companyName: string;
  address: string;
  phone: string;
  phoneCitizenSttention: string;
  website: string;
  phoneCitizensAttention?: string;
}

export interface ClientContractData {
  accountType: string;
  amount: number;
  amountLetters: string;
  annualRegularInterest: number;
  term: number;
  interest: number;
  interestToPayInLetters: string;
  aperture: number;
  interior_no: string;
  outdoor_no: string;
  request_no: string;
  postal_code: string;
  suburb: string;
  contract_no: number;
  sign_date: string;
  state: string;
  city: string;
  name: string;
  rfc: string;
  birthdate: string;
  email: string;
  address: string;
  phone: string;
  cellphone: string;
  family: string;
  curp: string;
  nationality: string;
  gender: string;
  clabe: string;
  bankName: string;
  clabeWithdraw: string;
  bankNameWithdraw: string;
  loanDestination: string;
  company: string;
  ip_client: string;
  clientInvoices: string;

  payments: number;
  paymentDate: string;
  interestToPay: number;
  totalAmount: number;
  cat: number;
  tfa: number;
  product: string;
  creditType: string;
  profeco: ProfecoInfo;
  ixhua: IxhuaInfo;
}
