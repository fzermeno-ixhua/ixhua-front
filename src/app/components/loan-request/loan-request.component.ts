import {
  AfterViewInit,
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  Renderer2,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable, Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { slideInAnimation } from './loan-request.component.animations';
import {NavigationEnd, Router} from "@angular/router";
import { ClientService } from 'src/app/services/client.service'
@Component({
  encapsulation: ViewEncapsulation.None,
  selector: 'app-loan-request',
  templateUrl: './loan-request.component.html',
  animations: [slideInAnimation]
})
export class LoanRequestComponent implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild('mainCardContainer', { static: false, read: ElementRef }) cardContainer: ElementRef = null;
  @ViewChild('bulletsContainer', { static: false, read: ElementRef }) navContainer: ElementRef = null;
  public isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(map(result => result.matches));

  /* in here its necessary to define tabs */
  tabs = [
    {
      name: 'Calculator',
      path: '/home/',
      title: 'Simulador',
      subtitle: 'Simulador',
      active: true
    },
    {
      name: 'Form',
      path: '/home/form',
      title: 'Requisitos para solicitud de préstamo',
      subtitle: 'Requisitos',
      active: false
    },
    {
      name: 'Contract',
      path: '/home/contract',
      title: 'Contrato de crédito simple',
      subtitle: 'Contrato',
      action: 'Enviar',
      active: false
    },
    {
      name: 'Documents',
      path: '/home/documents',
      title: 'Documentos:',
      subtitle: 'Documentos',
      active: false
    }
  ];

  selectedIndex = 0;
  lastIndexActive = 0;
  tabName = this.tabs[0].title;
  subtabName = this.tabs[0].subtitle;
  private routerEventsSubscription: Subscription = null;
  public customMessage:any
  public customURL:any

  constructor(
    private router: Router,
    public renderer: Renderer2,
    private breakpointObserver: BreakpointObserver,
    private clientService: ClientService,
  ) {  }

  public ngOnInit(): void {

    /* This info is not used anymore
    let info = JSON.parse(sessionStorage.auth)
    if (info.client) {
      if(info.client.rfc){
        let body =  {rfc:info.client.rfc}

        this.clientService
        .customClientMessage(body)
        .subscribe({
          next: data => {
            this.customMessage = data.message 
            this.customURL = data.website
          },   
          error: error => { 
            this.customMessage = ""
          }
        
        })
      }
    }
    */

    this.routerEventsSubscription =
      this.router.events
        .pipe( filter(e => e instanceof NavigationEnd) )
        .subscribe((event: NavigationEnd) => {
          switch (event.url) {
            case '/home/form':
              this.checkTab(null,1);
            break;
            case '/home/contract':
              this.checkTab(null,2);
            break;
            case '/home/documents':
              this.checkTab(null,3);
            break;
            default:
              this.checkTab(null,0);
          }
        });
  }

  ngAfterViewInit(): void {
    // setTimeout(this.calculateAndSetAbsolutePositionForNavBullets, 1000);
    setTimeout(() => {
      this.renderer.setStyle(this.navContainer.nativeElement, 'right', this.bulletsRight);
    }, 500);
  }

  public get bulletsRight() {
    if (!this.cardContainer || !this.navContainer) {
      // The DOM has not been initialized yet
      return '0px';
    }
    try {
      this.renderer.setStyle(this.navContainer.nativeElement, 'right', `0px`);
      const { x, width } = (<DOMRect>(<Element>this.cardContainer.nativeElement).getBoundingClientRect());
      const { x: bulletsX } = (<DOMRect>(<Element>this.navContainer.nativeElement).getBoundingClientRect());
      return `${bulletsX - (x + width)}px`;
    } catch (e) {
      console.error('Could not calculate the right value for the bullets nav', e);
      return '0px';
    }
  }

  public ngOnDestroy(): void {
    if (this.routerEventsSubscription === null) {
      return; // Do nothing
    }
    this.routerEventsSubscription.unsubscribe();
  }

  checkTab(e: Event, index: any) {
    if (!!e) {
      e.stopPropagation();
      e.preventDefault();
    }
    this.lastIndexActive = this.selectedIndex;
    this.selectedIndex = index;
    this._getTabTexts(index);
  }

  /*
    To get tab title
    i: tab index
  */
  _getTabTexts(i: number) {
    this.tabName = this.tabs[i].title;
  }
}
