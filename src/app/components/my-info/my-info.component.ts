import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, Input, OnInit, OnDestroy, ChangeDetectorRef, ViewChild } from '@angular/core';
import { SubSink } from 'subsink';
import * as dropdownsData from './my-info-dropdowns-data';
import { Store } from '@ngrx/store';
import { selectLoanState, State } from 'src/app/store/reducers';
import {
  selectClientForm,
  selectFamiliesList,
  selectZipData,
  State as LoanState
} from 'src/app/store/reducers/loan-request.reducer';
import { fetchZipCodeData, saveFormData } from 'src/app/store/actions';
import { debounceTime, filter, map } from 'rxjs/operators';
import { LoanService } from 'src/app/services/loan.service';
import { ClientService } from 'src/app/services/client.service';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';

import { ContractData, LoanRequest } from 'src/app/models/interfaces';
import { loanRequest } from 'src/app/store/actions';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatTooltipModule} from '@angular/material/tooltip';


@Component({
  selector: 'app-my-info',
  templateUrl: './my-info.component.html',
  //styleUrls: ['../loan-request/loan-request-styles/all.scss']
})

export class MyInfoComponent implements OnInit, OnDestroy {
 /**
 * MyInfoComponent Class.
 *
 * @implements
 *   OnInit , OnDestroy
 *
 * @param 
 * @returns 
 */

  @ViewChild('inputRef',{static: false}) inputRef;
  form: FormGroup;
  filesForm: FormGroup;
  inputNumberInvalidChars = ['-', '+', 'e', '*'];
  requiredMessage = 'Este campo es requerido';
  invalidEmailMessage = 'El formato es invalido';
  invalidBirthDateMessage = 'Error en la fecha de nacimiento, debe tener mas de 18 años de edad';
  formStates = dropdownsData.STATES;
  formMaritalStatus = dropdownsData.MARITAL_STATUS;
  formStudiesDegree = dropdownsData.STUDIES_DEGREE;
  formBanks = dropdownsData.BANKS;
  formCreditBureau = dropdownsData.CREDIT_BUREAU;
  formCreditLine = dropdownsData.CREDIT_LINE;
  formHouseType = dropdownsData.HOUSING_TYPE;
  formNationaloty = dropdownsData.NATIONALITY;
  formCityStates = dropdownsData.STATES;
  formTellUs = dropdownsData.TELL_US;
  formActualPosition = dropdownsData.ACTUAL_POSITION;
  formFamiliesList = [];
  formBirthDate = '2000-01-01';
  formZipData: {
    municipality: string;
    state: string;
    neighborhoods: string[];
  };
  //isFormUploadingFiles = false;
  isInvalidBirthDate = false;
  //uploadFilesError = false;
  validateEmail = false;

  auxNationality = null
  auxCityState = null
  creditLines = null

  public isZipCodesApiDown = false;
  private isNoneCreditLineAlreadySelected = false;
  //public show_extra_file = false;

  public isFirstLoan = true;
  private subs = new SubSink();
  private API_URL = environment.API_URL;
  private loanRequest: LoanRequest;
  public customMessage:any
  public errorMessage:any;

  public show_tell_us_note:any = false


  constructor(
    private store: Store<State>,
    private loanService: LoanService,
    private ClientService: ClientService,
    private router: Router,
    private http: HttpClient,
    private _snackBar: MatSnackBar
  ) {}

  /**
   * @description Converts a angular reactive form value to FormData
   *  in order to be sent over http request.
   * @param formValue The reactive form value
   */
  static toFormData<T>(formValue: T) {
    const formData = new FormData();
    for (const key of Object.keys(formValue)) {
      formData.append(key, formValue[key]);
    }
    return formData;
  }

  ngOnInit() {
    this.initForm();
    this.subs.sink = this.store
      .select<LoanState>(selectLoanState)
      .pipe(
        map(state => ({
          zipData: selectZipData(state),
          form: selectClientForm(state),
          isFirstLoan: state.isFirstLoan,
          familiesList: selectFamiliesList(state)
        }))
      )
      .subscribe(({ form, familiesList, isFirstLoan, zipData }) => {
        this.formFamiliesList = familiesList;
        this.isFirstLoan = isFirstLoan;
        this.formZipData = {
          municipality: zipData.municipio,
          state: zipData.estado,
          neighborhoods: zipData.colonias
        };
        let formValue = {
          ...form
        };

        if (formValue.nationality) {
          var keepGoingNationality = true;
          this.formNationaloty.forEach( (myObject, index) => {
            if(keepGoingNationality) {
              if (formValue.nationality == myObject){
                this.auxNationality = formValue.nationality
                keepGoingNationality = false
              }
              else{
                this.auxNationality = null
              }
            }
          });
          formValue.nationality = this.auxNationality
        }

        if (formValue.state) {       
          var keepGoingState = true;
          this.formCityStates.forEach((myObject, index) => {
            if(keepGoingState) {
              if (formValue.state == myObject){
                this.auxCityState = formValue.state
                keepGoingState = false
              }
              else{
                this.auxCityState = null
              }
            }
          });
          formValue.state = this.auxCityState
        }

        // Validate the ZIP Codes API response is valid
        if (!zipData.municipio || zipData.colonias.length === 0) {
          this.setAddressFieldsAvailability(true);
        } else {
          this.setAddressFieldsAvailability(false);
          // Only change the city and state fields if they are available from the API
          formValue['city'] = zipData.municipio;
          formValue['state'] = zipData.estado;
        }
      
        this.toggleDisabledFields(isFirstLoan);
        if (!!formValue) {
          
          const { credit_lines } = formValue;
          if (!!credit_lines) {
            // @ts-ignore
            formValue.credit_lines = credit_lines.split(',');
          }
          this.form.patchValue(formValue, { emitEvent: false });
          if (formValue.birthdate) {
            this.formBirthDate = formValue.birthdate;
          }
        }
      });
    this.subs.add(this.form.get('postal_code')
      .valueChanges.pipe(
        debounceTime(500),
        map<number, string>(val => !!val ? String(val) : ''),
        filter<string>(val => val.length >= 5),
      ).subscribe(zip => {
        let formData = this.form.getRawValue();
        formData = {
          ...formData,
          credit_lines: !!formData.credit_lines ? formData.credit_lines.join(',') : ''
        };
        this.store.dispatch(saveFormData({ form: formData }));
        this.store.dispatch(fetchZipCodeData({ zip }));
      }));

    
    // Binding email changeValue detection to apply lowercase
    this.form.get('email')
    .valueChanges
    .subscribe({
      next: val => {
        const elRef = this.inputRef.nativeElement;
        // get stored original unmodified value (not including last change)
        const orVal = elRef.getAttribute('data-model-value') || '';
        // modify new value to be equal to the original input (including last change)
        val = val.replace(orVal.toLowerCase(), orVal);
        // store original unmodified value (including last change)
        elRef.setAttribute('data-model-value', val);
        // set view value using DOM value property
        elRef.value = val.toLowerCase();
        this.form.controls['email'].setValue(val, {
          emitEvent: false,
          emitModelToViewChange: false
        });
      }
    })

    document.documentElement.scrollTop = 0;
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  
  hasError(controlName: string, typeError: string): boolean {
    return this.form.get(controlName).hasError(typeError);
  }

  controlTouched(controlName: string): boolean {
    return this.form.get(controlName).touched;
  }

  getLabelControlError(controlName: string, typeError: string): boolean {
    return (
      this.form.get(controlName).hasError(typeError) &&
      this.controlTouched(controlName)
    );
  }

  filterNumberInputs(e) {
    if (this.inputNumberInvalidChars.indexOf(e.key) !== -1) {
      e.preventDefault();
    }
  }

  initForm() {
   
    this.form = new FormGroup({
      names: new FormControl('', [
        Validators.required,
        Validators.maxLength(50)
      ]),
      father_last_name: new FormControl('', [
        Validators.required,
        Validators.maxLength(50)
      ]),
      mother_last_name: new FormControl('', [
        Validators.required,
        Validators.maxLength(50)
      ]),
      gender: new FormControl('Masculino', [Validators.required]),
      birthdate: new FormControl(''),
      nationality: new FormControl('Mexicano', [Validators.maxLength(20)]),
      civil_status: new FormControl(''),
      address: new FormControl('', [Validators.maxLength(50)]),
      outdoor_no: new FormControl('', [Validators.maxLength(50)]),
      interior_no: new FormControl('', [Validators.maxLength(50)]),
      postal_code: new FormControl('',[
          Validators.minLength(5),
          Validators.maxLength(5),
          Validators.pattern('^[0-9]{5}$')
      ]),
      suburb: new FormControl(''),
      city: new FormControl({ value: '', disabled: true }),
      state: new FormControl({ value: '', disabled: true }),
      country: new FormControl({ value: 'México'}),
      house_type: new FormControl(''),
      years_residence: new FormControl('', [Validators.max(99)]),
      phone: new FormControl('', [
        Validators.minLength(10),
        Validators.maxLength(10),
        Validators.pattern('^[0-9]{10}$')
      ]),
      cellphone: new FormControl('', [
        Validators.required,
        Validators.minLength(10),
        Validators.maxLength(10),
        Validators.pattern('^[0-9]{10}$')
      ]),
      email: new FormControl('', [
        Validators.required,
        Validators.email
      ]),
      birth_place: new FormControl(''),
      curp: new FormControl('', [Validators.pattern('^[a-zA-Z]{4}\\d{6}[a-zA-Z]{6}[0-9A-Za-z]{2}$')]),
      rfc: new FormControl(
        { value: '', disabled: true }, [
        Validators.required,
        Validators.pattern('^[A-Za-z]{4}[ |\\\\-]{0,1}[0-9]{6}[ |\\\\-]{0,1}[0-9A-Za-z]{3}$')
      ]),
      study_level: new FormControl(''),
      family_str: new FormControl('', [
        Validators.required,
        Validators.maxLength(50)]),
      actual_position: new FormControl(null, [Validators.maxLength(50)]),
      labor_old: new FormControl('', [
        Validators.required,
        Validators.max(99)
      ]),
      labor_old_month: new FormControl('', [
        Validators.required,
        Validators.max(11)
      ]),
      salary: new FormControl('', [Validators.min(0),Validators.max(999999.99),]),
      family_income: new FormControl('', [Validators.min(0),Validators.max(999999.99)]),
      economic_dependent: new FormControl('', [Validators.max(15)]),
      bank_name: new FormControl('',),
      bank_clabe: new FormControl('', [Validators.pattern('^[0-9]{16}([0-9]{2})?$')]),
      bank_withdraw_deposit: new FormControl('',),
      bank_name_withdraw: new FormControl('',),
      bank_clabe_withdraw: new FormControl('',),
      credit_bureau: new FormControl(''),
      credit_lines: new FormControl([],),
      tell_us: new FormControl('Alimentos'),
      tell_us_note: new FormControl(null),
      family_reference: new FormControl('', [Validators.maxLength(50)]),
      family_reference_phone: new FormControl('', [
        Validators.minLength(10),
        Validators.maxLength(10),
        Validators.pattern('^[0-9]{10}$')
      ]),
      reference: new FormControl('', [Validators.maxLength(50)]),
      reference_phone: new FormControl('', [
        Validators.minLength(10),
        Validators.maxLength(10),
        Validators.pattern('^[0-9]{10}$')
      ]),
      notes: new FormControl(null),
      // below value will be used in API to not validate all required info
      control_myinfo: new FormControl(1),
    });

    this.form.get('credit_lines')
      .valueChanges
      .subscribe(e => this.onCreditLinesChange(e));

    this.form.get('economic_dependent')
      .valueChanges
      .subscribe(e => this.onEconomicDependentChange(e));
  }

  private toggleDisabledFields(isFirstLoan: boolean = true): void {
    const disabledFieldsNames = [
      'names',
      'father_last_name',
      'mother_last_name',
      'gender',
      'birthdate',
      //'nationality',
      'birth_place',
      'curp',
      'email',
      'country'
    ];
    if (!isFirstLoan) {
      for (const fieldName of disabledFieldsNames) {
        this.form.get(fieldName).disable();
      }
      return;
    }
    
    // If this is not the first user loan request, we must enable all the fields
    for (const fieldName of disabledFieldsNames) {
      this.form.get(fieldName).enable();
    }
  }

  private setAddressFieldsAvailability(areEnabled = true) {
    const { form } = this;
    this.isZipCodesApiDown = areEnabled;
    ['city', 'state'].forEach(field => {
      if (areEnabled) {
        form.get(field).enable();
      } else {
        form.get(field).disable();
      }
    });
  }

  getDate(date: string) {
    let date1:any = new Date();
    let date2:any = new Date(date);

    let diff = Math.floor(date1.getTime() - date2.getTime());
    let day = 1000 * 60 * 60 * 24;

    let days = Math.floor(diff/day);
    let months = Math.floor(days/31);
    let years = Math.floor(months/12);

/*
    if (years+1>= 18) {
      this.isInvalidBirthDate = false
    }else{
      this.isInvalidBirthDate= true
    }
*/    
    this.form.get('birthdate').setValue(date);
  }


  public onEconomicDependentChange(value): void {
    const roundedValue = Math.floor(Math.abs(+value));
    this.form.get('economic_dependent').setValue(roundedValue, { emitEvent: false });
  }


  public onCreditLinesChange(value): void {
    let { form } = this;

    // If the user already marked 'Ninguno' and wanted to select other thing
    if (this.isNoneCreditLineAlreadySelected && value.length > 1) {
      let newValues = [ ...value ];
      newValues.pop();
      form.get('credit_lines').setValue(newValues, { emitEvent: false });
      this.isNoneCreditLineAlreadySelected = false;
      return;
    }

    // Check for the disable all option
    if (value.indexOf('Ninguno') === -1) {
      // If there is not 'Ninguno' option, do nothing
      this.isNoneCreditLineAlreadySelected = false;
      return;
    }

    // When none of the above validations are met, all the others values are removed
    form.get('credit_lines').setValue(['Ninguno'], { emitEvent: false });
    this.isNoneCreditLineAlreadySelected = true;
  }


  public changeTellUs() {

    if (this.form.controls.tell_us.value=='Otros (explique)'){
      this.show_tell_us_note = true;
      this.form.get('tell_us_note').setValidators([Validators.required]);
    }
    else{
      this.show_tell_us_note = false;
      this.form.get('tell_us_note').clearValidators();
      this.form.controls.tell_us_note.setValue(null)
    }
     this.form.get('tell_us_note').updateValueAndValidity();

  }


  public onFormSubmit(e): void {
    e.preventDefault();

    if (this.validateEmail) {
      return;
    }

    const { form } = this;
    form.markAllAsTouched();

    if (form.invalid) {
      this.validateEmail = false
      return;
    }

    this.validateEmail = false
    this.errorMessage=""

    //if(form.controls.bank_clabe_withdraw.value === null || this.form.controls.bank_clabe_withdraw.value == '' || this.form.controls.bank_clabe_withdraw.value == '000000000000000000'){
      /* If second bank is not set, gets first bank values*/
      this.form.controls.bank_name_withdraw.setValue(this.form.controls.bank_name.value);
      this.form.controls.bank_clabe_withdraw .setValue(this.form.controls.bank_clabe.value);
    //}

    //validate email in first loan
    const body = { rfc:form.get('rfc').value, email:form.get('email').value }
    this.http.post(`${this.API_URL}/api/v1/client/validate_email_onchange/`, body)

    .subscribe({
      
      next: data => {       
        // Send the files to the API  
        let formData = form.getRawValue();
        if(formData.credit_lines){
          this.creditLines = formData.credit_lines.join(',')
        }

        formData = {
          ...formData,
          credit_lines: this.creditLines
        };
        //console.log(formData)
        
        this.store.dispatch(saveFormData({ form: formData }));
        this.ClientService
        .updateInfo(formData)
        .subscribe(res => {
          
          //this.customMessage = res.message
          //this.router.navigateByUrl('/home');
          this.openSnackBarSuccess(res.message,"Cerrar")
          /*this.ngOnInit();
          window.scroll({
            top: 0,
            left: 0,
            behavior: 'smooth'
          });*/
        },
        error => {
          //this.errorMessage = error.error.error
          this.openSnackBarError(error.error.error,"Cerrar")
          return
        })
 
      },
      error: error => { 
        this.validateEmail = true
        return;
      }

    })
    
  }


  openSnackBarSuccess(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 7000,
      horizontalPosition: "end",
      verticalPosition: "bottom",
      panelClass: ['success-snackbar'],
    });
  }

  openSnackBarError(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 7000,
      horizontalPosition: "end",
      verticalPosition: "bottom",
      panelClass: ['error-snackbar'],
    });
  }


}
