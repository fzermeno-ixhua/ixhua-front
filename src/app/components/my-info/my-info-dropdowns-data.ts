export const STATES = [
  'Aguascalientes',
  'Baja California',
  'Baja California Sur',
  'Campeche',
  'Chiapas',
  'Chihuahua',
  'Ciudad de México',
  'Coahuila de Zaragoza',
  'Colima',
  'Durango',
  'Estado de México',
  'Guanajuato',
  'Guerrero',
  'Hidalgo',
  'Jalisco',
  'Michoacán de Ocampo',
  'Morelos',
  'Nayarit',
  'Nuevo León',
  'Oaxaca',
  'Puebla',
  'Querétaro',
  'Quintana Roo',
  'San Luis Potosí',
  'Sinaloa',
  'Sonora',
  'Tabasco',
  'Tamaulipas',
  'Tlaxcala',
  'Veracruz de Ignacio de la Llave',
  'Yucatán',
  'Zacatecas',
  'Otro'
];

export const MARITAL_STATUS = [
  'Soltero',
  'Casado',
  'Divorciado'
];

export const STUDIES_DEGREE = [
  'Primaria',
  'Secundaria',
  'Preparatoria',
  'Carrera Técnica',
  'Carrera Universitaria',
  'Posgrado'
];

export const BANKS = [
  'Actinver',
  'Afirme',
  'American Express',
  'Banca Azteca SA',
  'Banca MIFEL',
  'Banco del Bajío',
  //'Banco Interacciones, SA de CV',
  'Banco Mercantil del Norte SA (Banorte)',
  'Banco Ve Por Más',
  'Bancoppel',
  'Banjercito',
  'Banobras',
  'Banregio',
  //'Banrural',
  'Bansefi',
  'BANSI',
  'BASE',
  'BBVA Bancomer',
  'CIBANCO',
  'Citibanamex',
  //'Credomatic',
  'HSBC',
  'Inbursa',
  'Intercam Grupo Financiero',
  'MONEX',
  'Multiva',
  //'Rogue Federal Credit Union',
  'Sabadell SA',
  'Santander Serfin',
  'Scotiabank Inverlat',
];

export const CREDIT_BUREAU = [
  'Sin atraso',
  'Menos a 30 días',
  'Menos a 60 días',
  //'Menos a 90 días'
  'Mayor a 61 días'
];

export const CREDIT_LINE = [
  'Crédito personal',
  'Tarjeta de crédito',
  'Hipotecario',
  'Automotriz',
  'Comerciales',
  'Ninguno'
];

export const HOUSING_TYPE = [
  'Propia',
  'Arrendada',
  'Familiar',
  'Hipotecada',
  //'Infonavit'
];

export const NATIONALITY = [
  'Mexicano',
  'Extranjero',
];

export const TELL_US = [
  'Gastos médicos o de salud',
  'Gastos escolares',
  'Reparaciones del hogar',
  'Gastos auto/motocicleta',
  'Alimentos',
  'Pago de servicios',
  'Emergencia / Imprevisto',
  'Compra de regalos',
  'Diversión / Vacaciones',
  'Otras deudas',
  'Otros (explique)',
];

export const ACTUAL_POSITION = [
  'Director',
  'Dueño del negocio',
  'Gerente',
  'Jefe o coordinador de área',
  'Administrativo',
  'Operativo',
  'Obrero',
  'Profesionista independiente',
  'Empleado general',
  'Funcionario de gobierno',
  'Empleado de gobierno',
  'Directivo o empleado de empresa de seguridad privada',
  'Policia de seguridad privada',
  'Escolta',
];


