import {Component, OnInit, ViewEncapsulation,EventEmitter,Output} from '@angular/core';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
import { Store } from '@ngrx/store';
import { State } from 'src/app/store/reducers/auth.reducer';
import * as fromAuth from 'src/app/store/reducers/auth.reducer';
import {Subscription} from 'rxjs';
import { Subject } from 'rxjs';
import { ViewportScroller } from '@angular/common';
import { login } from 'src/app/store/actions';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { selectAuthState } from 'src/app/store/reducers';


import {
  AuthRfcConfirmComponent
} from 'src/app/components';

@Component({
  encapsulation: ViewEncapsulation.None,
  selector: 'login-home',
  templateUrl: './login-home.component.html',
  //styleUrls: ['../login/auth-styles/all.scss']
})
export class LoginHomeComponent implements OnInit {

  form = this.fb.group({
    rfc: new FormControl(null, Validators.required),
    password: new FormControl(null, Validators.required),
    checkbox: new FormControl(false, Validators.required)
  })
  
  private storeSubscription: Subscription;
  safeSrc: SafeResourceUrl;
  parentSubject:Subject<any> = new Subject();
  addOpenedClass: boolean = false;

  @Output() open: EventEmitter<any> = new EventEmitter();
  @Output() close: EventEmitter<any> = new EventEmitter();


  constructor(
    private fb: FormBuilder,
    private viewportScroller: ViewportScroller,
    private store: Store<State>
  ) {}

  ngOnInit(): void {
    window.sessionStorage.clear();
    window.scrollTo(0, 0) 

    this.storeSubscription = this.store.select<State>(selectAuthState)
      .subscribe(({ error }) => {
        if (error) {
          this.form.setErrors({
            invalidCredentials: error.message ? error.message : 'Ocurrió un error inesperado'
          });
        }
      });
    
  }
 
  _checkClickInBack() {
    this.parentSubject.next(true);
  }

  _statusClassOpened(event : false){
    if(event){
      this.addOpenedClass = true;
    } else {
      this.addOpenedClass = false;
    }
  }

   _goToPosition(e: string, fromXS: boolean) {
    this.viewportScroller.scrollToAnchor(e);
    if(fromXS) {
      this.close.emit(false);
    }
  }

  ngOnDestroy(): void {
    this.storeSubscription.unsubscribe();
  }


  public get rfcErrorMessage() {
    const { form } = this;

    if (!form.controls.rfc.errors) {
      return '&nbsp;'; // No error message
    }

    if (form.controls.rfc.errors.required) {
      return 'Este campo es requerido';
    }
    if (form.controls.rfc.errors.pattern) {
      return 'Tu RFC está incorrecto, tienen que ser de 12-13 caracteres (revisa que contenga la homoclave).';
    }

    // No error message
    return '&nbsp;';
  }


  public get passwordErrorMessage() {
    const { form } = this;

    if (!form.controls.password.errors) {
      return '&nbsp;'; // No error message
    }

    if (form.controls.password.errors.required) {
      return 'Este campo es requerido';
    }

    // No error message
    return '&nbsp;';
  }


  public onSubmit(): void {
    const { form } = this;
    form.markAllAsTouched();
    if (!form.valid) {
      return;
    }
    const { rfc, password } = form.value;
    this.store.dispatch(login({ rfc: rfc.toUpperCase(), password }));
  }

 
}
