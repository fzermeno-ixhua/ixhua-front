import { Component, Input, EventEmitter, Output } from '@angular/core';
import { LoanRequestFormComponent } from "../loan-request";

@Component({
  selector: 'app-upload-file',
  templateUrl: './upload-file.component.html',
  styleUrls: ['./upload-file.component.scss']
})
export class UploadFileComponent {
  private file: File;
  imageUrl = null;
  hasPdf = false;
  imageBackground = '';
  @Output() fileChange = new EventEmitter<File>();

  @Input() controlLabel: string;

  /**
   * When file change sets url image for show in front
   * @param event: any
   * @return void
   */
  public onFileChange(event): void {
    if (event.target.files && event.target.files.length) {
      const reader = new FileReader();
      const [file] = event.target.files;
      const fileList: FileList = event.target.files;
      this.file = fileList[0];
      const { type } = this.file;
      const fileExtension = this.file.name.split('.').pop();

      if (fileExtension === 'pdf' && LoanRequestFormComponent.ALLOWED_FILE_TYPES.indexOf(type) !== -1) {
        this.resetBackground();
        this.hasPdf = true;
        this.setImage();
      } else {
        reader.readAsDataURL(file);
        reader.onload = () => {
          const img = new Image();
          img.src = URL.createObjectURL(file);

          img.onload = () => {
            this.imageUrl = img.src;
            this.setImage();
          };
        };
      }
      this.fileChange.emit(this.file);
    }
  }

  /**
   * Set image background style
   * @return void
   */
  private setImage(): void {
    this.imageBackground = !this.imageUrl
      ? 'transparent url(/assets/img/pdf-asset.png) center/auto 80px no-repeat'
      : `transparent url(${this.imageUrl}) center/auto 80px no-repeat`;
  }

  private resetBackground(): void {
    this.hasPdf = false;
    this.imageUrl = null;
    this.hasPdf = false;
    this.imageBackground = null;
  }

  public removeFile(e) {
    e.stopPropagation();
    this.resetBackground();
  }
}
