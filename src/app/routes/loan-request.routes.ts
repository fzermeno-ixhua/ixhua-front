import { LoanRequestContractComponent } from './../components/loan-request/loan-request-contract/loan-request-contract.component';
import { Routes } from '@angular/router';

import {
  LoanRequestComponent,
  LoanRequestCalculatorComponent,
  LoanRequestFormComponent,
  LoanRequestDocumentsComponent,
} from 'src/app/components';

export const LOAN_REQUEST_ROUTES: Routes = [
  {
    path: '',
    component: LoanRequestComponent,
    children: [
      {
        path: '',
        component: LoanRequestCalculatorComponent,
        data: { animation: 'Calculator' }
      },
      {
        path: 'form',
        component: LoanRequestFormComponent,
        data: { animation: 'Form' }
      },
      {
        path: 'contract',
        component: LoanRequestContractComponent,
        data: { animation: 'Contract' }
      },
      {
        path: 'documents',
        component: LoanRequestDocumentsComponent,
        data: { animation: 'Documents' }
      }
    ]
  }
];
