import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router} from '@angular/router';
import {Store} from '@ngrx/store';
import {selectClientToken, State} from '../../store/reducers';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CanActivateAuthGuard implements CanActivate {
  constructor(
    private store: Store<State>,
    private router: Router
  ) {  }

  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
    return this.store.select<string>(selectClientToken)
      .pipe(
        map(token => {
          if (!token) {
            return true;
          }
          this.router.navigateByUrl('/home/my-dashboard');
          return false;
        })
      );
  }
}
