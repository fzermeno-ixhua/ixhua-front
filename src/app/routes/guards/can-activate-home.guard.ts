import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { State } from 'src/app/store/reducers';
import { Observable } from 'rxjs';
import { selectClientToken } from 'src/app/store/reducers';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CanActivateHomeGuard implements CanActivate {
  constructor(
    private store: Store<State>,
    private router: Router
  ) {  }

  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
    return this.store.select<string | null>(selectClientToken)
      .pipe(
        map(token => {
          if (!token) {
            this.router.navigateByUrl('/auth/login');
            return false;
          }
          return true;
        })
      );
  }

}
