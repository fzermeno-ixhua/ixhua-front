import { Routes } from '@angular/router';

import {
  AuthComponent,
  AuthFormComponent,
  AuthCreatePasswordComponent,
  AuthRfcConfirmComponent,
  AuthPinComponent,
  AuthForgotPasswordComponent,
  AuthExpiredRecoveryComponent,
  ActivateCountCellComponent,
  ActivateAccountEmailComponent,
  ActivationPinComponent,
  CreatePasswordComponent,
  ClientInfoValidationComponent,
  ClientInfoValidationDocumentsComponent,
  LoginHomeComponent,
} from 'src/app/components';

export const LOGIN_ROUTES: Routes = [
  {
    path: '',
    component: AuthComponent,
    children: [
      {
        path: '',
        component: AuthRfcConfirmComponent
      },
      {
        path: 'pin',
        component: AuthPinComponent
      },
      {
        path: 'create-password',
        component: AuthCreatePasswordComponent
      },
    ]
  },
  {
    path: 'activate-cell',
    component: ActivateCountCellComponent,
  },
  {
    path: 'activate-email',
    component: ActivateAccountEmailComponent,
  },
  {
    path: 'activation-pin',
    component: ActivationPinComponent,
  },
  {
    path: 'activate-password',
    component: CreatePasswordComponent,
  },
  {
    path: 'info-validation',
    component: ClientInfoValidationComponent,
  },
  /*{
    path: 'documents-validation',
    component: ClientInfoValidationDocumentsComponent,
  },*/
  {
    path: 'forgot',
    component: AuthForgotPasswordComponent
  },
  {
    path: 'reset/:token/:timestamp',
    component: AuthCreatePasswordComponent
  },
  {
    path: 'invalid-recovery',
    component: AuthExpiredRecoveryComponent
  },
  {
    path: 'login',
    //component: AuthFormComponent
    component: LoginHomeComponent
  },
];
