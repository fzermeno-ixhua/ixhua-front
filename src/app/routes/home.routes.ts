import { HomeComponent } from './../components/home/home.component';
import { Routes } from '@angular/router';
import { 
  MyLoansComponent, 
  OptionsComponent, 
  OptionsChangePasswordComponent, 
  MyDocumentsComponent, 
  MyInfoComponent,
  MyPaymentsComponent, 
  MyProfileComponent,
  MyContractComponent,
  MyDashboardComponent,
  HrMyteamComponent,
  HrPersonComponent,
  HrMymessagesComponent,
  MyHelpComponent
} from '../components';

export const HOME_ROUTES: Routes = [
  {
    path: '',
    component: HomeComponent,
    children: [
      {
        path: '',
        loadChildren: () =>
          import('../modules/loan-request.module').then(
            mod => mod.LoanRequestModule
          )
      },
      { path: 'loans', component: MyLoansComponent },
      { path: 'options', component: OptionsComponent },
      { path: 'change', component: OptionsChangePasswordComponent },
      { path: 'my-documents', component: MyDocumentsComponent },
      { path: 'my-info', component: MyInfoComponent },
      { path: 'my-payments', component: MyPaymentsComponent },
      { path: 'my-profile', component: MyProfileComponent },
      { path: 'my-contract', component: MyContractComponent },
      { path: 'help', component: MyHelpComponent },
      { path: 'dashboard', component: MyDashboardComponent },
      /* Human Resources Paths, must be transfered into its own Module */ 
      { path: 'hr-myteam/:id', component: HrPersonComponent },
      { path: 'hr-myteam', component: HrMyteamComponent },
      { path: 'hr-mymessages', component: HrMymessagesComponent },
      /* Wildcard Path - It must be place always at the end of the paths - Angular Routes uses a first-match wins strategy when matching route */
      { path: '**', component: MyDashboardComponent },
    ]
  }
];
