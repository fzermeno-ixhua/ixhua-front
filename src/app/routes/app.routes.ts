import { HomeComponent } from './../components/home/home.component';
import { Routes } from '@angular/router';
import { CanActivateHomeGuard } from './guards/can-activate-home.guard';
import { CanActivateAuthGuard } from './guards/can-activate-auth.guard';


import { 
  FaqsComponent,
  TycComponent,
  ArcoComponent,
  PrivacyComponent,
  BusinessComponent, } from '../components/pages';

export const APP_ROUTES: Routes = [
  {
    path: '*',
    redirectTo: '',
    pathMatch: 'full'
  },
  {
    path: '',
    redirectTo: '/auth',
    pathMatch: 'full'
  },
  {
    path: 'pin/:pin/:email',
    redirectTo: 'auth/pin'
  },
  {
    path: 'login',
    redirectTo: 'auth/login'
  },
  {
    path: 'auth',
    canActivate: [CanActivateAuthGuard],
    loadChildren: 'src/app/modules/login.module#LoginModule'
  },
  {
    path: 'register/:code',
    canActivate: [CanActivateAuthGuard],
    loadChildren: 'src/app/modules/login.module#LoginModule'
  },
  {
    path: 'home',
    canActivate: [CanActivateHomeGuard],
    loadChildren: () =>
      import('../modules/home.module').then(mod => mod.HomeModule)
  },
  {
    path: 'faqs',
    component: FaqsComponent,
  },
  {
    path: 'tyc',
    component: TycComponent,
  },
  {
    path: 'arco',
    component: ArcoComponent,
  },
  {
    path: 'privacy',
    component: PrivacyComponent,
  },
  {
    path: 'empresas',
    component: BusinessComponent,
  },
  {
    path: '**',
    redirectTo: '/auth',
    pathMatch: 'full'
  }

];
