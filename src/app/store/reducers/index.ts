import {
  ActionReducer,
  ActionReducerMap,
  createAction,
  createFeatureSelector,
  createSelector,
  MetaReducer, props
} from '@ngrx/store';
import * as fromAuth from './auth.reducer';
import * as fromLoan from './loan-request.reducer';
import { localStorageSync } from 'ngrx-store-localstorage';

export interface State {
  auth: fromAuth.State;
  loan: fromLoan.State;
}

export const reducers: ActionReducerMap<State> = {
  auth: null,
  loan: null,
};

/** The meta reducers are the same on prod and dev, so no condition is needed
 * (This could change)
 */
export const metaReducers: MetaReducer<State>[] = [
  localStorageSyncReducer,
  clearStateReducer,
];

// Used to persist storage
export function localStorageSyncReducer(reducer: ActionReducer<any>): ActionReducer<any> {
  return localStorageSync({ keys: ['auth', 'loans'], rehydrate: true, storage: sessionStorage })(reducer);
}

export function clearStateReducer(reducer: ActionReducer<any>): ActionReducer<any> {
  return (state, action) => {
    if (action === clearState) {
      sessionStorage.clear();
      return reducer({
        auth: null,
        loan: null
      }, action);
    }
    return reducer(state, action);
  };
}

/**
 * @description This action is used to clear all the store
 * this happens when the user is redirected back to the login
 * and when the browser emits a close event.
 */
export const clearState = createAction(
  '[Main] Clearing the whole thing'
);

/**
 * @description These actions are used when the client wants to delete their account
 */
export const deleteAccount = createAction(
  '[Main] Deleting user account'
);

export const deleteAccountSuccess = createAction(
  '[Main] Successfully deleted the client account'
);

export const deleteAccountFailed = createAction(
  '[Main] Something went wrong while deleting client\'s account',
  props<{ error: string }>()
);

/**
 * Selectors for the loan request feature
 */
export const selectLoanState = createFeatureSelector<fromLoan.State>('loan');
export const selectClientLoans = createSelector(
  selectLoanState,
  fromLoan.selectClientLoans
);
export const selectClientSeeds = createSelector(
  selectLoanState,
  fromLoan.selectClientSeeds
);
/**
 * Selectors for the auth state feature
 */
export const selectAuthState = createFeatureSelector<fromAuth.State>('auth');
export const selectAuthUser = createSelector(
  selectAuthState,
  fromAuth.selectAuthUser
);
export const selectClientToken = createSelector(
  selectAuthState,
  fromAuth.selectClientToken
);
export const selectClient = createSelector(
  selectAuthState,
  fromAuth.selectClient
);
