import { Action, createReducer, on } from '@ngrx/store';
import * as LoanActions from 'src/app/store/actions';
import {ContractData, GetClientLoansResponse, RequestLoanForm} from 'src/app/models';
import {LoanInfo} from '../../models/interfaces/ClientLoansResponse';

export interface State {
  loanInfo?: GetClientLoansResponse;
  seeds: {
    gray: number;
    green: number;
    yellow: number;
  };
  error?: any;
  loanOptions?: {
    date: string;
    aperture: number,
    interest: number;
    total: number;
    cat: number;
    tfa: number;
  }[];
  contractExtraData?: {
    accountType: string;
    amount: string;
    amountLetters: string;
    annualRegularInterest: number;
    optionSpecific: {
      term: number;
      interest: number;
      interestToPayInLetters: string;
    }[];
  };
  clientForm?: RequestLoanForm;
  isFirstLoan?: boolean;
  familiesList?: { id: number, name: string }[];
  loan: {
    amount: number;
    option: number;
  };
  loanRequestSuccessful: boolean;
  zipData?: {
    colonias: string[];
    municipio: string;
    estado: string;
    error?: any;
  };
  loans?: LoanInfo[];
  disableCalculatorNextButton: boolean;
  contract: ContractData;
}

export const initialState: State = {
  loan: {
    amount: 0,
    option: 1
  },
  seeds: {
    green: 5,
    yellow: 0,
    gray: 0,
  },
  loanRequestSuccessful: false,
  zipData: {
    municipio: '',
    estado: '',
    colonias: []
  },
  disableCalculatorNextButton: false,
  contractExtraData: null,
  contract: {
    websiteurl: '//www.ixhua.com.mx'
  }
};

export const loanReducer = createReducer(
  initialState,
  on(
    LoanActions.getClientLoansInfo,
    LoanActions.loanOptions,
    LoanActions.clientForm,
    LoanActions.getFamilies,
    LoanActions.loanRequest,
    LoanActions.fetchClientLoans,
      state => ({
        ...state,
        error: null
      })
  ),
  on(
    LoanActions.fetchZipCodeData,
    (state, { zip }) => ({
      ...state,
      clientForm: {
        ...state.clientForm,
        postal_code: zip
      },
      error: null
    })
  ),
  on(
    LoanActions.getClientLoansInfoSuccess,
    (state, { active_loan, available_loan, total_loan, seeds, min_loan, loan_unit, days_worked, days_to_loan }
  ) => ({
    ...state,
    seeds,
    loanInfo: {
      active_loan,
      available_loan,
      total_loan,
      min_loan,
      loan_unit,
      days_worked,
      days_to_loan,
    }
  })),
  on(LoanActions.loanOptionsSuccess, (state, action) => {
    const { options, extra_data } = action;
    const { account_type, amount, amount_letters, regular_interest_anual } = extra_data;
    return {
      ...state,
      disableCalculatorNextButton: false,
      loanOptions: options,
      contractExtraData: {
        accountType: account_type,
        amount,
        amountLetters: amount_letters,
        annualRegularInterest: regular_interest_anual,
        optionSpecific: options.map((option) => ({
          term: option.term,
          interest: option.tfa,
          interestToPayInLetters: option.tfa_letters,
        })),
      }
    };
  }),
  on(
    LoanActions.saveFormData,
    (state, { form }) => ({
      ...state,
      clientForm: form
    })
  ),
  on(LoanActions.clientFormSuccess, (state, { form, isFirstLoan, contractData }) => ({
    ...state,
    clientForm: form,
    isFirstLoan,
    contract: {
      ...contractData
    }
  })),
  on(
    LoanActions.getFamiliesSuccess,
    (state, { families }) => ({
      ...state,
      familiesList: families
    })
  ),
  on(
    LoanActions.loanRequestSuccess,
    (state, { success }) => ({
      ...state,
      loanRequestSuccessful: success
    })
  ),
  on(
    LoanActions.saveLoanOptions,
    (state, { amount, option }) => ({
      ...state,
      loan: {
        amount,
        option
      }
    })
  ),
  on(LoanActions.fetchZipCodeDataSuccess, (state, { colonias, municipio, estado, codigo_postal }) => ({
    ...state,
    clientForm: {
      ...state.clientForm,
      postal_code: `${codigo_postal}`
    },
    zipData: {
      colonias,
      municipio,
      estado
    }
  })),
  on(
    LoanActions.fetchClientLoansSuccess,
    (state, { data: loans }) => ({
      ...state,
      loans
    })
  ),
  // ERRORS
  on(
    LoanActions.loanRequestFail,
    (state, { error, errors }) => ({
      ...state,
      error,
      loanErrors: errors
    })
  ),
  on(LoanActions.fetchZipCodeDataFail, (state, { error }) => ({
    ...state,
    zipData: {
      ...state.zipData,
      error
    }
  })),
  on(
    LoanActions.getClientLoansFail,
    LoanActions.clientFormFail,
    LoanActions.getFamiliesFail,
    LoanActions.fetchClientLoansFail,
    (state, error) => ({
      ...state,
      error
    })
  ),
  on(
    LoanActions.loanOptionsFail,
    (state, error) => ({
      ...state,
      error,
      disableCalculatorNextButton: true
    })
  )
);

export function reducer(s: State, a: Action) {
  return loanReducer(s, a);
}
/* Selectors */
export const selectClientSeeds = (state: State) => {
  try {
    return state.seeds;
  } catch (e) {
    return {
      gray: 0,
      green: 5,
      yellow: 0,
    };
  }
};
export const selectClientLoans = (state: State) => {
  try {
    return state.loans;
  } catch (e) {
    console.error('[Loan state selector] key does not exist in state object.');
    return [];
  }
};
export const selectContractData = (state: State) => {
  try {
    const { contractExtraData, loan, contract, clientForm, loanOptions } = state;
    const selectedLoanOptions = loanOptions[ loan.option - 1 ];
    const {
      curp,
      rfc,
      nationality,
      gender,
      bank_clabe: clabe,
      bank_name: bankName,
      bank_clabe_withdraw: clabeWithdraw,
      bank_name_withdraw: bankNameWithdraw,
      tell_us: loanDestination // TODO: Search this property name in the contract object `ContractData`
    } = clientForm;
    const finalContractFormData = {
      ...contractExtraData,
      ...contractExtraData.optionSpecific[ loan.option - 1 ],
      ...contract,
      curp,
      rfc,
      nationality,
      gender,
      clabe,
      bankName,
      clabeWithdraw,
      bankNameWithdraw,
      loanDestination,
      payments: loan.option,
      paymentDate: selectedLoanOptions.date,
      interestToPay: selectedLoanOptions.interest,
      aperture: selectedLoanOptions.aperture,
      totalAmount: selectedLoanOptions.total,
      cat: selectedLoanOptions.cat,
      tfa: selectedLoanOptions.tfa,
      company: contract.family,
    };
    delete finalContractFormData.optionSpecific;
    return  finalContractFormData;
  } catch (e) {
    console.error('[Loan request reducer] ', e);
    return null;
  }
};
export const selectLoanRequest = (state: State) => {
  try {
    return {
      loan: state.loan,
      client: state.clientForm
    };
  } catch (e) {
    return null;
  }
};
export const selectZipData = (state: State) => {
  try {
    return state.zipData;
  } catch (e) {
    return {
      municipio: '',
      estado: '',
      colonias: []
    };
  }
};
export const selectFamiliesList = (state: State) => {
  try {
    return state.familiesList;
  } catch (e) {
    return [];
  }
};
export const selectMinLoan = (state: State) => {
  try {
    return state.loanInfo.min_loan;
  } catch (e) {
    return 0;
  }
};
export const selectMaxLoan = (state: State) => {
  try {
    return state.loanInfo.available_loan;
  } catch (e) {
    return 0;
  }
};
export const selectDaysWorked = (state: State) => {
  try {
    return state.loanInfo.days_worked;
  } catch (e) {
    return 0;
  }
};
export const selectDaysToLoan = (state: State) => {
  try {
    return state.loanInfo.days_to_loan;
  } catch (e) {
    return 0;
  }
};
export const selectClientForm = (state: State) => {
  try {
    return state.clientForm;
  } catch (e) {
    return null;
  }
};
export const selectLoanOptions = (state: State) => {
  if (!state) {
    return [];
  }
  if (!state.loanOptions) {
    return [];
  }
  return state.loanOptions;
};
