import * as AuthActions from 'src/app/store/actions/auth.actions';
import {AuthUser, Client } from 'src/app/models';
import { Action, createReducer, on } from '@ngrx/store';

export interface State {
  authUser?: AuthUser;
  isPinConfirmed?: boolean;
  isRfcConfirmed?: boolean;
  isCodeConfirmed?: boolean;
  forgotEmailSent?: boolean;
  isFirstLogin: boolean;
  client?: Client;
  error?: any;
  forgotEmailNotification ?:string;
}

export const initialState: State = {
  authUser: new AuthUser(),
  isFirstLogin: false
};

export const authReducer = createReducer(
  initialState,
  on(AuthActions.confirmRfc, (state, { rfc }) => {
    return {
      ...state,
      authUser: { ...state.authUser, rfc }
    };
  }),
  on(AuthActions.confirmRfcSuccess, (state, { success, email }) => {
    return {
      ...state,
      authUser: { ...state.authUser,mutedEmail: email },
      isRfcConfirmed: success,
      error: null
    };
  }),
  on(AuthActions.confirmRfcFail, (state, { error: e }) => ({
    ...state,
    error: e,
    isRfcConfirmed: false
  })),
  on(AuthActions.confirmCode, (state, { code }) => {
    return {
      ...state,
      authUser: { ...state.authUser, code }
    };
  }),
  on(AuthActions.confirmCodeSuccess, (state, { success }) => {
    return {
      ...state,
      authUser: { ...state.authUser,},
      isCodeConfirmed: success,
      error: null
    };
  }),
  on(AuthActions.validatePin, (state, { pin: p }) => ({
    ...state,
    authUser: { ...state.authUser, pin: p }
  })),
  on(AuthActions.validatePinSuccess, (state, { success }) => ({
    ...state,
    isPinConfirmed: success,
    error: null
  })),
  on(AuthActions.changePassword, (state, { password: p }) => ({
    ...state,
    authUser: { ...state.authUser, password: p }
  })),
  on(AuthActions.forgotPasswordSuccess, (state,{email:e}) => ({
    ...state,
    forgotEmailSent: true,
    forgotEmailNotification: e
  })),
  on(AuthActions.forgotPasswordEmailReset, state => ({
    ...state,
    forgotEmailSent: false
  })),
  on(AuthActions.login, (state, { rfc }) => ({
    ...state,
    client: new Client(rfc),
    error: null
  })),
  on(
    AuthActions.loginSuccess,
    AuthActions.changePasswordSuccess,
    AuthActions.resetPasswordSuccess,
    (state, { token: t }) => ({
      ...state,
      error: null,
      client: {
        ...state.client,
        token: t
      }
    })
  ),
  on(AuthActions.saveEmail, (state, { email }) => ({
    ...state,
    isRfcConfirmed: true,
    authUser: {
      ...state.authUser,
      mutedEmail: email
    },
  })),
  on(AuthActions.savePinRouteParams, (state, { pin, email }) => ({
    ...state,
    authUser: {
      ...state.authUser,
      pin: atob(pin),
      mutedEmail: atob(email)
    }
  })),
  on(AuthActions.firstLogin, state => ({
    ...state,
    isFirstLogin: true
  })),
  on(
    AuthActions.whoAmISuccess,
    (state, { user }) => ({
      ...state,
      client: {
        ...state.client,
        ...user
      }
    })
  ),
  on(
    AuthActions.validatePinFail,
    AuthActions.changePasswordFail,
    AuthActions.resetPasswordFail,
    AuthActions.forgotPasswordFail,
    AuthActions.whoAmIFail,
    AuthActions.loginFail, (state, { error: e }) => ({
      ...state,
      error: e
    })),
  on(AuthActions.logout, () => initialState)
);

export function reducer(state: State | undefined, action: Action) {
  return authReducer(state, action);
}

export const selectAuthUser = (state: State) => state.authUser;
export const selectClient = (state: State) => {
  try {
    return state.client;
  } catch (e) {
    console.error(e, 'Client not defined in store');
    return null;
  }
};
export const selectClientToken = (state: State) => {
  if (!state) {
    return null;
  }
  const { client } = state;
  if (client) {
    return client.token ? client.token : null;
  }
  return null;
};
