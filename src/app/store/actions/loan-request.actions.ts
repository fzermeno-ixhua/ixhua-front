import { createAction, props } from '@ngrx/store';
import {
  FamiliesListResponse,
  GetClientLoansResponse,
  LoanCalculatorRequest,
  LoanCalculatorResponse,
  LoanRequest,
  LoanResponse,
  RequestLoanForm,
  ZipCodeResponse,
  ClientLoansResponse
} from 'src/app/models';

/**
 * Basic loan info for the client
 */
export const getClientLoansInfo = createAction(
  '[Loan] Asking for the loan info'
);

export const getClientLoansInfoSuccess = createAction(
  '[Loan] Success asking for client loans',
  props<GetClientLoansResponse>()
);

export const getClientLoansFail = createAction(
  '[Loan] An error happened while fetching client loans',
  props<{ error: any }>()
);

/**
 * The loan calculator depending on the amount input by the user
 */
export const loanOptions = createAction(
  '[Loan] Fetching the options for the selected amount',
  props<LoanCalculatorRequest>()
);

export const loanOptionsSuccess = createAction(
  '[Loan] Successfully fetched loan options',
  props<LoanCalculatorResponse>()
);

export const loanOptionsFail = createAction(
  '[Loan] Failed to fetch loan options',
  props<{ error: any }>()
);

/**
 * Actions to fetch the stored data about the current client
 */
export const clientForm = createAction(
  '[Loan] Fetching the current data from the user'
);

export const clientFormSuccess = createAction(
  '[Loan] Successfully fetched client form data',
  props<{ form: RequestLoanForm, isFirstLoan: boolean, contractData: any }>() // TODO: Type the `contractData` prop
);

export const clientFormFail = createAction(
  '[Loan] An error happened while fetching client form data',
  props<{ error: any }>()
);

/**
 * This action fires when the client "submits" the form
 */
export const saveFormData = createAction(
  '[Loan] Saving form data',
  props<{ form: RequestLoanForm }>()
);

// TODO: Remove all the funcionality related to the families service
/**
 * Actions to fetch, handle and store families for the client form
 */
export const getFamilies = createAction(
  '[Loan] Fetching available families list'
);

export const getFamiliesSuccess = createAction(
  '[Loan] Successfully fetched families list',
  props<FamiliesListResponse>()
);

export const getFamiliesFail = createAction(
  '[Loan] Failed to get families list',
  props<{ error: any }>()
);

/**
 * Actions for actually sending loan request
 */
export const loanRequest = createAction(
  '[Loan] Sending loan request',
  props<LoanRequest>()
);

export const loanRequestSuccess = createAction(
  '[Loan] Successfully sent the loan request',
  props<LoanResponse>()
);

export const loanRequestFail = createAction(
  '[Loan] Failed the submit for the loan request',
  props<{ error: any, errors?: { [s: string]: string } }>()
);

/**
 * Save the selected option & amount
 */
export const saveLoanOptions = createAction(
  '[Loan] Saving the option & the amount',
  props<{ option: number, amount: number }>()
);

/**
 * This actions are used to fetch data with the ZIP code
 * on the client form.
 */
export const fetchZipCodeData = createAction(
  '[Loan] Fetching the colonies for the ZIP code',
  props<{ zip: string }>()
);

export const fetchZipCodeDataSuccess = createAction(
  '[Loan] Successfully fetched zip code data',
  props<ZipCodeResponse>()
);

export const fetchZipCodeDataFail = createAction(
  '[Loan] Failed to fetch the zip code data',
  props<{ error: any }>()
);

/**
 * Actions for the "Mis prestamos" section
 */
export const fetchClientLoans = createAction(
  '[Loan] Fetching client loans'
);

export const fetchClientLoansSuccess = createAction(
  '[Loan] Succesfully fetched client loans',
  props<ClientLoansResponse>()
);

export const fetchClientLoansFail = createAction(
  '[Loan] Failed to fetch client loans',
  props<{ error: any }>()
);
