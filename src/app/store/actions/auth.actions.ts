import { createAction, props } from '@ngrx/store';
import {
  ConfirmRFCResponse,
  ChangePasswordRequest,
  ChangePasswordResponse,
  LoginRequest,
  LoginResponse,
  ResetPasswordEmailRequest,
  ResetPasswordEmailResponse,
  ResetPasswordRequest,
  ResetPasswordResponse, 
  WhoAmIResponse
} from 'src/app/models';

/**
 * RFC Confirmation actions
 */
export const confirmRfc = createAction(
  '[Auth] RFC submitted to auth',
  props<{ rfc: string }>()
);

export const confirmRfcSuccess = createAction(
  '[Auth] RFC confirmed successfully',
  props<ConfirmRFCResponse>()
);

export const confirmRfcFail = createAction(
  '[Auth] RFC confirmation failed',
  props<{ error: any }>()
);

/**
 * Activation Code actions
 */
export const confirmCode = createAction(
  '[Auth] Activation Code submitted to auth',
  props<{ code: string }>()
);

export const confirmCodeSuccess = createAction(
  '[Auth] Activation Code confirmed successfully',
  props<{ success: boolean }>()
);

export const confirmCodeFail = createAction(
  '[Auth] Activation Code confirmation failed',
  props<{ error: any }>()
);


/**
 * PIN validation actions
 */
export const validatePin = createAction(
  '[Auth] PIN submitted for validation',
  props<{ pin: string }>()
);

export const validatePinSuccess = createAction(
  '[Auth] PIN validated successfully',
  props<{ success: boolean }>()
);

export const validatePinFail = createAction(
  '[Auth] PIN validation failed',
  props<{ error: any }>()
);

/**
 * Change password after PIN validation actions
 */
export const changePassword = createAction(
  '[Auth] Password change submitted',
  props<ChangePasswordRequest>()
);

export const changePasswordSuccess = createAction(
  '[Auth] Changed password successfully',
  props<ChangePasswordResponse>()
);

export const changePasswordFail = createAction(
  '[Auth] Can not change password',
  props<{ error: any }>()
);

/**
 * Login actions
 */
export const login = createAction(
  '[Auth] User login submitted',
  props<LoginRequest>()
);

export const loginSuccess = createAction(
  '[Auth] Logged in successfully',
  props<LoginResponse>()
);

export const loginFail = createAction(
  '[Auth] Login failed',
  props<{ error: any }>()
);

/**
 * Password recovery actions (email)
 */
export const forgotPasswordEmail = createAction(
  '[Auth] Password recovery request',
  props<ResetPasswordEmailRequest>()
);

export const forgotPasswordSuccess = createAction(
  '[Auth] Forgot password mail sent successfully',
  props<ResetPasswordEmailResponse>()
);

export const forgotPasswordFail = createAction(
  '[Auth] Password email recovery send failed',
  props<{ error: any }>()
);

export const forgotPasswordEmailReset = createAction(
  '[Auth] Setting the sentEmail flag to false'
);

/**
 * Password recovery actions (actual recovery)
 */
export const resetPassword = createAction(
  '[Auth] Reset password',
  props<ResetPasswordRequest>()
);

export const resetPasswordSuccess = createAction(
  '[Auth] Reset password success',
  props<ResetPasswordResponse>()
);

export const resetPasswordFail = createAction(
  '[Auth] Reset password fail',
  props<{ error: any }>()
);


// Email received from service
export const saveEmail = createAction(
  '[Auth] Saving email',
  props<{ email: string }>()
);

// PIN and email received from router event
export const savePinRouteParams = createAction(
  '[Auth] Saving pin and email',
  props<{ pin: string, email: string }>()
);

// To determine if show the welcome dialog on first login
export const firstLogin = createAction(
  '[Auth] This is the first client login'
);

/***
 * Who I am actions
 */
export const whoAmI = createAction(
  '[Auth] Who Am I Request'
);

export const whoAmIFail = createAction(
  '[Auth] The who am I request failed',
  props<{ error: any }>()
);

export const whoAmISuccess = createAction(
  '[Auth] Successfully fetched the user information',
  props<{ success: boolean, user: WhoAmIResponse }>()
);

// Logout
export const logout = createAction(
  '[Auth] Loggin out the cient'
);
