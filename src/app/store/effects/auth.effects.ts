import { Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import {catchError, map, switchMap, take, tap} from 'rxjs/operators';
import { Router } from '@angular/router';
import { of } from 'rxjs';
import * as AuthActions from 'src/app/store/actions/auth.actions';
import { AuthService } from 'src/app/services/auth.service';
import { Store } from '@ngrx/store';
import { State } from 'src/app/store/reducers';
import { MatDialog } from '@angular/material';
import { FirstLoginDialogComponent } from 'src/app/components/first-login-dialog/first-login-dialog.component';
import {AuthEndpointsService} from '../../services/auth-endpoints/auth-endpoints.service';
import { Gtag } from 'angular-gtag';

@Injectable()
export class AuthEffects {

  constructor(
    private actions$: Actions,
    private router: Router,
    private authService: AuthService,
    private authEndpointsService: AuthEndpointsService,
    private store: Store<State>,
    private dialog: MatDialog,
    public gtag : Gtag,
  ) {  }

  rfcConfirmation$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.confirmRfc),
      switchMap(({ rfc }) =>
        this.authService.confirmRfc({ rfc })
          .pipe(
            map((res) => {
              if (!res.success) {
                if (res.error == 'Validando RFC.') {
                  this.gtag.event('click', { 
                   method: 'soy-nuevo',
                   event_category: 'formulario',
                   event_label: 'soy-nuevo'
                   });
                  this.router.navigate(['/auth/info-validation'])
                }
                else if(res.error == 'CAPTURED_RFC'){
                  this.router.navigate(['/auth/info-validation'])
                  return AuthActions.confirmRfcFail({ error: { message: "Registro incompleto - Paso 1" } });
                }
                else if((res.error == 'CAPTURED_INFORMATION')||(res.error == 'CAPTURED_DOCUMENTS')){
                  this.router.navigate(['/auth/info-validation'])
                  return AuthActions.confirmRfcFail({ error: { message: "Registro incompleto - Paso 2" } });
                }
                else if(res.error == 'PIN_SENT'){
                  this.router.navigate(['/auth/activation-pin'])
                  return AuthActions.confirmRfcFail({ error: { message: "Registro incompleto - Paso 3" } });
                }
                
                return AuthActions.confirmRfcFail({ error: { message: res.error } });
                
              }
              if (res.success) {
                this.router.navigateByUrl('/auth/activate-cell');
                return AuthActions.confirmRfcSuccess(res);
              }
            }),
            catchError(err => of(AuthActions.confirmRfcFail(err)))
          )
      )
    )
  );

  pinValidation$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.validatePin),
      switchMap(({ pin }) =>
        this.authService.validatePin({ pin })
          .pipe(
            map(({ success, error }) => {
              if (!success) {
                return AuthActions.validatePinFail({ error: { message: error } });
              }
              this.router.navigateByUrl('/auth/create-password');
              return AuthActions.validatePinSuccess({ success });
            }),
            catchError(({ error }) => of(AuthActions.validatePinFail({ error: { message: error.error } })))
          )
      )
    )
  );

  changePassword$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.changePassword),
      switchMap( ({ pin, password,passwordConfirm }) => {
        this.store.dispatch(AuthActions.firstLogin());
        return this.authService.changePasswordAfterPin({ pin, password, passwordConfirm })
          .pipe(
            // This token is the API response to auth the client
            map(({ error, token, success }) => {
              if (!success) {
                return AuthActions.changePasswordFail({ error: { message: error }});
              }
              return AuthActions.changePasswordSuccess({ success, token });
            }),
            catchError(e => of(AuthActions.changePasswordFail(e)))
          );
        }
      )
    )
  );

  firstLoginModal$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.firstLogin),
      take(1),
      tap(() => {
        const dialogRef = this.dialog.open(FirstLoginDialogComponent, {
          width: '700px',
          panelClass: 'first-login-dialog',
          disableClose: true
        });
        setTimeout(() => dialogRef.close(), 5 * 1000);
      })
    ), { dispatch: false }
  );

  forgotPasswordEmail$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.forgotPasswordEmail),
      switchMap(({ rfc }) =>
        this.authService.forgotPassword({ rfc })
          .pipe(
            map(({ success, error,email }) => {
              if (!!error) {
                return AuthActions.forgotPasswordFail({
                  error: !!error ? error : 'Ocurrió un error insesperado.'
                });
              }
              return AuthActions.forgotPasswordSuccess({ success,email });
            }),
            catchError(({ error: e }) => of(AuthActions.forgotPasswordFail(e)))
          )
      )
    )
  );

  resetPassword$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.resetPassword),
      switchMap(({ token, password }) =>
        this.authService.changePassword({ token, password })
          .pipe(
            map(({ token: t, error }) => {
              if (error) {
                return AuthActions.resetPasswordFail({
                  error: {
                    message: error.message ? error.message : 'Ocurrió un error inesperado.'
                  }
                });
              }
              return AuthActions.resetPasswordSuccess({ token: t });
            }),
            catchError(({ error }) => of(AuthActions.resetPasswordFail({
              error: error ? error : 'Ocurrió un error inesperado.'
            })))
          )
      )
    )
  );

  clientLogin$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.login),
      switchMap(({ rfc, password }) =>
        this.authService.clientLogin({ rfc, password })
          .pipe(
            map(({ token, error }) => {
              if (error) {
                return AuthActions.loginFail({ error: { message: error } });
              }
              this.gtag.event('click', { 
                method: 'login',
                event_category: 'formulario',
                event_label: 'login'
              });
              return AuthActions.loginSuccess({ token });
            }),
            catchError(e => of(AuthActions.loginFail(e)))
          )
      )
    )
  );

  redirectAfterLogin$ = createEffect(() =>
      this.actions$.pipe(
        ofType(
          AuthActions.loginSuccess,
          AuthActions.changePasswordSuccess,
          AuthActions.resetPasswordSuccess
        ),
        map(() => {
          this.router.navigateByUrl('/home/my-dashboard');
          return AuthActions.whoAmI();
        })
      )
  );

  whoAmIRequest$ = createEffect(() =>
      this.actions$.pipe(
        ofType(AuthActions.whoAmI),
        switchMap(() =>
          this.authEndpointsService.whoAmI()
            .pipe(
              map(response => {
                return AuthActions.whoAmISuccess(response);
              }),
              catchError(e => of(AuthActions.whoAmIFail(e)))
            )
        )
      )
  );

  logout$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.logout),
      tap(() => this.router.navigateByUrl('/auth/login'))
    ), { dispatch: false }
  );

}
