import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import * as LoanActions from 'src/app/store/actions/loan-request.actions';
import {catchError, map, switchMap } from 'rxjs/operators';
import {LoanService} from '../../services/loan.service';
import {of, forkJoin} from 'rxjs';
import {Store} from '@ngrx/store';
import {State} from '../reducers';
import {LoanRequestSuccessModalComponent} from '../../components/loan-request/loan-request-contract/loan-request-success-modal';
import {MatDialog} from '@angular/material';
import {Router} from '@angular/router';
import { Gtag } from 'angular-gtag';

@Injectable()
export class LoanRequestEffects {

  constructor(
    private actions$: Actions,
    private loanService: LoanService,
    private store: Store<State>,
    private dialog: MatDialog,
    private router: Router,
    public gtag : Gtag
  ) {
  }

  fetchClientLoans$ = createEffect(() => {
    return this.actions$
      .pipe(
        ofType(LoanActions.fetchClientLoans),
        switchMap(() => this.loanService.clientLoans().pipe(
          map(res => LoanActions.fetchClientLoansSuccess(res)),
          catchError(e => of(LoanActions.fetchClientLoansFail(e)))
        ))
      );
  });

  getClientLoansInfo$ = createEffect(() =>
    this.actions$.pipe(
      ofType(LoanActions.getClientLoansInfo),
      switchMap(() =>
        this.loanService.loanInfo()
          .pipe(
            map(({active_loan, available_loan, total_loan, min_loan, loan_unit, days_worked, days_to_loan, seeds, error}) => {
              if (error) {
                return LoanActions.getClientLoansFail({
                  error: {
                    message: error ? error : 'Ocurrió un error inesperado.'
                  }
                });
              }
              return LoanActions.getClientLoansInfoSuccess({
                active_loan, available_loan, total_loan,
                min_loan, loan_unit, days_worked, days_to_loan, seeds,
              });
            }),
            catchError(e => of(LoanActions.getClientLoansFail(e)))
          )
      )
    )
  );

  getLoanOptions$ = createEffect(() =>
    this.actions$.pipe(
      ofType(LoanActions.loanOptions),
      switchMap(({amount}) =>
        this.loanService.loanOptions({amount})
          .pipe(
            map(res => {
              if (res.error) {
                const {error} = res;
                return LoanActions.loanOptionsFail({
                  error: {
                    message: error ? error : 'Ocurrió un error inesperado'
                  }
                });
              }
              return LoanActions.loanOptionsSuccess(res);
            }),
            catchError(({error}) => of(LoanActions.loanOptionsFail(error)))
          )
      )
    )
  );

  fetchClientFormData$ = createEffect(() =>
    this.actions$.pipe(
      ofType(LoanActions.clientForm),
      switchMap(() =>
        forkJoin({
          contractData: this.loanService.contractData(),
          clientInfo: this.loanService.clientInfo(),
        }).pipe(
          map(({ contractData, clientInfo }) => {
            const { postal_code: zip } = clientInfo.form;
            this.store.dispatch(LoanActions.fetchZipCodeData({zip}));
            return LoanActions.clientFormSuccess({
              form: {
                ...clientInfo.form
              },
              isFirstLoan: clientInfo.first,
              contractData
            });
          }),
          catchError(e => of(LoanActions.clientFormFail(e)))
        )
      )
    )
  );

  /*
  tmpServicesMerged$ = createEffect(() =>
    this.actions$.pipe(
      ofType(LoanActions.clientForm),
      tap(() => {
        forkJoin({
          contractData: this.loanService.contractData(),
          clientInfo: this.loanService.clientInfo(),
        })
          .subscribe(val => {
            debugger
          });
      })
    ),
    { dispatch: false }
  );
  */

  fetchFamiliesList$ = createEffect(() =>
    this.actions$.pipe(
      ofType(LoanActions.getFamilies),
      switchMap(() =>
        this.loanService.familiesOptions()
          .pipe(
            map(({families}) => {
              return LoanActions.getFamiliesSuccess({families});
            }),
            catchError(e => of(LoanActions.getFamiliesFail(e)))
          )
      )
    )
  );

  senLoanRequest$ = createEffect(() =>
    this.actions$.pipe(
      ofType(LoanActions.loanRequest),
      switchMap(({client, loan, extra_data }) => {
        return this.loanService.loanRequest({ client, loan, extra_data })
          .pipe(
            map(({success, errors, error}) => {
              if (errors || error) {
                return LoanActions.loanRequestFail({
                  errors,
                  error
                });
              }

              this.gtag.event('click', { 
                method: 'contrato',
                event_category: 'boton',
                event_label: 'contrato'
              });

              const dialogRef = this.dialog.open(LoanRequestSuccessModalComponent, {
                panelClass: 'mat-dialog-success',
                width: '50vw',
                disableClose: true
              });

              dialogRef.afterClosed()
                .toPromise()
                .then(() => {
                  this.router.navigateByUrl('/home/loans');
                });
              return LoanActions.loanRequestSuccess({success});
            }),
            catchError(e => of(LoanActions.loanRequestFail(e)))
          );
      })
    )
  );

  fetchZipCodeData$ = createEffect(() =>
    this.actions$.pipe(
      ofType(LoanActions.fetchZipCodeData),
      switchMap(({zip}) =>
        this.loanService.zipCode({zip})
          .pipe(
            map(res => LoanActions.fetchZipCodeDataSuccess(res)),
            catchError(e => of(LoanActions.fetchZipCodeDataFail(e)))
          )
      )
    )
  );

}
