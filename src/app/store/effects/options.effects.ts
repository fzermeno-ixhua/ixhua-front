import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  State,
  deleteAccount,
  deleteAccountSuccess,
  deleteAccountFailed,
} from '../reducers';
import { logout } from 'src/app/store/actions';
import { catchError, map, switchMap } from "rxjs/operators";
import { ClientService } from "../../services/client.service";
import { of } from "rxjs";
import { Store } from "@ngrx/store";
import { MatDialog } from "@angular/material/dialog";
import { AccountDeletionFailedModalComponent } from "src/app/components/options/account-deletion-failed-modal.component";

@Injectable()
export class OptionsEffects {

  constructor(
    private actions$: Actions,
    private clientService: ClientService,
    private store: Store<State>,
    private dialog: MatDialog,
  ) {  }

  deleteAccount$ = createEffect(() =>
    this.actions$.pipe(
      ofType(deleteAccount),
      switchMap(() =>
        this.clientService.deleteClientAccount()
          .pipe(
            map(res => {
              this.store.dispatch(logout());
              return deleteAccountSuccess();
            }),
            catchError(({ error }) => {
              this.dialog.open(AccountDeletionFailedModalComponent, {
                width: '870px',
                disableClose: true
              });
              return of(deleteAccountFailed(error));
            })
          )
      )
    )
  );

}
