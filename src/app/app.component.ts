import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router, Event as RouterEvent,NavigationStart,NavigationEnd,NavigationCancel,NavigationError} from '@angular/router';
import {filter, map } from 'rxjs/operators';
import {Store} from '@ngrx/store';
import {State} from './store/reducers';
import {logout, savePinRouteParams} from './store/actions';
import {Subject, Subscription} from 'rxjs';
import { CloseSessionDialogComponent } from 'src/app/components/close-session-dialog/close-session-dialog.compoment';
import { MatDialog } from '@angular/material';
import { environment } from '../environments/environment';
import { HttpClient } from '@angular/common/http';
import {MatPasswordStrengthComponent} from '@angular-material-extensions/password-strength';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  static TIME_TO_CLOSE_SESSION = 365 * 60 * 60 * 1000; //Days * Minutes * Seconds * Milliseconds

  private clientActivitySubject = new Subject();
  private lastInteractionSubscriber: Subscription;
  private session:any;
  private timeoutId = null;
  private API_URL = environment.API_URL;
  public showOverlay = true;

  constructor(
    private router: Router,
    private store: Store<State>,
    private dialog: MatDialog,
    private http: HttpClient,
  ) { 
    router.events.subscribe((event: RouterEvent) => {
      this.navigationInterceptor(event)
    })
  }

  /**
   * @description The route received by the email is: /pin/:pin/:email
   * this router event subscription is used to take those params
   * in the route before its redirected to /auth/pin
   * and save that params in the store before being lost
   */
  ngOnInit(): void {
    this.router.events
      .pipe(
        filter(e => e instanceof NavigationEnd),
        filter<NavigationEnd>(e => e.urlAfterRedirects === '/auth/pin'),
        filter<NavigationEnd>(e => e.url.startsWith('/pin/')),
        map<NavigationEnd, string[]>(e => e.url.split('/').slice(-2)),
        filter<string[]>(p => p.length === 2)
      ).subscribe(
        p => this.store.dispatch(savePinRouteParams({
          pin: p[0],
          email: p[1]
        }))
      );
    // Init the timeout
    this.timeoutId = setTimeout(() => this.onTimerFinish(), AppComponent.TIME_TO_CLOSE_SESSION);
    this.lastInteractionSubscriber =
      this.clientActivitySubject.subscribe(() => {
        if (this.timeoutId === null) { return; }
        clearTimeout(this.timeoutId);
        this.timeoutId = setTimeout(() => this.onTimerFinish(), AppComponent.TIME_TO_CLOSE_SESSION);
      });
  }

  ngOnDestroy(): void {
    this.clientActivitySubject.complete();
    this.lastInteractionSubscriber.unsubscribe();
  }

  public onClick(): void {
    this.clientActivitySubject.next();
  }

  private onTimerFinish(): void {
    this.session = JSON.parse(sessionStorage.auth)
    
    if(this.session.client){
      if (this.session.client.rfc){
        const body = { rfc:this.session.client.rfc }

        this.http.post(`${this.API_URL}/api/v1/client/close_session_expire/`, body)
        .subscribe({
          next: data => {
            console.log(data)
          },
          error: error => { 
            console.log(error)
          }
        })

        const dialogRef = this.dialog.open(CloseSessionDialogComponent, {
          width: '870px',
          panelClass: 'close-session-dialog',
          disableClose: true
        });
      }
    }
    this.store.dispatch(logout());
  }


    // Shows and hides the loading spinner during RouterEvent changes
  navigationInterceptor(event: RouterEvent): void {
      
    if (event instanceof NavigationStart) {
      this.showOverlay = true;
    }
    if (event instanceof NavigationEnd) {
      this.showOverlay = false;
    }

    // Set loading state to false in both of the below events to hide the spinner in case a request fails
    if (event instanceof NavigationCancel) {
      this.showOverlay = false;
    }
    if (event instanceof NavigationError) {
      this.showOverlay = false;
    }
  }

}
