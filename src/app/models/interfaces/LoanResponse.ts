export interface LoanResponse {
  success?: boolean;
  message?: string;
  error?: any;
  errors?: {
    [s: string]: string;
  };
}
