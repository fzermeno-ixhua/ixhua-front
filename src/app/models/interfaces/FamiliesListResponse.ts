export interface FamiliesListResponse {
  families: Family[];
}

interface Family {
  id: number;
  name: string;
}
