import { RequestLoanForm } from './RequestLoanForm';

export interface LoanRequest {
  loan: {
    option: number;
    amount: number;
  };
  client: RequestLoanForm;
  extra_data?: any; // TODO: Type this field correctly
}
