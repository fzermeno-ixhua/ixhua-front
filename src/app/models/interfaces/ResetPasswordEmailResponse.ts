export interface ResetPasswordEmailResponse {
  success: true;
  error?: string;
  email?: string;
}
