export interface DeleteAccountResponse {
  success: boolean;
  error?: boolean;
}
