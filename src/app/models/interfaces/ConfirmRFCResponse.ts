export interface ConfirmRFCResponse {
  success: boolean;
  email?: string;
  error?: string;
}
