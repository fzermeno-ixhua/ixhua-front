export interface GetClientLoansResponse {
  total_loan: number;
  active_loan: number;
  available_loan: number;
  min_loan: number;
  loan_unit: string;
  days_worked: number;
  days_to_loan: number;
  seeds?: {
    green: number;
    yellow: number;
    gray: number;
  };
  error?: string;
}
