export interface ZipCodeResponse {
  codigo_postal: number;
  municipio: string;
  estado: string;
  colonias: string[];
}
