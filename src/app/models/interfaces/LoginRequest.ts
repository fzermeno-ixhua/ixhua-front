export interface LoginRequest {
  rfc: string;
  password: string;
}
