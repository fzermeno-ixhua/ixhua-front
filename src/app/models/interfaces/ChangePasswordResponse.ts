export interface ChangePasswordResponse {
  success: boolean;
  token: string;
  error?: string;
}
