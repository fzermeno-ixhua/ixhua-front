export interface ConfirmPINResponse {
  success: boolean;
  error?: string;
}
