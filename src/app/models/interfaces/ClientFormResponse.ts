import { RequestLoanForm } from './RequestLoanForm';

export interface ClientFormResponse {
  first: boolean;
  form: RequestLoanForm;
}
