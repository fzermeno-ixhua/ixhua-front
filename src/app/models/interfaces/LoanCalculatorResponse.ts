export interface LoanCalculatorResponse {
  options: LoanOption[];
  error?: string;
  extra_data?: { [s: string]: any };
}

export interface LoanOption {
  date: string;
  term: number;
  interest: number;
  tfa: number;
  aperture: number;
  total: number;
  cat: number;
  tfa_letters: string;
}
