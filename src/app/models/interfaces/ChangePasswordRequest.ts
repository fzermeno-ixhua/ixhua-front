export interface ChangePasswordRequest {
  pin?: string;
  token?: string;
  password: string;
  passwordConfirm: string;
}
