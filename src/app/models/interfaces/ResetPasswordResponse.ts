export interface ResetPasswordResponse {
  token?: string;
  error?: any;
}
