export interface ContractData {
  // TODO: Remove the optional indication to the hardcoded properties
  websiteurl?: string;
  company?: string;
  president?: string;
  website?: string;
  folio?: number;
  rfc?: string;
  clientName?: string;
  curp?: string;
  birthddate?: string;
  place_of_birth?: string;
  nacionality?: string;
  gender?: string;
  email?: string;
  address?: string;
  phone?: number;
  cellphone?: number;
  company_where_you_work?: string;
  payment_day?: string;
  privacy_policy_url?: string;
  creditAmount?: number;
  loan_interest?: number;
  clabe?: number;
  bank?: string;
  contract_period_of_time?: number;
  company_account_bank?: string;
  company_account?: number;
  company_clabe?: number;
  account_holder?: string;
  account_reference?: string;
  moratorium_interest?: number;
  profecourl?: string;
  profeco_number?: number;
  profeco_number_inside_the_republic?: number;
  profeco_email?: string;
  contract_date?: { day: string; month: string; year: string };
  family?: string;
}
