export interface ClientLoansResponse {
  data: LoanInfo[];
}

export interface LoanInfo {
  request_no: string;
  amount: number;
  request_date: string;
  payment_date: string;
  status: string;
  delayed_days?: number;
  reason_for_rejection?: string;
}
