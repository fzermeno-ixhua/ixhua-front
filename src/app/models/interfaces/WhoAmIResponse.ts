export interface WhoAmIResponse {
  first_name: string;
  middle_name: string;
  father_last_name: string;
  mother_last_name: string;
  display_name: string;
  role: number;
  email: string;
  phone: string;
  cellphone: string;
}
