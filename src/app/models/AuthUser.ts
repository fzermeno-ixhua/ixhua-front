/**
 * @description This model is used to describe
 * all the users when they're NOT logged in and they're
 * on the process to create/verify their account.
 */
export class AuthUser {
  public rfc = null;
  public mutedEmail = null;
  public pin = null;
  public password = null;

  constructor() {  }
}
