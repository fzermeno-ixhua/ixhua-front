/**
 * @description This is the actual authorized user
 * inside the application. Note the token field
 * used for requests
 */
import { WhoAmIResponse } from './interfaces';

export class Client implements WhoAmIResponse {
  cellphone: string = null;
  email: string = null;
  phone: string = null;
  role = -1;
  // tslint:disable-next-line:variable-name
  first_name: string = null;
  // tslint:disable-next-line:variable-name
  middle_name: string = null;
  // tslint:disable-next-line:variable-name
  mother_last_name: string = null;
  // tslint:disable-next-line:variable-name
  display_name: string = null;
  // tslint:disable-next-line:variable-name
  father_last_name: string = null;
  public token: string = null;

  constructor(
    public rfc: string = null
  ) {  }
}
